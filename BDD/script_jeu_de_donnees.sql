
-- #------------------------------------------------------------
-- #        SCRIPT INSERT JEU DE DONNEES
-- #------------------------------------------------------------

-- #------------------------------------------------------------
-- # Table: centre
-- #------------------------------------------------------------

-- Aurélien
INSERT INTO centre
    (
    nom_centre,
    adresse_ligne_1_centre,
    code_postal_centre,
    ville_centre,
    tel_centre,
    tel_crcd,
    email_centre,
    url_map_centre,
    nom_contact_centre,
    horaires_centre
    )
VALUES
    (
        "Afpa Saint-jean-de-vedas",
        "12 Rue Jean Mermoz",
        "34430",
        "Saint-jean-de-vedas",
        "0826461414",
        "0499527900",
        "afpasjdv@afpa.fr",
        "https://www.google.com/maps/place/AFPA/@43.5645741,3.8450793,15z/data=!4m5!3m4!1s0x0:0x5fe2ea1bc7b758d9!8m2!3d43.5645741!4d3.8450793",
        "hôte d'accueil",
        "8h-17h"
    );


-- #------------------------------------------------------------
-- #   Table: social
-- #------------------------------------------------------------

-- Rapha
INSERT INTO social
    (id_social, id_centre, nom_social, pictogramme_social, lien_social, ordre_social, active_social)
VALUES
    (1, 1, 'Facebook', 'fa-facebook', 'https://fr-fr.facebook.com/', 1, 1),
    (2, 1, 'Twitter', 'fa-twitter', 'https://twitter.com/?lang=fr', 2, 1),
    (4, 1, 'Youtube', 'fa-youtube', 'https://www.youtube.com/', 3, 1),
    (5, 1, 'Instagram', 'fa-instagram-square', 'https://www.instagram.com/?hl=fr', 4, 0),
    (6, 1, 'Pinterest', 'fa-pinterest', 'https://www.pinterest.fr/', 5, 0);


-- #------------------------------------------------------------
-- #   Table: faq
-- #------------------------------------------------------------

-- Terry
INSERT INTO faq
    (id_centre, question_faq, reponse_faq, ordre_faq, active_faq)
Value
(1,
"Les produits sont-ils bio ?",
"Certains producteurs proposent des produits bio, la mention est précisée dans leur fiche d'identité",
1,
1
),
(1, "Quels sont les moyens de paiement acceptés ?", "Les moyens de paiement acceptés sont les espèces et la carte bancaire", 2, 1),
(1, "Comment récupère t-on les produits ?", "Les produits sont recupéres dans les locaux de l'AFPA à l'accueil", 3, 1);


-- #------------------------------------------------------------
-- #   Table: actualité
-- #------------------------------------------------------------

-- Benjamin
INSERT INTO actualite
    (titre_actualite, accroche_actualite, contenu_actualite, date_actualite, active_actualite, id_centre)
VALUES
    ('Ma premiere actu', 'je suis une super accroche', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dolor doloremque enim fugiat illum odit quia, ut? Aliquid laborum minus quae tenetur voluptatum. Alias delectus impedit iste! Aspernatur, assumenda at aut consectetur dolorum esse excepturi exercitationem hic modi nisi porro quaerat quasi quia quidem quisquam recusandae sed similique? Dicta dolorum, illum iste mollitia non odio praesentium quia quos reiciendis unde!', '2020-10-10', 0, 1),
    ('ma seconde actu', 'lsdkfjg lskfdjfgh sdkffg', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dolor doloremque enim fugiat illum odit quia, ut? Aliquid laborum minus quae tenetur voluptatum. Alias delectus impedit iste! Aspernatur, assumenda at aut consectetur dolorum esse excepturi exercitationem hic modi nisi porro quaerat quasi quia quidem quisquam recusandae sed similique? Dicta dolorum, illum iste mollitia non odio praesentium quia quos reiciendis unde!', '2020-10-10', 1, 1),
    ('sqldfjhq sdlkhqdlk', 'msleijgois dsijfg smdlfijg ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus dolor doloremque enim fugiat illum odit quia, ut? Aliquid laborum minus quae tenetur voluptatum. Alias delectus impedit iste! Aspernatur, assumenda at aut consectetur dolorum esse excepturi exercitationem hic modi nisi porro quaerat quasi quia quidem quisquam recusandae sed similique? Dicta dolorum, illum iste mollitia non odio praesentium quia quos reiciendis unde!', '2020-10-10', 1, 1);


-- #------------------------------------------------------------
-- #   Table: type_utilisateur
-- #------------------------------------------------------------

-- Aurélien
INSERT INTO type_utilisateur
    (nom_type_utilisateur, active_type_utilisateur)
VALUES
    ("client-stagiaire", 1),
    ("client-salarier", 1),
    ("administrateur", 1);


-- #------------------------------------------------------------
-- #   Table: utilisateur
-- #------------------------------------------------------------

INSERT INTO utilisateur
    (id_centre, id_type_utilisateur, civilite_utilisateur, nom_utilisateur, prenom_utilisateur, email_utilisateur, tel_utilisateur, code_utilisateur, mdp_utilisateur, cle_init_mdp_utilisateur, validation_compte_utilisateur, active_utilisateur)
VALUES
    -- Aurélien
    (1, 3, 1, 'M', 'Jean_Patrick', 'bigou@enbarre.fr', '0456235789', 'A537u', 'passworda1', 'password_key_one1', 2, 1),
    (1, 3, 1, 'S', 'Gregory', 'cubil@enpoudre.fr', '1456235789', 'A538r', 'passworda2', 'password_key_two2', 2, 1),
    (1, 3, 1, 'R', 'Benjamin', 'kikou@enboite.com', '0456235789', 'A539i', 'passworda3', 'password_key_three3', 2, 1),
    -- Adrien
    (1, 3, 1, 'afpanier', 'Admin', 'admin@afpanier.fr', '0426199582', '000000000000', '_13gT;@i8UZ.#jf', NULL, 2, 1),
    (1, 2, 1, 'Toiredéfille', 'Théodore', 't.cuvellier@gogole.fr', '0425613589', '37FR5T672012', 'm0n_sup3r_mdp!', NULL, 2, 1),
    (1, 1, 0, 'Titgoute', 'Emma', 'e.titgoute@gogole.fr', '0448579563', 'EU3259F45111', 'monchien', "877455", 1, 1),
    -- Greg
    (1, 1, 0, 'De La Berge', 'Gertrude', 'delaberge.gertrude@gmail.com', '0607080910', '553271698413', 'CeM0tDeP@sse€stTrèsPuiss@nt', 'jesuislacledegertrudedelaberge', 2, 1),
    (1, 2, 1, 'Son', 'Goku', 'jesuisunsaiyan@hotmail.com', '0708091011', '974531587169', 'K@m€h@m€h@!', 'vegetaleterneldeuxieme', 0, 1),
    (1, 3, 1, 'Lubilette', 'Jean-Machin', 'mobilubilette@outlook.com', '0605040302', '346845215987', '€lC@mpeonD€L@Lubil€tte', 'lubilettemobilettepouetpouet', 1, 1),
    -- Sissi
    (1, 1, 0, 'Dudu', 'Lilou', 'dudu@lilou.fr', '0123654789', 'B852m', 'afpa', null , 1, 1),
    (1, 2, 1, 'Laffreux', 'Jojo', 'jojo@lafreux.com', '7896541230', 'Q123p', 'afpa', null, 1, 1),
    (1, 3, 1, 'Lagafe', 'Gaston', 'gaston@lagafe.fr', '7896301254', 'P258b', 'afpa', null, 0, 1);


-- #------------------------------------------------------------
-- #   Table: producteur
-- #------------------------------------------------------------

-- Cyril
INSERT INTO producteur
    (
    rs_producteur,
    adresse_producteur,
    adresse_complement_producteur,
    cp_producteur,
    ville_producteur,
    telephone_producteur,
    email_producteur,
    note_producteur,
    image_producteur,
    gmap_producteur,
    description_producteur,
    nom_contact_producteur,
    prenom_contact_producteur,
    telephone_contact_producteur,
    email_contact_producteur,
    active_producteur
    )
VALUES
    (
        "Farm Corp",
        "65 rue des courgettes",
        "Appartement 23",
        "34000",
        "Montpellier",
        "06.87.85.85.98.",
        "FarmCorp@gmail.fr",
        NULL,
        NULL,
        NULL,
        "Blah blah blah",
        "DuChamp",
        "Jean-Paul",
        "0601020304",
        NULL,
        1
),
    (
        "Chez Bibi",
        "48 allée des tigres du bengale",
        NULL,
        "34430",
        "Saint Jean de Vedas",
        "06.22.22.22.22.",
        "Bibi@outlook.fr",
        NULL,
        NULL,
        NULL,
        "Blah blah blah",
        "Pétunia",
        "Rodolphe",
        "07.18.81.18.81.",
        "Roro@gmail.com",
        1
),
    (
        "Carottes & Co",
        "13 impasse du lapin",
        NULL,
        "34970",
        "Lattes",
        "06.11.11.11.11.",
        "CarottesCo@outlook.com",
        NULL,
        NULL,
        NULL,
        "Blah blah blah",
        "Dufour",
        "Gertrude",
        NULL,
        "GertrudeDuFour@gmail.fr",
        1
);


-- #------------------------------------------------------------
-- #   Table: type_producteur
-- #------------------------------------------------------------

-- Cyril
INSERT INTO type_producteur
    (nom_type_producteur,active_type_producteur)
VALUES
    ("Légumes", 1),
    ("Fruits", 1),
    ("Poissons", 0);


-- #------------------------------------------------------------
-- #   Table: commentaire
-- #------------------------------------------------------------

-- Cyril
INSERT INTO commentaire
    (
    id_producteur,
    id_utilisateur,
    contenu_commentaire,
    niveau_commentaire,
    date_commentaire
    )
VALUES
    (
        1,
        1,
        "C'est un gentil producteur",
        1,
        "2020-10-23"
),
    (
        1,
        2,
        "Il a changé des artichaud par des tomates",
        0,
        "2020-11-02"
),
    (
        3,
        2,
        "Ces produits sont dégeulasses !!!! Berk berk berk !",
        2,
        "2020-11-12"
);


-- #------------------------------------------------------------
-- #   Table: panier
-- #------------------------------------------------------------

-- Jp
INSERT INTO panier
    (
    id_producteur,
    nom_panier,
    description_panier,
    visibilite_panier_debut,
    visibilite_panier_fin,
    commande_panier_debut,
    commande_panier_fin,
    paiement_panier_debut,
    paiement_panier_fin,
    livraison_panier
    )
VALUES
    (
        1,
        "Mixte_Farm_Corp_1",
        "Un peu de tout",
        "2020-10-06 07:00:00",
        "2020-10-24 17:00:00",
        "2020-10-06 08:00:00",
        "2020-10-13 12:00:00",
        "2020-10-09 08:00:00",
        "2020-10-14 12:00:00",
        "2020-10-15 13:00:00"
),
    (
        2,
        "Fruit_Chez_Bibi_1",
        "Plusieurs fruits",
        "2020-10-13 07:00:00",
        "2020-10-31 17:00:00",
        "2020-10-13 08:00:00",
        "2020-10-20 12:00:00",
        "2020-10-16 08:00:00",
        "2020-10-21 12:00:00",
        "2020-10-22 13:00:00"
),
    (
        3,
        "Carotte_Carotte_&_Co_1",
        "Que des carottes !!!",
        "2020-10-20 07:00:00",
        "2020-11-07 17:00:00",
        "2020-10-20 08:00:00",
        "2020-10-27 12:00:00",
        "2020-10-23 08:00:00",
        "2020-10-28 12:00:00",
        "2020-10-29 13:00:00"
);


-- #------------------------------------------------------------
-- #   Table: variante_panier
-- #------------------------------------------------------------

-- Jp
INSERT INTO variante_panier
    (id_panier, titre_variante_panier, prix_variante_panier)
VALUES
    (1, "Petit Panier", 5.50),
    (1, "Moyen Panier", 8),
    (2, "Petit Panier", 5.50),
    (2, "Grand Panier", 10),
    (3, "Petit Panier", 3.50),
    (3, "Moyen Panier", 5.50);


-- #------------------------------------------------------------
-- #   Table: produit
-- #------------------------------------------------------------

-- Jp
INSERT INTO produit
    (nom_produit)
VALUES
    ("Carotte"),
    ("Fraise"),
    ("Pommes de Terre"),
    ("Melon"),
    ("Pommes"),
    ("Tomates"),
    ("Artichaud");


-- #------------------------------------------------------------
-- #   Table: commande
-- #------------------------------------------------------------

-- Sissi
INSERT INTO commande
    (id_utilisateur, reference_commande, numero_commande, code_validation_commande, date_validation_commande, date_paiement_commande, etat_commande)
VALUES
    ('7', 'afpan3', '89', null, "2020-09-29", "2020-09-29", '2' ),
    ('8', 'afpan3', '90', null, "2020-09-10", "2020-09-11", '3' ),
    ('8', 'afpan9', '91', null, "2020-09-15", "2020-09-17", '3'),
    ('8', 'afpan9', '92', null, "2020-09-27", "2020-09-27", '1'),
    ('9', 'afpan3', '93', null, "2020-09-10", "2020-09-11", '3'),
    ('9', 'afpan6', '94', null, "2020-09-29", null, '0' );


-- #------------------------------------------------------------
-- #   Table: type_promotion
-- #------------------------------------------------------------

-- Damien
INSERT INTO type_promotion
    (nom_type_promotion, active_type_promotion)
VALUES
    ('euro', 1),
    ('pourcent', 1),
    ('aliment', 1);


-- #------------------------------------------------------------
-- #   Table: promotion
-- #------------------------------------------------------------

-- Damien
INSERT INTO promotion
    (id_type_promotion, code_promotion, date_debut_promotion, date_fin_promotion, valeur_promotion, texte_libre_promotion, quantite_maxi_globale_promotion, active_promotion)
VALUES
    (2, 'MUFAS31', '2019-10-15', '2019-11-14', 10, '', 30, 1),
    (1, 'EUROL57', '2020-11-10', '2020-12-09', 5, '', 100, 1),
    (3, 'PILOTY74', '2020-12-15', '2021-01-14', 1, 'pastèque', 80, 1),
    (3, 'VERABI19', '2020-10-26', '2020-10-30', 1, 'potiron', 50, 0);


-- #------------------------------------------------------------
-- #   Table: utilisateur__promotion
-- #------------------------------------------------------------

-- Sissi
insert into utilisateur__promotion
    (id_promotion, id_utilisateur)
VALUES
    (4, 5),
    (2, 9),
    (1, 2);


-- #------------------------------------------------------------
-- #   Table: variante_panier__promotion
-- #------------------------------------------------------------

-- Damien
INSERT INTO variante_panier__promotion
    (id_promotion, id_variante_panier)
VALUES
    (1, 5),
    (2, 2),
    (3, 1),
    (4, 2);


-- #------------------------------------------------------------
-- #    Table: produit__variante_panier
-- #------------------------------------------------------------

-- Jp
INSERT INTO produit__variante_panier
    (id_variante_panier, id_produit, quantite_produit_variante_panier, mesure_produit_variante_panier)
VALUES
    (1, 2, 1, "kg"),
    (1, 3, 1, "kg"),
    (2, 2, 2, "kgs"),
    (2, 3, 2, "kgs"),
    (3, 2, 1, "kg"),
    (3, 5, 1, "kg"),
    (3, 7, 1, "kg"),
    (4, 2, 3, "kgs"),
    (4, 5, 3, "kgs"),
    (4, 7, 3, "kgs"),
    (5, 1, 2, "kgs"),
    (6, 1, 6, "kgs");


-- #------------------------------------------------------------
-- #   Table: commande__variante_panier
-- #------------------------------------------------------------

-- Jp
INSERT INTO commande__variante_panier
    (id_commande, id_variante_panier, quantite_commandee)
VALUES
    (1, 5, 2),
    (2, 2, 3),
    (3, 1, 1),
    (4, 2, 6);


-- #------------------------------------------------------------
-- #   Table: producteur__type_producteur
-- #------------------------------------------------------------

-- Cyril
INSERT INTO producteur__type_producteur
    (id_type_producteur, id_producteur)
VALUES
    ( 1, 1),
    ( 1, 3),
    ( 2, 1),
    ( 2, 2);