-- #------------------------------------------------------------
-- #        Script MySQL.
-- #------------------------------------------------------------


-- #------------------------------------------------------------
-- #        SCRIPT CREATION BDD
-- #------------------------------------------------------------

-- #------------------------------------------------------------
-- #    Table: centre
-- #------------------------------------------------------------

CREATE TABLE centre(
        id_centre              Int  Auto_increment  NOT NULL ,
        nom_centre             Varchar (100) NOT NULL ,
        adresse_ligne_1_centre Varchar (80) NOT NULL ,
        adresse_ligne_2_centre Varchar (80) ,
        code_postal_centre     Varchar (5) NOT NULL ,
        ville_centre           Varchar (80) NOT NULL ,
        tel_centre             Varchar (20) ,
        tel_crcd               Varchar (20) NOT NULL ,
        email_centre           Varchar (60) ,
        url_map_centre         Varchar (250) NOT NULL ,
        nom_contact_centre     Varchar (100) ,
        horaires_centre        Text ,
        b_crcd_ouvert          Bool NOT NULL DEFAULT 0,

	CONSTRAINT centre_PK PRIMARY KEY (id_centre)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: social
-- #------------------------------------------------------------

CREATE TABLE social(
        id_social          Int  Auto_increment  NOT NULL ,
        id_centre          Int NOT NULL ,
        nom_social         Varchar (20) NOT NULL ,
        pictogramme_social Varchar (25) NOT NULL ,
        lien_social        Varchar (50) NOT NULL ,
        ordre_social       TinyINT NOT NULL ,
        active_social      Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT social_PK PRIMARY KEY (id_social) ,

	CONSTRAINT social_centre_FK FOREIGN KEY (id_centre) REFERENCES centre(id_centre)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: faq
-- #------------------------------------------------------------

CREATE TABLE faq(
        id_faq       Int  Auto_increment  NOT NULL ,
        id_centre    Int NOT NULL ,
        question_faq Varchar (250) NOT NULL ,
        reponse_faq  Mediumtext NOT NULL ,
        ordre_faq    TinyINT NOT NULL ,
        active_faq   Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT faq_PK PRIMARY KEY (id_faq) ,

	CONSTRAINT faq_centre_FK FOREIGN KEY (id_centre) REFERENCES centre(id_centre)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: producteur
-- #------------------------------------------------------------

CREATE TABLE producteur(
        id_producteur                 Int  Auto_increment  NOT NULL ,
        rs_producteur                 Varchar (100) NOT NULL ,
        adresse_producteur            Varchar (100) NOT NULL ,
        adresse_complement_producteur Varchar (100) ,
        cp_producteur                 Varchar (5) NOT NULL ,
        ville_producteur              Varchar (50) NOT NULL ,
        telephone_producteur          Varchar (20) NOT NULL ,
        email_producteur              Varchar (50) NOT NULL ,
        note_producteur               TinyINT ,
        image_producteur              Varchar (255) ,
        gmap_producteur               Varchar (255) ,
        description_producteur        Text ,
        nom_contact_producteur        Varchar (50) ,
        prenom_contact_producteur     Varchar (50) ,
        telephone_contact_producteur  Varchar (20) ,
        email_contact_producteur      Varchar (50) ,
        active_producteur             Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT producteur_PK PRIMARY KEY (id_producteur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: panier
-- #------------------------------------------------------------

CREATE TABLE panier(
        id_panier               Int  Auto_increment  NOT NULL ,
        id_producteur           Int NOT NULL ,
        nom_panier              Varchar (50) NOT NULL ,
        description_panier      Varchar (250) NOT NULL ,
        visibilite_panier_debut Datetime NOT NULL ,
        visibilite_panier_fin   Datetime NOT NULL ,
        commande_panier_debut   Datetime NOT NULL ,
        commande_panier_fin     Datetime NOT NULL ,
        paiement_panier_debut   Datetime NOT NULL ,
        paiement_panier_fin      Datetime NOT NULL ,
        livraison_panier        Datetime NOT NULL ,
        image_panier            Varchar (50) ,
        active_panier           Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT panier_PK PRIMARY KEY (id_panier) ,

	CONSTRAINT panier_producteur_FK FOREIGN KEY (id_producteur) REFERENCES producteur(id_producteur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: variante_panier
-- #------------------------------------------------------------

CREATE TABLE variante_panier(
        id_variante_panier    Int  Auto_increment  NOT NULL ,
        id_panier      Int NOT NULL ,
        titre_variante_panier Varchar (50) NOT NULL ,
        prix_variante_panier  Float NOT NULL ,

	CONSTRAINT variante_panier_PK PRIMARY KEY (id_variante_panier) ,

	CONSTRAINT variante_panier_panier_FK FOREIGN KEY (id_panier) REFERENCES panier(id_panier)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: produit
-- #------------------------------------------------------------

CREATE TABLE produit(
        id_produit     Int  Auto_increment  NOT NULL ,
        nom_produit    Varchar (50) NOT NULL ,
        active_produit Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT produit_PK PRIMARY KEY (id_produit)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: type_producteur
-- #------------------------------------------------------------

CREATE TABLE type_producteur(
        id_type_producteur     Int  Auto_increment  NOT NULL ,
        nom_type_producteur    Varchar (30) NOT NULL ,
        active_type_producteur Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT type_producteur_PK PRIMARY KEY (id_type_producteur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: type_utilisateur
-- #------------------------------------------------------------

CREATE TABLE type_utilisateur(
        id_type_utilisateur     Int  Auto_increment  NOT NULL ,
        nom_type_utilisateur    Varchar (50) NOT NULL ,
        active_type_utilisateur Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT type_utilisateur_PK PRIMARY KEY (id_type_utilisateur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: utilisateur
-- #------------------------------------------------------------

CREATE TABLE utilisateur(
        id_utilisateur                Int  Auto_increment  NOT NULL ,
        id_centre                     Int NOT NULL ,
        id_type_utilisateur           Int NOT NULL ,
        civilite_utilisateur          TinyINT NOT NULL ,
        nom_utilisateur               Varchar (100) NOT NULL ,
        prenom_utilisateur            Varchar (100) NOT NULL ,
        email_utilisateur             Varchar (200) NOT NULL ,
        tel_utilisateur               Varchar (20) NOT NULL ,
        code_utilisateur              Varchar (12) NOT NULL ,
        mdp_utilisateur               Varchar (250) NOT NULL ,
        cle_init_mdp_utilisateur      Varchar (250) ,
        validation_compte_utilisateur TinyINT NOT NULL ,
        active_utilisateur            Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT utilisateur_PK PRIMARY KEY (id_utilisateur) ,

	CONSTRAINT utilisateur_centre_FK FOREIGN KEY (id_centre) REFERENCES centre(id_centre) ,
	CONSTRAINT utilisateur_type_utilisateur0_FK FOREIGN KEY (id_type_utilisateur) REFERENCES type_utilisateur(id_type_utilisateur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: commande
-- #------------------------------------------------------------

CREATE TABLE commande(
        id_commande              Int  Auto_increment  NOT NULL ,
        id_utilisateur           Int NOT NULL ,
        reference_commande       Varchar (50) NOT NULL ,
        numero_commande          Varchar (10) NOT NULL ,
        code_validation_commande Varchar (10) ,
        date_validation_commande Datetime NOT NULL ,
        date_paiement_commande   Datetime ,
        etat_commande            TinyINT NOT NULL ,

	CONSTRAINT commande_PK PRIMARY KEY (id_commande) ,

	CONSTRAINT commande_utilisateur_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: commentaire
-- #------------------------------------------------------------

CREATE TABLE commentaire(
        id_commentaire      Int  Auto_increment  NOT NULL ,
        id_producteur       Int NOT NULL ,
        id_utilisateur      Int NOT NULL ,
        contenu_commentaire Text NOT NULL ,
        niveau_commentaire  TinyINT NOT NULL ,
        date_commentaire    Varchar (50) NOT NULL ,

	CONSTRAINT commentaire_PK PRIMARY KEY (id_commentaire) ,

	CONSTRAINT commentaire_producteur_FK FOREIGN KEY (id_producteur) REFERENCES producteur(id_producteur) ON DELETE CASCADE ,
	CONSTRAINT commentaire_utilisateur0_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: type_promotion
-- #------------------------------------------------------------

CREATE TABLE type_promotion(
        id_type_promotion     Int  Auto_increment  NOT NULL ,
        nom_type_promotion    Varchar (50) NOT NULL ,
        active_type_promotion Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT type_promotion_PK PRIMARY KEY (id_type_promotion)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: promotion
-- #------------------------------------------------------------

CREATE TABLE promotion(
        id_promotion                    Int  Auto_increment  NOT NULL ,
        id_type_promotion               Int NOT NULL ,
        code_promotion                  Varchar (10) NOT NULL ,
        date_debut_promotion            Datetime NOT NULL ,
        date_fin_promotion              Datetime ,
        valeur_promotion                Int NOT NULL ,
        texte_libre_promotion           Varchar (50) ,
        quantite_maxi_globale_promotion Int NOT NULL ,
        active_promotion                Bool NOT NULL DEFAULT 1 ,

	CONSTRAINT promotion_PK PRIMARY KEY (id_promotion) ,

	CONSTRAINT promotion_type_promotion_FK FOREIGN KEY (id_type_promotion) REFERENCES type_promotion(id_type_promotion)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: actualite
-- #------------------------------------------------------------

CREATE TABLE actualite(
        id_actualite       Int  Auto_increment  NOT NULL ,
        id_centre          Int NOT NULL ,
        titre_actualite    Varchar (50) NOT NULL ,
        accroche_actualite Varchar (250) NOT NULL ,
        contenu_actualite  Mediumtext NOT NULL ,
        date_actualite     Date NOT NULL ,
        active_actualite   TINYINT NOT NULL DEFAULT 0 ,

	CONSTRAINT actualite_PK PRIMARY KEY (id_actualite) ,

	CONSTRAINT actualite_centre_FK FOREIGN KEY (id_centre) 
                                        REFERENCES centre(id_centre) 
                                        ON DELETE CASCADE 
                                        ON UPDATE NO ACTION
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: utilisateur__promotion
#------------------------------------------------------------

CREATE TABLE utilisateur__promotion(
        id_promotion   Int NOT NULL ,
        id_utilisateur Int NOT NULL ,

	CONSTRAINT utilisateur__promotion_PK PRIMARY KEY (id_promotion,id_utilisateur) ,

	CONSTRAINT utilisateur__promotion_promotion_FK FOREIGN KEY (id_promotion) REFERENCES promotion(id_promotion) ,
	CONSTRAINT utilisateur__promotion_utilisateur0_FK FOREIGN KEY (id_utilisateur) REFERENCES utilisateur(id_utilisateur)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: variante_panier__promotion
-- #------------------------------------------------------------

CREATE TABLE variante_panier__promotion(
        id_promotion Int NOT NULL ,
        id_variante_panier  Int NOT NULL ,

	CONSTRAINT variante_panier__promotion_PK PRIMARY KEY (id_promotion,id_variante_panier) ,

	CONSTRAINT variante_panier__promotion_promotion_FK FOREIGN KEY (id_promotion) REFERENCES promotion(id_promotion) ,
	CONSTRAINT variante_panier__promotion_variante_panier0_FK FOREIGN KEY (id_variante_panier) REFERENCES variante_panier(id_variante_panier)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: produit__variante_panier
-- #------------------------------------------------------------

CREATE TABLE produit__variante_panier(
        id_variante_panier               Int NOT NULL ,
        id_produit                Int NOT NULL ,
        quantite_produit_variante_panier Int NOT NULL ,
        mesure_produit_variante_panier   Varchar (20) NOT NULL ,

	CONSTRAINT produit__variante_panier_PK PRIMARY KEY (id_variante_panier,id_produit) ,

	CONSTRAINT produit__variante_panier_variante_panier_FK FOREIGN KEY (id_variante_panier) REFERENCES variante_panier(id_variante_panier) ,
	CONSTRAINT produit__variante_panier_produit0_FK FOREIGN KEY (id_produit) REFERENCES produit(id_produit)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: commande__variante_panier
-- #------------------------------------------------------------

CREATE TABLE commande__variante_panier(
        id_commande        Int NOT NULL ,
        id_variante_panier        Int NOT NULL ,
        quantite_commandee TinyINT NOT NULL ,

	CONSTRAINT commande__variante_panier_PK PRIMARY KEY (id_commande,id_variante_panier) ,

	CONSTRAINT commande__variante_panier_commande_FK FOREIGN KEY (id_commande) REFERENCES commande(id_commande) ,
	CONSTRAINT commande__variante_panier_variante_panier0_FK FOREIGN KEY (id_variante_panier) REFERENCES variante_panier(id_variante_panier)
)ENGINE=InnoDB;


-- #------------------------------------------------------------
-- #    Table: producteur__type_producteur
-- #------------------------------------------------------------

CREATE TABLE producteur__type_producteur(
        id_type_producteur Int NOT NULL ,
        id_producteur      Int NOT NULL ,

	CONSTRAINT producteur__type_producteur_PK PRIMARY KEY (id_type_producteur,id_producteur) ,

	CONSTRAINT producteur__type_producteur_type_producteur_FK FOREIGN KEY (id_type_producteur) REFERENCES type_producteur(id_type_producteur) ,
	CONSTRAINT producteur__type_producteur_producteur0_FK FOREIGN KEY (id_producteur) REFERENCES producteur(id_producteur)
)ENGINE=InnoDB;


