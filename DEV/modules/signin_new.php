<?php
require_once "service.php";
/**
 * Class Signin_new | fichier signin_new.php
 *
 * Description de la classe à renseigner.
 *
 * Cette classe necessite l'utilisation de la classe :
 *
 * require_once "service.php";
 *
 * @package Afpanier Project
 * @subpackage service
 * @author @Afpa Lab Team - Prenom Nom stagiaire
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */

Class Signin_new
{

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;
	
	public $civility = "";
	public $lastName = "";
	public $firstName = "";
	public $birthdate = "";
	public $userName = "";
	public $emailAdress = "";
	public $phoneNumber = "";
	public $code = "";
	public $password = "";
	public $passwordConfirmation = "";
	
	public $civilityError = "";
	public $lastNameError = "";
	public $firstNameError = "";
	public $birthdateError = "";
	public $userNameError = "";
	public $emailAdressError = "";
	public $phoneNumberError = "";
	public $codeError = "";
	public $passwordError = "";
	public $passwordConfirmationError = "";

    /**
     * init variables resultat
     *
     * execute main function
     */
    public function __construct() {
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
     *
     * Destroy service
     *
     */
    public function __destruct() {
        // destroy objet_service
        unset($objet_service);
    }

    /**
     * Get interface to gestion of signin_new
     */
    function main()
	{
		$objet_service = new Service();
		// Ici je fais mon appel $objet_service->ma_methode_qui_est_dans_le_service
		
		
		// Je passe mes parametres pour y avoir acces dans mes pages HTML
		$this->resultat = $objet_service->resultat;
		$this->VARS_HTML = $objet_service->VARS_HTML;
    }

	function checkSignInForm()
	{
		// Get all form's infos.
		$civility = $_POST['input_signin_gender'];
		$lastName = $_POST['input_signin_lastname'];
		$firstName = $_POST['input_signin_firstname'];
		$birthdate = $_POST['input_signin_birthdate'];
		$userName = $_POST['input_signin_mailingadress'];
		$emailAdress = $_POST['input_signin_emailadress'];
		$phoneNumber = $_POST['input_signin_zipcode'];
		$code = $_POST['input_signin_mailingadress'];
		$password = $_POST['input_signin_password'];
		$passwordConfirmation = $_POST['input_signin_passwordconfirmation'];

		$error = false;

		// If civility is not defined, display an error message.
		if(empty($civility))
		{
			$error = true;
			$civilityError = "Vous devez renseigner votre civilité.";
			DOMDocument::getElementById("civility_alert")->setAttribute("display", "block");
		}

		// If last name has not been defined, notify an error.
		if(empty($lastName))
		{
			$error = true;
			$lastNameError = "Vous devez renseigner votre nom.";
			DOMDocument::getElementById("lastname_alert")->setAttribute("display", "block");
		}

		// If first name has not been defined, notify an error.
		if(empty($firstName))
		{
			$error = true;
			$firstNameError = "Vous devez renseigner votre pr&eacute;nom.";
			DOMDocument::getElementById("firstname_alert")->setAttribute("display", "block");
		}
		
		// If birth date has not been defined, notify an error.
		if(empty($birthdate))
		{
			$error = true;
			$birthdateError = "Vous devez renseigner votre date de naissance.";
			DOMDocument::getElementById("birthdate_alert")->setAttribute("display", "block");
		}
		
		// If user name is not null but not five numbers long, display an error message.
		if(empty($userName))	
		{
			$error = true;
			$userNameError = "Vous devez renseigner un nom d'utilisateur.";
			DOMDocument::getElementById("username_alert")->setAttribute("display", "block");
		}
		/*else if($userName->existsInDatabase())
		{
			$error = true;
			$userNameError = "Ce nom d'utilisateur existe, veuillez en entrer un autre.";
			DOMDocument::getElementById("username_alert")->setAttribute("display", "block");
		}
		*/
		
		// If email adress has not been defined, notify an error.
		if(empty($emailAdress))
		{
			$error = true;
			$emailAdressError = "Vous devez renseigner une adresse courriel.";
			DOMDocument::getElementById("emailadress_alert")->setAttribute("display", "block");
		}
		/*else if($emailAdress->existsInDatabase())
		{
			$error = true;
			$emailAdressError = "Cette adresse courriel est déjà associée à un compte, veuillez en entrer un autre.";
			DOMDocument::getElementById("username_alert")->setAttribute("display", "block");
		}
		*/
		
		// If phone number has not been defined, notify an error.
		if(empty($phoneNumber))
		{
			$error = true;
			$phoneNumberError = "Vous devez renseigner un num&eacute;ro de t&eacute;l&eacute;phone.";
			DOMDocument::getElementById("emailadress_alert")->setAttribute("display", "block");
		}
		
		// If code is not null but not five numbers long, display an error message.
		if(empty($code))	
		{
			$error = true;
			$codeError = "Vous devez renseigner votre code agent/b&eacute;n&eacute;ficiaire.";
			DOMDocument::getElementById("code_alert")->setAttribute("display", "block");
		}
		/*else if($code->existsInDatabase())
		{
			$error = true;
			$code = "Cet code agent/bénéficiaire est déjà associé à un compte, veuillez en entrer un autre.";
			DOMDocument::getElementById("username_alert")->setAttribute("display", "block");
		}
		*/
		
		// If password is empty, display an error message and make sure password confirmation error messages aren't displayed.
		if(empty($password))
		{
			$error = true;
			$passwordError = "Vous devez renseigner un mot de passe.";
			DOMDocument::getElementById("password_alert")->setAttribute("display", "block");
		}
		else
		{
			// If password confirmation is empty, display the null error message, and make sure the different error message is not displayed.
			if(empty($passwordConfirmation))
			{
				$error = true;
				$passwordConfirmationError = "Vous devez confirmer votre mot de passe.";
				DOMDocument::getElementById("passwordconfirmation_alert")->setAttribute("display", "block");
			}
			// Else if password confirmation is different from password, display the different error message, and make sure the null error message is not displayed.
			else if($confirmPassword != $password)
			{
				$error = true;
				$passwordConfirmationError = "La confirmation du mot de passe doit correspondre au mot de passe.";
				DOMDocument::getElementById("passwordconfirmation_alert")->setAttribute("display", "block");
			}
		}

		// If there is an error, display the form error message box.
		if($error)
		{
			DOMDocument::getElementById("form_alert")->setAttribute("display", "block");
		}
		// Else, add a new user to the database and load the confirmation page.
		else
		{
			$this->addNewUser();
		}
	}
}

?>
