
//CREATION D'UN TABLEAU DE COMMENTAIRES REMPLIS
aOfCommentaires =
[
    {
        "producteur": aOfProducteurs[0],
        "contenus": "Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
        "evaluation": "positif",
        "date": "20/02/2020"
    },
    {
        "producteur": aOfProducteurs[0],
        "contenus": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.",
        "evaluation": "positif",
        "date": "20/02/2020"
    },
    {
        "producteur": aOfProducteurs[1],
        "contenus": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
        "evaluation": "positif",
        "date": "20/02/2020"
    }
]

function calculNote(iIndiceProducteur)
{
    var nbPositif = 0;
    var i;
    for (i=0; i<aOfCommentaires.length; i++)	
    {
        if (aOfCommentaires[i].producteur.idfournisseur ==iIndiceProducteur)
        {
            if (aOfCommentaires[i].evaluation == "positif")
            {
                nbPositif+= 1;
            }
            else if (aOfCommentaires[i].evaluation == "negatif")
            {
                nbPositif+= -1;
            };
        }
    }
    aOfProducteurs[iIndiceProducteur].note = nbPositif;
}

function afficheCommentaire()
{
    var sHTMLCommentaire ="";
    var nbPositif = 0;
    for (i=0; i<aOfCommentaires.length; i++)	
    {
        if (aOfCommentaires[i].producteur.idfournisseur ==iIndiceEditionEncours)
        {
            sHTMLCommentaire+="<div class=\"border rounded p-3\">"
            sHTMLCommentaire+= "<p>("+aOfCommentaires[i]["producteur"]["raisonsociale"]+") Commentaire du "+aOfCommentaires[i]["date"]+" : </p>";
            sHTMLCommentaire+= "<p>\""+aOfCommentaires[i]["contenus"]+"\"</p>";
            sHTMLCommentaire+= "<p> Evaluation : "+aOfCommentaires[i]["evaluation"]+"</p>";
            sHTMLCommentaire+= "<button onClick=\"supprimCommentaire(" + i + ")\" class=\"btn_action rounded\">Supprimer</button></br>";
            sHTMLCommentaire+="</div>"
        }
        document.getElementById("commentaireliste").innerHTML = sHTMLCommentaire;
    };
};

afficheCommentaire();

function ajouteCommentaire()
{
    var dateCommentaire = new Date();
    
    var evaluationCommentaire;
    if (document.getElementById("evaluation1").checked == true)
    {
        evaluationCommentaire = document.getElementById("evaluation1").value
    }
    else if (document.getElementById("evaluation3").checked == true)
    {
        evaluationCommentaire = document.getElementById("evaluation3").value
    }
    else
    {
        evaluationCommentaire = document.getElementById("evaluation2").value
    }

    var newCommentaire = 
    {
        producteur: aOfProducteurs[iIndiceEditionEncours],
        contenus: document.getElementById("newCommentaire").value,
        evaluation: evaluationCommentaire,
        date: dateCommentaire.getDate()+"/"+(dateCommentaire.getMonth()+1)+"/"+dateCommentaire.getFullYear()
    };
    
    if (document.getElementById("newCommentaire").value !== null && document.getElementById("newCommentaire").value !== '')
    {
         aOfCommentaires.push(newCommentaire);
    };

    document.getElementById("newCommentaire").value = "";
    document.getElementById("evaluation1").checked = false;
    document.getElementById("evaluation2").checked = false;
    document.getElementById("evaluation3").checked = false;
    afficheCommentaire();
    calculNote(iIndiceEditionEncours);
    rebuildTable() 
};

function supprimCommentaire(iSupprimComment)
{
    aOfCommentaires.splice (iSupprimComment, 1);
    afficheCommentaire();
    calculNote(iIndiceEditionEncours);
    rebuildTable() 
}