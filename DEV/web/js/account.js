sources.client = {
  client_id: 246,
  client_civilite: "other",
  client_nom: "Bidule",
  client_prenom: "Dominique",
  client_code_beneficiaire: "2020775930G",
  client_mail: "dom.bidule@fake.com",
  client_tel: "06 01 23 45 67",
  client_compte_valide: false,
  nombre_paniers: 17,
};

if (!sources.client.client_compte_valide) {
  document.getElementById("banner-container").innerHTML = `
<div class="mb-4" role="alert" aria-live="assertive" aria-atomic="true">
  <div class="toast-header">
    <img src="..." class="rounded mr-2" alt="...">
    <strong class="mr-auto">Compte en attente de validation</strong>
    <small>11 mins ago</small>
    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="">
    Encore un peu de patience, un administrateur va bientôt valider votre compte.
  </div>
</div>`;
}

hydrate();
