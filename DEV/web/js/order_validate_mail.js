/**
 * Génère un numéro de commande et code unique automatiquement
 */

$( document ).ready(function() {
    $('#numcom').html('N° ' + Math.floor(Math.random() * 1000000));
    $('#numuni').html(Math.floor(Math.random() * 9999));
});

/**
 * Tableau des codes uniques
 * @type {*[]}
 */
var aNumUni = [];

function numUni()
{
    /**
     * Récupère le code unique transmis, le stock dans le tableau.
     * Vérifie qu'une valeur est bien entrée, vide l'input après envoi.
     */
    let iNum = document.getElementById("code").value;
    if (iNum !== "") {
        aNumUni.push(iNum);
        document.getElementById("code").value = "";
        document.getElementById("code").style.borderColor = "green";
        window.location.href = "route.php?page=order_validate_new";
    }
    else {
        /**
         * Si l'input est vide, affichage message d'erreur et modification css
         * @type {string}
         */
        document.getElementById("code").style.borderColor = "red";
    }

    console.log(aNumUni);
}
