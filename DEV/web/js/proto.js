const A_TYPES = ['number','bigint','string','boolean','symbol','undefined','function','object']


// -------		REGEXP		-------

RegExp.prototype.escape = function(str) {
	return str.escapeForRegExp();
}


// -------		STRING		-------

String.prototype.toCamelCase = function() {
	return this
		.replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
		.replace(/\s/g, '')
		.replace(/^(.)/, function($1) { return $1.toLowerCase(); });
	// Autre possibilité :
	// return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(lettre, index) {
	// 	return (index == 0) ? lettre.toLowerCase() : lettre.toUpperCase();
	// }).replace(/\s+/g, '');
}
String.prototype.toPascalCase = function() {
	return this
		.replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
		.replace(/\s/g, '')
		.replace(/^(.)/, function($1) { return $1.toUpperCase(); });
}
String.prototype.reverse = function() {
	return this.split('').reverse().join('');
}
String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
}
String.prototype.isStrInteger = function() {
	return ((parseFloat(this) == parseInt(this)) && !isNaN(this))
}
String.prototype.isStrNumber = function() {
	return (!isNaN(this))
}
/**
 * Remplace la sous-chaîne renvoyée par la fonction substr() par le texte fourni en argument
 * ex:
 * let str = 'le_petit_chaperon_rouge';
 * alert( str.substr(3, 5) )								// affiche 'petit'
 * alert( str.substrReplace('grand', 3, 5) )				// affiche 'le_grand_chaperon_rouge'
 * @param {String} substr Sous-chaîne à insérer
 * @param {Integer} iPosStartInsert Position où commencer l'insertion
 * @param {Integer} iLengthStrToDeleteBeforeInsert Longueur de la chaîne à supprimer avant d'insérer l'autre
 * @return {String} Chaîne avec notre sous-chaîne à la place de l'autre
 */
String.prototype.substrReplace = function(substr = 'YourText', iPosStartInsert = 0, iLengthStrToDeleteBeforeInsert = 0) {
	return (this.substr(0, iPosStartInsert) + substr + this.substr(iPosStartInsert + iLengthStrToDeleteBeforeInsert));
}
/**
 * Remplace la sous-chaîne renvoyée par la fonction substr() par le texte fourni en argument
 * ex:
 * let str = 'le_petit_chaperon_rouge';
 * alert( str.substring(3, 8) )								// affiche 'petit'
 * alert( str.substringReplace('grand', 3, 8) )				// affiche 'le_grand_chaperon_rouge'
 * @param {String} substr Sous-chaîne à insérer
 * @param {Integer} iPosStartInsert Position où commencer l'insertion
 * @param {Integer} iPosEndInsert Position où commence la partie à conserver
 * @return {String} Chaîne avec notre sous-chaîne à la place de l'autre
 */
String.prototype.substringReplace = function(substr = 'YourText', iPosStartInsert = 0, iPosEndInsert = 0) {
	return (this.substr(0, iPosStartInsert) + substr + this.substr(iPosEndInsert));
}
/**
 * Echappe certains caractères d'une chaîne pour qu'ils ne soint pas interprétés par le moteur RegExp JS
 * @return {String} Chaîne échappée
 */
String.prototype.escapeForRegExp = function() {
	let bDebugMode = true;
	S_CARACTERS_TO_ESCAPE = `]\=?^-[$(/)|`;
	let a_sSourceStringCar = this.split('');
	let iIndexCar = 0;
	let sCarStrSource, sResult;
	for (let iIndexSourceStringCar = 0; iIndexSourceStringCar < a_sSourceStringCar.length; iIndexSourceStringCar++) {
		sCarStrSource = a_sSourceStringCar[iIndexSourceStringCar];
		for (let sEscapableCar of S_CARACTERS_TO_ESCAPE) {
			if (sCarStrSource === sEscapableCar) {
				a_sSourceStringCar[iIndexSourceStringCar] = "\\" + a_sSourceStringCar[iIndexSourceStringCar];
			}
		}
		iIndexCar++;
	}
	if (bDebugMode)  { clog('a_sSourceStringCar', (sResult = a_sSourceStringCar.join(''))) };
	return sResult;
}
/**
 * Renvoie le string compris entre deux délimiteurs
 * Attention aux caractères spéciaux des regex à échapper
 * @param {String} sDelimiterStart Délimiteur de début
 * @param {String} sDelimiterEnd Délimiteur de fin
 * @param {!String} sFlags Flags de recherche du regex (ex : 'g' / 'i' / 'gi'...)
 * @param {!Boolean} bGetDetails Renvoie les détails (index...) ou pas    (index : position du 1er caractère du délimiteur de début)
 * @param {!Integer} iMaxLaps Définir un nombre maximum de résultats / tours de boucle (100 par défaut)
 * @param {!Boolean} bResultAlwaysInArray
 */
String.prototype.getStrBetween = function(sDelimiterStart = '(', sDelimiterEnd = ')', sFlags = 'gi', bGetDetails = false, iMaxLaps = 100, bResultAlwaysInArray = false) {
	let bDebugMode = false;
	let sRegex = sDelimiterStart.escapeForRegExp(sDelimiterStart) + '(.*?)' + sDelimiterEnd.escapeForRegExp(sDelimiterEnd);
	let oRegex = new RegExp(sRegex, sFlags);
	if (bDebugMode) { clog('oRegex après init', oRegex) }
	let oResult;
	let aResult = [];
	let iCountLaps = 0;
	if (!oRegex.global) { iMaxLaps = 1; }
	while ((oResult = oRegex.exec(this)) !== null && iCountLaps < iMaxLaps) { // && oRegex.lastIndex !== 0
		if (bDebugMode)  { clog(iCountLaps, oResult, oRegex) };
		aResult.push(Object.assign({}, oResult));
		iCountLaps++;
	}
	if (iCountLaps === iMaxLaps) { console.log(`getStrBetween() : maximum number of laps reached`)}
	if (aResult.length === 1) {
		return (bGetDetails || bResultAlwaysInArray) ? aResult : aResult[0][1];
	} else if (aResult.length > 1) {
		return (bGetDetails) ? aResult : aResult.map(x => x[1]);
	}
}


// -------		ARRAY		-------

/**
 * Renvoie le nombre d'éléments contenus dans l'array
 * @return {Integer} Nombre d'éléments
 */
Array.prototype.count = function() {
	let iNbElements = 0;
	this.forEach(function() {
		iNbElements++;
	})
	return iNbElements;
}
/**
 * Renvoie le nombre d'objets autres que des arrays contenus dans l'array source,
 * Le filtrage concerne les tableaux d'objets :
 * possibilité de filtrer les résultats en ne comptant que les objets internes
 * dont la propriété 'sPropNameToSearch' vaut la valeur 'valueToCompare' (inférieure à, égale, supérieure à...)
 * @param {?String} sPropNameToSearch Nom de la propriété du/des objet(s) qui se trouve(nt) dans l'array et dont on cherchera la valeur
 * @param {?String} sComparisonOperator '===' | '==' | '!==' | '!=' | '<' | '<=' | '>' | '>='
 * @param {?*} valueToCompare Valeur de la propriété à tester
 * @return {Integer} Nombre de colonnes
 */
Array.prototype.countObj = function(sPropNameToSearch = '', sComparisonOperator = '!==', valueToCompare = undefined) {
	let iNbColonnes = 0;
	if (sPropNameToSearch === '') {
		this.forEach(function(valeur) {
			if (typeof(valeur) === 'object') {
				iNbColonnes++;
			}
		})
	} else {
		this.forEach(function(valeur) {
			if (typeof(valeur) === 'object' && !Array.isArray(valeur)) {
				switch (sComparisonOperator) {
					case '===': if (valeur[sPropNameToSearch] === valueToCompare)  { iNbColonnes++; } break;
					case '==' : if (valeur[sPropNameToSearch]  == valueToCompare)  { iNbColonnes++; } break;
					case '!==': if (valeur[sPropNameToSearch] !== valueToCompare)  { iNbColonnes++; } break;
					case '!=' : if (valeur[sPropNameToSearch]  != valueToCompare)  { iNbColonnes++; } break;
					case  '<' : if (valeur[sPropNameToSearch]   < valueToCompare)  { iNbColonnes++; } break;
					case  '<=': if (valeur[sPropNameToSearch]  <= valueToCompare)  { iNbColonnes++; } break;
					case  '>' : if (valeur[sPropNameToSearch]   > valueToCompare)  { iNbColonnes++; } break;
					case  '>=': if (valeur[sPropNameToSearch]  >= valueToCompare)  { iNbColonnes++; }
				}
			}
		})
	}
	return iNbColonnes;
}
/**
 * Transforme un array en objet (uniquement si chaque valeur de l'array est de type 'string')
 * La valeur de chaque élément de l'array devient clé du nouvel objet
 * Celle-ci a pour valeur celle renseignée en argument
 * @param {*} valueOfEachProperty
 */
Array.prototype.toObject = function(valueOfEachProperty = {}) {
	let oResult = {};
	this.forEach(function(valArray) {
		//todo: uniquement si string
		oResult[valArray] = valueOfEachProperty;
	})
	return oResult;
}
/**
 * Renvoie le nombre d'éléments de chaques types contenus dans l'array
 * possibilité de différentier les objets 'array'/'date'/'regex' des autres objets
 * @param {?Array[String]|String} filterType Type des objets qui se trouvent dans l'array que l'on veut compter
 * @param {?Boolean} bDifferentiateObjects Si true, les 'array'/'date'/'regex' apparaitront tels quels et non comme 'object'
 * @param {?Boolean} bHideIfNull Si true, les objets d'un type dont il n'y a aucune occurence n'apparaitront pas dans les résultats
 * @return {Integer|Object{Integer}} Integer si un seul filtre de type | Objet{Integer} sinon
 */
Array.prototype.countType = function(filterType = null, bDifferentiateObjects = true, bCheckDetails = true, bHideIfNull = true, bAlwaysResultInObject = false) {
	let oTypeFound = A_TYPES.toObject(0);
	let aObjectType = ['array', 'date', 'regex'];
	let oResult = {};
	// filterType --> vers Array()
	if (filterType === '' || filterType === null || filterType == undefined) {
		filterType = [];
	} else if (typeof(filterType) === 'string') {
		filterType = [filterType];
	}
	if ( (!Array.isArray(filterType)) ) {
		displayError();
	}
	// vérifie si (types d'objets spécifiés === array ou/et date ou/et regex)
	filterType.forEach(function(sType) {
		if (typeof(sType) === 'string') {
			if (aObjectType.indexOf(sType) >= 0) {
				bDifferentiateObjects = true;
			}
		} else {
			displayError();
		}
	})
	// si (bDifferentiateObjects) { ajoute les formats d'objets spécifiques à oTypeFound }
	if (bDifferentiateObjects) {
		aObjectType.forEach(function(sTypeObjet) {
			oTypeFound[sTypeObjet] = 0;
		})
	}
	this.forEach(function(valeur) {
		if (typeof(valeur) === 'object' && bDifferentiateObjects) {
			// todo... regex + date
			if (Array.isArray(valeur)) {
				oTypeFound['array']++;
			} else {
				oTypeFound[typeof(valeur)]++;
			}
		} else {
			oTypeFound[typeof(valeur)]++;
		}
	})
	if (bHideIfNull) {
		Object._deletePropIf(oTypeFound, '', '===', 0);
	}
	// renvoie le résultat sous forme d'entier si un seul type trouvé, ou d'array si plusieurs
	if (!bAlwaysResultInObject && filterType[1] == undefined && filterType[0] !== undefined) {
		return oTypeFound[filterType[0]] ?? 0;
	} else {
		// on ne renvoie que les résultats correspondant aux types demandés dans filterType
		oResult = filterType.length ?
			Object._filterProp(oTypeFound, filterType) :
			oTypeFound;
		return oResult;
	}
	function displayError() {
		throw new TypeError(`countType() ---> filterType n'accepte que des formats transmis en string | array de strings parmi la liste suivante :
			'number', 'bigint',	'string', 'boolean', 'symbol', 'undefined',	'function',	'object', 
			et 'array', 'date', 'regex'`);
	}
}
/**
 * Vérifie si l'objet ne contient que le(s) type(s) fournis en argument
 * @param {String|[String]} typeContent Type(s) à vérifier
 * @param {?Boolean} bDifferentiateObjects Si true, les 'array'/'date'/'regex' apparaitront tels quels et non comme 'object'
 * @param {Boolean} bMustContainAllTypesProvided Si true, la fonction renverra false si TOUS les types fournis en argument n'ont pas été trouvés dans l'array
 * @return {Boolean} true si l'array ne contient pas des valeurs d'autres types que les types demandés
 */
Array.prototype.containsOnlyType = function(typeContent = '', bDifferentiateObjects = true, bMustContainOnlyAllTypesProvided = true) {
	let iCountAll, iCountType = 0, oCountType;
	if (typeof(typeContent) === 'string')  { typeContent = [typeContent]; }
	iCountAll = this.count();
	oCountType = this.countType(typeContent, bDifferentiateObjects, true, true, true);
	typeContent.forEach(function(sType) {
		iCountType = iCountType + oCountType[sType];
		delete oCountType[sType];
	})
	if (bMustContainOnlyAllTypesProvided && Object.keys(oCountType).length > 0)  {
		return false;
	}
	return (iCountType === iCountAll);
}
Array.prototype.exeFct = function(val = '') {
	let oChaineEntreAccolades = this.getStrBetween('#{', '}', '', true);
	clog('oChaineEntreAccolades', oChaineEntreAccolades);
	let oResultats;
	let	sNomFonction = this[0];
	let sArgs = '';
	let iIndexArg = 0;
	this.splice(0, 1);
	let iNbArgs = this.length;
	this.forEach(function(argum) {
		iIndexArg++;
		sArgs += JSON.stringify(argum);
		if (iIndexArg < iNbArgs) { sArgs += ', ' }
	})
	sCommande = `${sNomFonction}(${sArgs})`;
	// Remplace '#{0}' par le(s) argument(s) fournis
	// if ((oResultats = regexDélimiteurs.exec(sCommande)) != null) {
	// 	regexDélimiteurs.lastIndex = 0;
	// 	let iPosDbtSousChaine, iPosDbtFormat, iPosFinFormat, iPosFinSousChaine, sChaineEntreAccolades, sSousDonnéeFormatée;
	// 	// let iTailleSousChaine, iTailleFormat, iTailleDonnéeInsérée,
	// 	sDonnéeFormatée = sCommande;
	// 	while ((oResultats = regexDélimiteurs.exec(sCommande)) != null) {
	// 		iPosDbtSousChaine = oResultats.index;
	// 		iPosDbtFormat = iPosDbtSousChaine + 2;
	// 		iPosFinFormat = iPosDbtFormat + oResultats[1].length;
	// 		iPosFinSousChaine = iPosFinFormat + 1;
	// 		sChaineEntreAccolades = oResultats[1];
	// 		//sSousDonnéeFormatée = obtDonnéeFormatée(donnéeBrute, sChaineEntreAccolades, valeurSiIndéfini);
	// 		sDonnéeFormatée = sDonnéeFormatée.replace(/#{(.*?)}/, 'iii');
	// 	}
	// 	return sDonnéeFormatée;
	// }
	clog('sArgs:   ', sArgs, '      ---      ', 'sCommande:   ', sCommande)
	return eval(  `${sCommande}`  );
}
Array.prototype.getKeyObjInArrayWhere = function(sPropNameToSearch = '', sComparisonOperator = '!==', valueToCompare = undefined, bIncludingArrays = false) {
	let aArray = this;
	let oInArray, sObjInArrayType;
	let aKeys = Object.keys(aArray);
	let iKeys = aKeys.length;
	let iKey;
	for (let i = 0; i < iKeys; i++) {
		iKey = aKeys[i];
		oInArray = aArray[iKey];
		sObjInArrayType = typeOf(oInArray);
		if (bIncludingArrays && sObjInArrayType === 'array')  { sObjInArrayType = 'object'; }
		if (sObjInArrayType !== 'object')  { continue; }
		switch (sComparisonOperator) {
			case '===': if (oInArray[sPropNameToSearch] === valueToCompare)  { return iKey; } break;
			case '==' : if (oInArray[sPropNameToSearch]  == valueToCompare)  { return iKey; } break;
			case '!==': if (oInArray[sPropNameToSearch] !== valueToCompare)  { return iKey; } break;
			case '!=' : if (oInArray[sPropNameToSearch]  != valueToCompare)  { return iKey; } break;
			case  '<' : if (oInArray[sPropNameToSearch]   < valueToCompare)  { return iKey; } break;
			case  '<=': if (oInArray[sPropNameToSearch]  <= valueToCompare)  { return iKey; } break;
			case  '>' : if (oInArray[sPropNameToSearch]   > valueToCompare)  { return iKey; } break;
			case  '>=': if (oInArray[sPropNameToSearch]  >= valueToCompare)  { return iKey; }
		}
	}
}
/**
 * Pour un array contenant des objets, renvoie la valeur d'une propriété pour chaque objet, si une condition est remplie dans cet objet
 * @param {String} sPropNameToSearch
 * @param {String} sPropNameToCompare
 * @param {String} sComparisonOperator
 * @param {*} valueToCompare
 * @param {Boolean} bIncludingArrays
 * @return {Array}
 */
Array.prototype.getPropFromAllObjectsInArrayWhere = function(sPropNameToSearch = '', sPropNameToCompare, sComparisonOperator = '!==', valueToCompare = undefined, bIncludingArrays = false) {
	let aArray = this;
	let oInArray, sObjInArrayType;
	let aKeys = Object.keys(aArray);
	let iKeys = aKeys.length;
	let iKey;
	let aResult = [];
	for (let i = 0; i < iKeys; i++) {
		iKey = aKeys[i];
		oInArray = aArray[iKey];
		sObjInArrayType = typeOf(oInArray);
		if (bIncludingArrays && sObjInArrayType === 'array')  { sObjInArrayType = 'object'; }
		if (sObjInArrayType !== 'object')  { continue; }
		switch (sComparisonOperator) {
			case '===': if (oInArray[sPropNameToCompare] === valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case '==' : if (oInArray[sPropNameToCompare]  == valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case '!==': if (oInArray[sPropNameToCompare] !== valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case '!=' : if (oInArray[sPropNameToCompare]  != valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case  '<' : if (oInArray[sPropNameToCompare]   < valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case  '<=': if (oInArray[sPropNameToCompare]  <= valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case  '>' : if (oInArray[sPropNameToCompare]   > valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); } break;
			case  '>=': if (oInArray[sPropNameToCompare]  >= valueToCompare)  { aResult.push(oInArray[sPropNameToSearch]); }
		}
	}
	return aResult;
}

// -------		OBJECT		-------

Object._containsOnlyType = function(obj, typeContent = '', bDifferentiateObjects = true, bMustContainOnlyAllTypesProvided = true) {
	let aProps = Object.values(obj);
	return aProps.containsOnlyType(typeContent, bDifferentiateObjects, bMustContainOnlyAllTypesProvided);
}

Object._count = function(obj) {
	let aProps = Object.values(obj);
	return aProps.count();
}

Object._countObj = function(obj, sPropNameToSearch = '', sComparisonOperator = '!==', valueToCompare = undefined) {
	let aProps = Object.values(obj);
	return aProps.countObj(sPropNameToSearch, sComparisonOperator, valueToCompare);
}

Object._countType = function(obj, filterType = null, bDifferentiateObjects = true, bCheckDetails = true, bHideIfNull = true, bAlwaysResultInObject = false) {
	let aProps = Object.values(obj);
	return aProps.countType(filterType, bDifferentiateObjects, bCheckDetails, bHideIfNull, bAlwaysResultInObject);
}

/**
 * Supprime la propriété 'sPropName' d'un objet si sa valeur est....
 * Possibilité de cibler Toutes les propriétés de l'objet en mettant 'sPropName' à ''
 * @param {Object} obj Objet concerné
 * @param {?String} sPropName Nom de la propriété qui sera peut-être supprimée (ex: 'maProp')
 * @param {?String} sComparisonOperator Opérateur de comparaison ('===' | '==' | '!==' | '!=' | '<' | '<=' | '>' | '>=')
 * @param {?String} valueToCompare Valeur qui entraînera la suppression (ex: undefined, ''). '' par défaut
 */
Object._deletePropIf = function(obj, sPropName = '', sComparisonOperator = '===', valueToCompare = '') {
	let bAllPropsAreConcerned = false;
	if (Array.isArray(obj))  { return; }
	if (sPropName === '' || sPropName == undefined || sPropName === null) {
		bAllPropsAreConcerned = true;
	}
	Object.keys(obj).forEach(function(key) {
		if (bAllPropsAreConcerned === true || key === sPropName) {
			switch (sComparisonOperator) {
				case '===': if (obj[key] === valueToCompare)  { delete obj[key]; } break;
				case '==' : if (obj[key]  == valueToCompare)  { delete obj[key]; } break;
				case '!==': if (obj[key] !== valueToCompare)  { delete obj[key]; } break;
				case '!=' : if (obj[key]  != valueToCompare)  { delete obj[key]; } break;
				case  '<' : if (obj[key]   < valueToCompare)  { delete obj[key]; } break;
				case  '<=': if (obj[key]  <= valueToCompare)  { delete obj[key]; } break;
				case  '>' : if (obj[key]   > valueToCompare)  { delete obj[key]; } break;
				case  '>=': if (obj[key]  >= valueToCompare)  { delete obj[key]; }
			}
		}
	})
	return Object.keys.length;
}

/**
 * Permet de copier certaines propriétés d'un objet selon des conditions selon leur valeur
 * @param {Object} obj Objet source dont on veut copier uniquement certaines propriétés vers un nouvel objet
 * @param {? [ [sNomProp1, '!==', undefined],
 * 			   [sNomProp2, '>', 26] ] } aPropsFilterConfig Array de configuration du filtrage des propriétés
 * • Possibilité de fournir uniquement le nom des propriétés qu'on veut copier
 * [sNomProp1, sNomProp2] MAIS dans ce cas seules les propriétés sNomProp1 et sNomProp2 seront filtrées si elles existent
 * sans aucune condition de comparaison de valeur
 * • Ou possibilité de préciser les conditions qui entrainent ou non la copie
 * @return Nouvel objet avec les propriétés filtrées
 *
 * • Exemples :
 * objSource = {
 * 		nom: 'toto',
 * 		prenom: 'rémi',
 * 		genre: 'homme',
 * 		age: 24};
 *
 * ◘ Ex1:
 * let resultat1 = Object._filterProp(objSource, ['prenom', 'age']);
 * resultat1 --> {prenom: 'rémi', age: 24}
 *
 * ◘ Ex2:
 * let aConfig2 = [ ['nom', '!==', undefined],
 * 					['prenom', '===', 'francky'],
 * 					['age', '>', 15] ];
 * let resultat2 = Object._filterProp(objSource, aConfig2);
 * resultat2 --> {nom: 'toto', genre: 'homme'}
 */
Object._filterProp = function(obj, aPropsFilterConfig) {
	var oResult = {};
	// todo : vérif que propFilter === arr.arr OU arr.str
	Object.keys(obj).forEach(function(sKeyObj) {
		aPropsFilterConfig.forEach(function(aPropConfig) {
			if (typeof(aPropConfig) === 'string')  { aPropConfig = [ aPropConfig, '!==', undefined]; }
			if (sKeyObj === aPropConfig[0]) {
				switch (aPropConfig[1]) {
					case '===': if (obj[sKeyObj] === aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case '==' : if (obj[sKeyObj]  == aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case '!==': if (obj[sKeyObj] !== aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case '!=' : if (obj[sKeyObj]  != aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case  '<' : if (obj[sKeyObj]   < aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case  '<=': if (obj[sKeyObj]  <= aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case  '>' : if (obj[sKeyObj]   > aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					case  '>=': if (obj[sKeyObj]  >= aPropConfig[2])  { oResult[sKeyObj] = obj[sKeyObj]; } break;
					default: throw new SyntaxError(`_filterProp() ---> l'opérateur de comparaison fourni dans aPropsFilterConfig n'est pas reconnu. Vous devez en fournir un parmi la liste suivante :
						'===', '==', '!==', '!=', '<', '<=', '>', '>='`);
				}
			}
		})
	})
	return oResult;
}

/**
 * Copy properties from oSource to oTarget, without overwriting existing properties
 * @param {Object} oTarget Target object which will be enriched with the properties of oSource
 * @param {Object} oSource Source object having the properties to copy
 * @return {Object} Returns oTarget
 */
Object._assignWithoutCrush = function(oTarget, oSource) {
	if (typeOf(oTarget) !== 'object' || typeOf(oTarget) !== 'object')  {
		throw new TypeError(`oTarget et oSource doivent être des objets ayant des propriétés énumérables.`)
	}
	Object.keys(oSource).forEach(function(key) {
		if (!oTarget.hasOwnProperty(key)) {
			oTarget[key] = oSource[key];
		}
	})
	return oTarget;
}


// -------		NUMBER		-------

Number.prototype.separateThousands = function(sSeparator = ' ') {
	var sNombre = ''+this.valueOf();
	var sRetour = '';
	var count=0;
	for(var i=sNombre.length-1 ; i>=0 ; i--)
	{
		if(count!=0 && count % 3 == 0)
			sRetour = sNombre[i]+sSeparator+sRetour ;
		else
			sRetour = sNombre[i]+sRetour ;
		count++;
	}
	return sRetour;
}
Number.prototype.enLettres = function() {
	let iNombre = this.valueOf();
	if (isNaN(iNombre) || iNombre < 0 || 999 < iNombre) {
		return 'Veuillez entrer un nombre entier compris entre 0 et 999.';
	}
	var units2Letters = ['', 'un', 'deux', 'trois', 'quatre', 'cinq', 'six', 'sept', 'huit', 'neuf', 'dix', 'onze', 'douze', 'treize', 'quatorze', 'quinze', 'seize', 'dix-sept', 'dix-huit', 'dix-neuf'],
		tens2Letters = ['', 'dix', 'vingt', 'trente', 'quarante', 'cinquante', 'soixante', 'soixante', 'quatre-vingt', 'quatre-vingt'];
	var units = iNombre % 10,
		tens = (iNombre % 100 - units) / 10,
		hundreds = (iNombre % 1000 - iNombre % 100) / 100;
	var unitsOut, tensOut, hundredsOut;
	if (iNombre === 0) {
		return 'zéro';
	} else {
		// Traitement des unités
		unitsOut = (units === 1 && tens > 0 && tens !== 8 ? 'et-' : '') + units2Letters[units];
		// Traitement des dizaines
		if (tens === 1 && units > 0) {
			tensOut = units2Letters[10 + units];
			unitsOut = '';
		} else if (tens === 7 || tens === 9) {
			tensOut = tens2Letters[tens] + '-' + (tens === 7 && units === 1 ? 'et-' : '') + units2Letters[10 + units];
			unitsOut = '';
		} else {
			tensOut = tens2Letters[tens];
		}
		tensOut += (units === 0 && tens === 8 ? 's' : '');
		// Traitement des centaines
		hundredsOut = (hundreds > 1 ? units2Letters[hundreds] + '-' : '') + (hundreds > 0 ? 'cent' : '') + (hundreds > 1 && tens == 0 && units == 0 ? 's' : '');
		// Retour du total
		return hundredsOut + (hundredsOut && tensOut ? '-' : '') + tensOut + (hundredsOut && unitsOut || tensOut && unitsOut ? '-' : '') + unitsOut;
	}
}


// -------		DATE		-------

Date.prototype.AA = function() {
	let sDonnéeFormatée = '' + this.AAAA();
	return sDonnéeFormatée.substr(2, 4);
}
Date.prototype.AAAA = function() {
	return this.getFullYear();
}
Date.prototype.J = function() {
	return this.getDate();
}
Date.prototype.JJ = function() {
	let sDonnéeFormatée = '' + this.J();
	if (sDonnéeFormatée.length === 1) { sDonnéeFormatée = '0' + sDonnéeFormatée; }
	return sDonnéeFormatée;
}
/**
 * Renvoie le jour de la semaine en lettres et en abbrégé
 * @param {?String} sLangue Langue souhaitée (parmi 'fr', 'en'/'us'/'uk')
 * @param {?String} sFormat Formatage du texte (parmi 'toUpperCaseFirstLetter', 'toUpperCase', 'toLowerCase')
 * @param {?'auto'|TinyInt} iNbCaractères Nombre de caractères de l'abbrégé
 * @return {String}
 */
Date.prototype.JJJ = function(sLangue = 'fr', sFormat = 'toUpperCaseFirstLetter', iNbCaractères = 'auto') {		// sLangue = 'fr' / 'uk' / 'us'
	let iJourSemaineChiffre = this.getDay();
	let oNbCaractères = {};
	oNbCaractères.fr = [3, 3, 3, 5, 3, 5, 3]; 	// ou 3 partout
	// us : 3 partout
	// uk : 3 partout (sans point au bout)
	let sDonnéeFormatée = this.JJJJ(sLangue, sFormat);
	if (iNbCaractères === 'auto') {
		switch (sLangue) {
			case 'en':
			case 'us':
			case 'uk': 			iNbCaractères = 3; break;
			default:			iNbCaractères = oNbCaractères[sLangue][iJourSemaineChiffre];
		}
	} else {
		if (iNbCaractères >= sDonnéeFormatée.length)  { return sDonnéeFormatée; }
	}
	sDonnéeFormatée = sDonnéeFormatée.substr(0, iNbCaractères);
	if (sLangue !== 'uk')  { sDonnéeFormatée += '.' }
	return sDonnéeFormatée;
}
Date.prototype.JJJJ = function(sLangue = 'fr', sFormat = 'toUpperCaseFirstLetter') {	// sLangue = 'fr' / 'uk' / 'us'
	let iNumJourSemaine = this.getDay();
	if (iNumJourSemaine == 0) { if (sLangue === 'fr') { sDonnéeFormatée = 'dimanche' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'sunday' } }
	if (iNumJourSemaine == 1) { if (sLangue === 'fr') { sDonnéeFormatée = 'lundi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'monday' } }
	if (iNumJourSemaine == 2) { if (sLangue === 'fr') { sDonnéeFormatée = 'mardi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'tuesday' } }
	if (iNumJourSemaine == 3) { if (sLangue === 'fr') { sDonnéeFormatée = 'mercredi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'wednesday' } }
	if (iNumJourSemaine == 4) { if (sLangue === 'fr') { sDonnéeFormatée = 'jeudi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'thursday' } }
	if (iNumJourSemaine == 5) { if (sLangue === 'fr') { sDonnéeFormatée = 'vendredi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'friday' } }
	if (iNumJourSemaine == 6) { if (sLangue === 'fr') { sDonnéeFormatée = 'samedi' } else if (sLangue === 'uk'  || 'us' || 'en') { sDonnéeFormatée = 'saturday' } }
	switch (sFormat) {
		case 'toUpperCase' : sDonnéeFormatée = sDonnéeFormatée.toUpperCase(); break;
		case 'toUpperCaseFirstLetter' : sDonnéeFormatée = sDonnéeFormatée.capitalize();
	}
	return sDonnéeFormatée;
}
Date.prototype.M = function() {
	return this.getMonth() + 1;
}
Date.prototype.MM = function() {
	let sDonnéeFormatée = '' + this.M();
	if (sDonnéeFormatée.length === 1) { sDonnéeFormatée = '0' + sDonnéeFormatée; }
	return sDonnéeFormatée;
}
Date.prototype.MMM = function(sLangue = 'fr', sFormat = 'toUpperCaseFirstLetter', iNbCaractères = 'auto', sSéparateur = '.') {	// sLangue = 'fr' / 'uk' / 'us'
	let oNbCaractères = {};
	if (sLangue === 'en')  { sLangue = 'us' }
	oNbCaractères.fr = [null, 4, 4, 0, 3, 0, 0, 5, 0, 4, 3, 3, 3];
	oNbCaractères.us = [null, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3];
	oNbCaractères.uk = [null, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3];
	let sDonnéeFormatée = this.MMMM(sLangue, sFormat);
	if (iNbCaractères === 'auto')  { iNbCaractères = oNbCaractères[sLangue][this.M()]; }
	if (iNbCaractères === 0) {
		return sDonnéeFormatée;
	} else if (iNbCaractères >= sDonnéeFormatée.length) {
		return sDonnéeFormatée;
	}
	sDonnéeFormatée = sDonnéeFormatée.substr(0, iNbCaractères);
	if (sLangue !== 'uk')  { sDonnéeFormatée += sSéparateur }
	return sDonnéeFormatée;

}
Date.prototype.MMMM = function(sLangue = 'fr', sFormat = 'toUpperCaseFirstLetter') {	// sLangue = 'fr' / 'uk' / 'us'
	let sDonnéeFormatée, iM = this.M();
	if (iM == 1) { if (sLangue === 'fr') { sDonnéeFormatée = 'janvier' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'january' } }
	if (iM == 2) { if (sLangue === 'fr') { sDonnéeFormatée = 'février' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'february' } }
	if (iM == 3) { if (sLangue === 'fr') { sDonnéeFormatée = 'mars' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'march' } }
	if (iM == 4) { if (sLangue === 'fr') { sDonnéeFormatée = 'avril' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'april' } }
	if (iM == 5) { if (sLangue === 'fr') { sDonnéeFormatée = 'mai' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'may' } }
	if (iM == 6) { if (sLangue === 'fr') { sDonnéeFormatée = 'juin' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'june' } }
	if (iM == 7) { if (sLangue === 'fr') { sDonnéeFormatée = 'juillet' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'july' } }
	if (iM == 8) { if (sLangue === 'fr') { sDonnéeFormatée = 'août' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'august' } }
	if (iM == 9) { if (sLangue === 'fr') { sDonnéeFormatée = 'septembre' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'september' } }
	if (iM == 10) { if (sLangue === 'fr') { sDonnéeFormatée = 'octobre' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'october' } }
	if (iM == 11) { if (sLangue === 'fr') { sDonnéeFormatée = 'novembre' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'november' } }
	if (iM == 12) { if (sLangue === 'fr') { sDonnéeFormatée = 'décembre' } else if (sLangue === 'uk' || 'us' || 'en') { sDonnéeFormatée = 'december' } }
	switch (sFormat) {
		case 'toUpperCase' : sDonnéeFormatée = sDonnéeFormatée.toUpperCase(); break;
		case 'toUpperCaseFirstLetter' : sDonnéeFormatée = sDonnéeFormatée.capitalize();
	}
	return sDonnéeFormatée;
}
Date.prototype.AAAAMMJJ = function(sSeparator = '-') {
	return this.AAAA() + sSeparator + this.MM() + sSeparator + this.JJ();
}
Date.prototype.JJMMAAAA = function(sSeparator = '/') {
	return this.JJ() + sSeparator + this.MM() + sSeparator + this.AAAA();
}



// -------		JQUERY		-------

$.fn.toggleAttr = function(attr, val) {
	var test = $(this).attr(attr);
	if ( test ) {
		// if attrib exists with ANY value, still remove it
		$(this).removeAttr(attr);
	} else {
		$(this).attr(attr, val);
	}
	return this;
};
$.fn.toggleAttrVal = function(attr, val1, val2) {
	var test = $(this).attr(attr);
	if ( test === val1) {
		$(this).attr(attr, val2);
		return this;
	}
	if ( test === val2) {
		$(this).attr(attr, val1);
		return this;
	}
	// default to val1 if neither
	$(this).attr(attr, val1);
	return this;
};
function isJQueryElement(elemt) {
	if (typeOf(elemt) !== 'object')  { return false; }
	if (!elemt.hasOwnProperty('prevObject'))  { return false; }
	if (!Object.getPrototypeOf(elemt).hasOwnProperty('jquery'))  { return false; }
	if (Object.keys(elemt).length === 3)  { return true; }
}


// -------		FUNCTION		-------

/**
 * Renvoie le type de l'élément en précisant 'array' si tableau
 * @param {Variant} element Elément concerné
 * @return {String} Type détaillé
 */
function typeOf(element) {
	return (Array.isArray(element) ? 'array' : typeof(element));
}

function clog(...args) {
	console.log(...args);
}

function getDomElemtValue(domElement) {
	if (isJQueryElement(domElement))  { domElement = domElement[0]; }
	let sNodeName = domElement.nodeName;
	let sType = domElement.type;
	if (sNodeName === 'INPUT') {
		if (sType === 'checkbox' || sType === 'radio')  {
			return domElement.checked;
		} else {
			return domElement.value;
		}
	} else if ( sNodeName === 'SELECT' 	||
		sNodeName === 'TEXTAREA' ) 	{
		return domElement.value;
	}
	return domElement.value;
}

// -------		CLASS		-------

class Checkbox {
	static oDefaultAttr = {
		type:			'checkbox',
		id:				'',
		name:			'',
		class:			'',
		checked:		false
	}
	static html(oAttr = Checkbox.oDefaultAttr, sLabel = '') {
		// <input type="checkbox" id="scales" name="scales" checked>
		// <label for="scales">Scales</label>
		ajouterPropriétésObj(oAttr, Checkbox.oDefaultAttr);
		let sHtml = ``;
		let sLabelFor, sChecked;
		if (!Checkbox.isConfigOk(oAttr))  {
			Checkbox.displayError('config', 'bad_type');
		}
		// remove null or false attributes
		Object._deletePropIf(oAttr, '', '===', '');
		Object._deletePropIf(oAttr, '', '===', false);
		// !name ?   -->   name = id
		if ((oAttr.name == undefined || oAttr.name === '') && (typeof(oAttr.id) === 'string' && oAttr.id.length))  { oAttr.name = oAttr.id }
		// html code generation
		if (typeof(sLabel) === 'string' && sLabel !== '') {
			sLabelFor = (oAttr.name != undefined && oAttr.name !== '') ?
				` for="${oAttr.name}"`
				: ``;
			sHtml += `<label${sLabelFor}>${sLabel}</label>\n`;
		}
		sHtml += `<input`;
		Object.keys(oAttr).forEach(function(sAttrName) {
			if (typeof(sAttrName) === 'string') {
				sHtml += ` ${sAttrName}='${oAttr[sAttrName]}'`;
			}
		})
		// checked ?
		sHtml += (oAttr.checked) ? ` checked` : ``;
		sHtml += `>`;
		return sHtml;
	}
	static isConfigOk(oConfig) {
		let aPossibleTypeFormats;
		if (typeOf(oConfig) !== 'object')  { return false; }
		// retrieves the types of possible values ​​for this type of input
		aPossibleTypeFormats = Object.keys(Object._countType(Checkbox.oDefaultAttr));
		if (!Object._containsOnlyType(Checkbox.oDefaultAttr, aPossibleTypeFormats))  {
			Checkbox.displayError('attr', 'bad_type', oConfig.type);
		}
		return true;
	}
	static getDefaultAttr(sType) {
		return Input.oDefaultAttr;
	}
	static showHelpInConsole() {
		console.log('Input : utilisation : ', {
			'Checkbox.html()': `Renvoie le code html d'un checkbox avec la config par défaut.`,
			'Checkbox.getDefaultAttr("email")': `Renvoie la valeur par défaut de chaque attributs pour un input de type checkbox.`
		});
	}
	static displayError(errorElement, errorDetail) {
		let sMessage = '', errorCategory = Error;
		if (errorElement === 'input_type') {
			if (errorDetail === 'not_exist') {
				sMessage = `Le type d'input fourni est incorrect (${arguments[3]} fourni).`;
			}
		} else if (errorElement === 'attr') {
			if (errorDetail === 'bad_type') {
				sMessage = `Au moins un attribut est de type différent de ceux présents dans la config par défaut (Input.oDefaultAttr.${arguments[3]}).`;
			}
		} else {
			sMessage = 'Erreur inconnue.';
		}
		Enum.showHelpInConsole();
		throw new errorCategory(sMessage);
	}
}
class Input {
	// default attributes based on element type
	static oDefaultAttr = {
		text:		{
			type:			'text',
			id:				'',
			name:			'',
			class:			'',
			minlength:		'',
			maxlength:		'',
			size:			'',
			pattern:		'',
			placeholder:	'',
			readonly:		'',
			spellcheck:		false,
			required:		false,
			//autocorrect:		false,
			//mozactionhint:	false,
		},
		number:		{
			type:			'number',
			id:				'',
			name:			'',
			class:			'',
			value:			'',
			min:			'',
			max:			'',
			placeholder:	'',
			readonly:		'',
			step:			'',
			required:		false,
		},
		date:		{
			type:			'date',
			id:				'',
			name:			'',
			class:			'',
			value:			'',
			min:			'',
			max:			'',
			step:			'',
			required:		false,
		}
	}
	static oConfig = {
		sHtmlBeforeAttr: 	`<input`,
		sHtmlAfterAttr: 	`>`
	}
	static html(sType = 'text', oAttr = undefined, sLabel = '') {
		let sHtml = ``, sLabelFor;
		if (!Input.doesTypeExist(sType)) {
			Input.displayError('input_type', 'not_exist', sType);
		}
		if (oAttr == undefined || oAttr === '') {
			oAttr = Object.assign( {}, Input.oDefaultAttr[sType] );
		} else if (typeOf(oAttr) === 'object') {
			oAttr = Object._assignWithoutCrush(oAttr, Input.oDefaultAttr[sType]);
		}
		if (!Input.isConfigOk(oAttr))  {
			Input.displayError('config', 'bad_type');
		}
		// remove null or false attributes
		Object._deletePropIf(oAttr, '', '===', '');
		Object._deletePropIf(oAttr, '', '===', false);
		// set name since id if undefined
		if (oAttr.id != undefined && oAttr.id !== '') {
			if (oAttr.name == undefined || oAttr.name === '')  { oAttr.name = oAttr.id; }
		}
		// html code generation
		if (typeof(sLabel) === 'string' && sLabel !== '') {
			sLabelFor = (oAttr.name != undefined && oAttr.name !== '') ?
				` for="${oAttr.name}"`
				: ``;
			sHtml += `<label${sLabelFor}">${sLabel}</label>\n`;
		}
		sHtml += Input.oConfig.sHtmlBeforeAttr;
		Object.keys(oAttr).forEach(function(sAttrName) {
			if (typeof(sAttrName) === 'string') {
				sHtml += ` ${sAttrName}="${oAttr[sAttrName]}"`;
			}
		})
		sHtml += Input.oConfig.sHtmlAfterAttr;
		return sHtml;
	}
	static doesTypeExist(sType) {
		return (Input.oDefaultAttr[sType] !== undefined);
	}
	static isConfigOk(oConfig) {
		let aPossibleInputTypeFormats;
		if (typeOf(oConfig) !== 'object')  { return false; }
		if (!Input.doesTypeExist(oConfig.type))  { return false; }
		// retrieves the types of possible values ​​for this type of input
		aPossibleInputTypeFormats = Object.keys(Object._countType(Input.oDefaultAttr[oConfig.type]));
		if (!Object._containsOnlyType(Input.oDefaultAttr[oConfig.type], aPossibleInputTypeFormats))  {
			Input.displayError('attr', 'bad_type', oConfig.type);
		}
		return true;
	}
	static getDefaultAttr(sType) {
		if (!Input.doesTypeExist(sType)) {
			Input.displayError('input_type', 'not_exist', sType);
		}
		return Input.oDefaultAttr[sType];
	}
	static showHelpInConsole() {
		console.log(`Input : utilisation : `, {
			"Input.html()": `Renvoie le code html d'un input de type texte avec la config par défaut.`,
			"Input.html('text', 'Ma super valeur', oMesAttributs)": `Renvoie le code html d'un input de type texte avec ma config personnalisée (oMesAttributs), et la valeur 'Ma super valeur' par défaut dans l'input.`,
			"Input.html('date')": `Renvoie le code html d'un input de type date avec la config par défaut.`,
			"Input.getDefaultAttr('email')": `Renvoie la valeur par défaut de chaque attributs pour un input de type email.`
		});
		console.log(`A faire : \n`, {
			todo: `créer un obj contenant la config par défaut pour les autres types d'input (on été traités seulement 'text', 'date', 'number')`
		})
	}
	static displayError(errorElement, errorDetail) {
		let sMessage = errorDetail + '\n', errorCategory = Error;
		if (errorElement === 'input_type') {
			if (errorDetail === 'not_exist') {
				sMessage += `Le type d'input fourni est incorrect (${arguments[3]} fourni).\n`;
			}
		} else if (errorElement === 'attr') {
			if (errorDetail === 'bad_type') {
				sMessage += `Au moins un attribut est de type différent de ceux présents dans la config par défaut (Input.oDefaultAttr.${arguments[3]}).\n`;
			}
		} else {
			sMessage += 'Erreur inconnue.\n';
		}
		Input.showHelpInConsole();
		throw new errorCategory(sMessage);
	}
}
/**
 * Classe permettant de définir les valeurs possibles pour un objet Enum (dont on passe l'id au constructeur)
 * Et éventuellement des propriétés supplémentaires que l'on pourra récupérer facilement
 *
 * Config contenant les valeurs acceptées par les objets Enum liés à l'id de cet objet
 * doit être au format Array [], plusieurs inclusions possibles :

 * ◘◘ format Object {}	 // Permet de définir la valeur d'affichage, et autant de propriétés que l'on souhaite et
 * 						    offre l'avantage de pouvoir définir un nom de clé pour chaque propriété
 *
 * ex :
 * oSexeEnumDef = [
 *		{ base: 'h',		display: 'homme',		description: 'un monsieur' },
 *		{ base: 'f',		display: 'femme',		description: 'une madame' }
 *  ]
 *
 * oSexeEnum.val = 'f'						// oSexeEnum a pour valeur sélectionne la deuxième ligne (femme)
 * clog( oSexeEnum.val )					// affiche 'f'
 * clog( oSexeEnum.val('') )				// affiche 'f'
 * clog( oSexeEnum.val('base') )			// affiche 'f'
 * clog( oSexeEnum.val(0) )					// affiche 'f'
 * clog( oSexeEnum.val(1) )					// affiche 'femme'
 * clog( oSexeEnum.val(2) )					// affiche 'une madame'
 * clog( oSexeEnum.val('display') )			// affiche 'femme'
 * clog( oSexeEnum.val('description') )		// affiche 'une madame'


 * ◘◘ format Array []	 // Idem mais ne permet pas de définir de clé (ex: 'description')
 *
 * oSexeEnumDef = [
 *		['h', 'homme', 'un monsieur'],
 *		['f', 'femme', 'une madame']
 * ]
 *
 * ex :
 * oSexeEnum.val = 'f'						// oSexeEnum a pour valeur sélectionne la deuxième ligne (femme)
 * clog( oSexeEnum.val(2) )					// affiche 'une madame'
 * clog( oSexeEnum.val('display') )			// erreur... !
 * clog( oSexeEnum.val('description') )		// erreur... !


 * ◘◘ format String ''	 // Idem mais ne permet pas de définir de valeur d'affichage ni aucune propriété
 *

 * oSexeEnumDef = [
 *		'h',
 *		'f'
 * ]
 *
 * ex :
 * oSexeEnum.val = 'f'						// oSexeEnum a pour valeur sélectionne la deuxième ligne (femme)
 * clog( oSexeEnum.val )					// affiche 'f'
 * clog( oSexeEnum.val(0) )					// affiche 'f'
 * clog( oSexeEnum.val() )					// affiche 'f'
 * clog( oSexeEnum.val(1) )					// erreur... !
 * clog( oSexeEnum.val('display') )			// erreur... !
 * clog( oSexeEnum.val('description') )		// erreur... !




 * oSexeEnumDef = [
 *		{ base: 'h',		display: 'homme',		description: 'un monsieur' },
 *		{ base: 'f',		display: 'femme',		description: 'une madame' }
 *  ]

 */
class EnumDef {
	static oConst = {
		sHtmlBefore: 	`<select`,
		sHtmlAfter: 	`>`
	}
	static aPossibleValues = [];	// toutes les valeurs possibles en col[0] + leurs propriétés (pour les objets Enum liés à l'id fourni)
	//defaultValue = undefined;		// valeur par défaut (pour chaque objet Enum associé par l'id)
	static aDefaultValue = [];
	/**
	 * @param {*} id Id unique de l'EnumDef (lévera une erreur si construction future d'un objet avec ce même id)
	 * @param { [{..}..] | [[..]..] | String.. } aPossibleValues Liste des valeurs que pourront posséder les objets Enum liés à cet id
	 * @param {*} defaultValue Valeur par défaut des objets Enum créés qui (liés par l'id à cet objet EnumDef)
	 */
	constructor(id, aPossibleValues = null, defaultValue = undefined) {
		if (arguments.length === 1)  {						// Appel depuis un objet Enum
			//if (!EnumDef.isIdAvailable(id))  { return; }	// avec id EnumDef déjà instancié
			let bEnumCanInterractWithme = (!EnumDef.isIdAvailable(id));
			if (!bEnumCanInterractWithme) {
				this.id = null;
				return;
			}
			this.id = id;
		} else {
			EnumDef.isIdOk(id, true);
			this.createObj(id, aPossibleValues, defaultValue);
			//this.ShowHelpInConsole();
		}
	}
	createObj(id, aPossibleValues, defaultValue) {
		this.id = id;
		this.setPossibleValues(aPossibleValues);
		this.defaultValue = defaultValue;
		if (!(defaultValue == undefined || defaultValue === ''))  {
			this.isValueOk(defaultValue);
		}
	}
	static showHelpInConsole() {
		console.log('EnumDef : utilisation : ', {
			'val': `Lire/définir une valeur pour l'objet énum : objEnumDef.val`,
			'val()': `Lire/définir une valeur pour l'objet énum : objEnumDef.val en précisant entre parenthès le numéro de colonne concernée.`
		});
	}
	static isIdAvailable(id) {
		return (EnumDef.aPossibleValues[id] == undefined);
	}
	static isIdOk(id, bErrorIfNotOk = false) {
		if (typeof(id) !== 'string')  {
			if (bErrorIfNotOk)  { EnumDef.displayError('id', 'id_bad_type', typeof(id)); }
			return false;
		}
		if (!EnumDef.isIdAvailable(id))  {
			if (bErrorIfNotOk)  { EnumDef.displayError('id', 'id_not_available', id); sResult = false; }
			return false;
		}
		return true;
	}

	static displayError(errorElement, errorDetail) {
		let sMessage = errorDetail + '\n', errorCategory = Error;
		if (errorElement === 'id') {
			if (errorDetail === 'id_bad type') {
				sMessage += `L'id' donné à l'objet EnumDef doit être de type string (ici de type ${arguments[2]}).`;
			} else if (errorDetail === 'id_not_available') {
				sMessage += `L'id '${arguments[2]}' donné à l'objet EnumDef est déjà utilisé.`;
			}
		} else if (errorElement === 'config') {
			if (errorDetail === 'config_bad_type') {
				errorCategory = TypeError;
				sMessage += `La config des valeurs possibles doit être de type Array ou Object (${arguments[2]} fourni).`;
			} else if (errorDetail === 'no_elements') {
				sMessage += `La config des valeurs possibles de l'EnumDef ne contient aucun élément.`;
			}
			sMessage += `Celle-ci doit être un objet énumérable
				ayant pour clés les valeurs stockées et pour valeurs ce qui sera affiché pour l'utilisateur.`;
		} else if (errorElement === 'value') {
			if (errorDetail === 'value_bad_type') {
				errorCategory = TypeError;
				sMessage += `La valeur d'un élément (1ère colonne de l'objet élément des valeurs possibles) de EnumDef doit être du type String | Integer (${arguments[2]} fourni).`;
			} else if (errorDetail === 'value_too_big') {
				sMessage += `La valeur ne peut être renvoyée car l'index de la propriété ciblée (${arguments[2]} fourni) est supérieur ou égal au nombre de propriétés de l'élément de l'Enum.`;
			} else if (errorDetail === 'value_not_found') {
				sMessage += `La valeur "${arguments[2]}" transmise ne fait pas partie des valeurs possibles pour cet EnumDef.`;
			} else if (errorDetail === 'value_bad_target_format_array') {
				sMessage += `La cible doit ici être un numéro de colonne entre 0 et ${arguments[2]}`;
			} else if (errorDetail === 'value_bad_target_incorrect_number') {
				sMessage += `Le numéro de cible doit être un entier compris entre 0 et le nombre de propriétés de l'élément sélectionné dans l'Enum;`;
			}
		} else {
			sMessage += 'Erreur inconnue.';
		}
		EnumDef.showHelpInConsole();
		throw new errorCategory(sMessage);
	}
	isValueOk(val, bErrorIfNotOk = false, vReturn = 'boolean') {
		let elem;
		let sValType = typeof(val);
		if (!(sValType === 'string' || (sValType === 'number' && Number.isInteger(val)))) {
			if (bErrorIfNotOk)  { EnumDef.displayError('value', 'value_bad_type', sValType); }
			return false;
		}
		// renvoie true si la valeur se touve parmi les valeurs possibles en première colonne
		for (let i = 0; i < EnumDef.aPossibleValues[this.id].length; i++) {
			elem = EnumDef.aPossibleValues[this.id][i];
			if (typeOf(elem) === 'object') {
				if (Object.values(elem)[0] === val)  { return (vReturn === 'boolean') ? true : elem; };
			} else if (typeOf(elem) === 'array') {
				if (elem[0] === val)  { return (vReturn === 'boolean') ? true : elem; }
			}
		}
		if (bErrorIfNotOk)  { EnumDef.displayError('value', 'value_not_found', val); }
		return false;
	}
	/**
	 * Renvoie toutes les données (les différentes valeurs possibles)
	 */
	getData(element = '') {
		if (element === '') {
			return EnumDef.aPossibleValues[this.id];
		} else {
			return this.isValueOk(element, true, 'element');
		}
	}
	isConfigOk(aPossibleValues) {
		let configType;
		if (Array.isArray(aPossibleValues))  { configType = 'array' }
		configType = typeof(aPossibleValues);
		switch (configType) {
			case 'array':
				if (!aPossibleValues.length)  { EnumDef.displayError('config', 'no_elements'); }
				break;
			case 'object':
				if (!Object.keys(aPossibleValues).length)  { EnumDef.displayError('config', 'no_elements'); }
				break;
			default:
				EnumDef.displayError('config', 'config_bad_type', configType);
		}
		this.setPrivateProp('typeOfPossibleValuesContainer', configType);
		return true;
	}

	setPossibleValues(aPossibleValues) {
		let sConfigType = typeOf(aPossibleValues);
		let sElementType;
		let aGoodPossibleValues = [];
		if (sConfigType !== 'array')  { EnumDef.displayError('config', 'config_bad_type', sConfigType); }
		aPossibleValues.forEach(function(elem) {
			sElementType = typeof(elem);
			if (sElementType === 'string') {
				aGoodPossibleValues.push( elem );
			}
			if (typeof(elem) === 'object') {		// 'typeof' = including array
				aGoodPossibleValues.push( elem );
			}
		})
		EnumDef.aPossibleValues[this.id] = aGoodPossibleValues;
	}
	ShowHelpInConsole = function() {
		console.log('objet', this);
		console.log('EnumDef', EnumDef);
	}
	set defaultValue(val) {
		if (!this.isValueOk(val))  { EnumDef.displayError('value', 'value_not_found'); }
		EnumDef.aDefaultValue[this.id] = val;
	}
	get defaultValue() {
		return EnumDef.aDefaultValue[this.id];
	}
	/**
	 * Return an element property
	 * @param {*} value Value of the targeted element
	 * @param {Integer|String} targetProp Index OR name of the targeted property
	 */
	getPropFromValue(value, targetProp = 1) {
		let oThis = this;
		let element;
		let sTargetType = typeof(targetProp);
		let sResult, sElementType, iLength;
		if (sTargetType === 'number' && (!Number.isInteger(targetProp) || targetProp < 0))  { EnumDef.displayError('value', 'value_bad_target_incorrect_number', targetProp); }
		this.isValueOk(value);
		if (EnumDef.aPossibleValues[oThis.id] == undefined || !EnumDef.aPossibleValues[this.id].length)  { EnumDef.displayError('config', 'no_elements'); }
		for (let i = 0; i < EnumDef.aPossibleValues[oThis.id].length; i++) {
			element = EnumDef.aPossibleValues[oThis.id][i];
			sElementType = typeOf(element);
			if (sElementType === 'array') {
				iLength = element.length;
				if (sTargetType !== 'number' || targetProp >= iLength)  { EnumDef.displayError('value', 'value_bad_target_format_array', iLength - 1); }
				if (element[0] === value)  {
					// targeted element in array
					sResult = element[targetProp]; return sResult;
				}
			} else if (sElementType === 'object') {
				if (typeof(targetProp) === 'number') {
					if (targetProp >= Object.keys(element).length)   { EnumDef.displayError('value', 'value_too_big', targetProp); }
					targetProp = Object.keys(element)[targetProp];
					sTargetType = typeof(targetProp);
				}
				if (!element.hasOwnProperty(targetProp))    { EnumDef.displayError('value', 'value_not_found', value); }
				if (sTargetType !== 'string' && sTargetType !== 'number')  { EnumDef.displayError('value', 'value_bad_target_format_object', targetProp); }
				// targeted element in object
				if (element[Object.keys(element)[0]] === value)  { sResult = element[targetProp]; return sResult; }
			}
		}
		return sResult;
	}
}
class Enum extends EnumDef{
	sValue;
	constructor(id, value = undefined) {
		super(id);
		if (super.isValueOk(value, false)) {
			this.sValue = value;
			this.id = id;
		} else {
			this.sValue =  super.defaultValue;
		}
	}
	getPropFromValue(value, targetProp = 1) {
		return super.getPropFromValue(value, targetProp);
	}
	get value() {
		return this.sValue;
	}
	getProp(targetProp = 1) {
		let val = this.value;
		return this.getPropFromValue(val, targetProp);
	}
	html(sLabel = '', disabled = false) {
		let oThis = this;
		let val = this.value;
		let oData = super.getData('');
		let sDataType = typeOf(oData);
		let sHtml = ``;
		let sSelected = ``;
		let sDisabled = ``;
		let sElemVal;
		let sElementType;
		//clog('data', oData)

		if (typeof(sLabel) === 'string' && sLabel !== '') {
			sHtml += `<label for="${this.id}">${sLabel}</label>\n`;
		}
		sHtml += `<select id="${this.id}" name="${this.id}">\n`;

		if (sDataType !== 'array')  { EnumDef.displayError('config', 'config_bad_type', sDataType); }
		oData.forEach(function(elem) {
			sElemVal = Object.values(elem)[0];
			sElementType = typeOf(elem);
			sSelected = (sElemVal === val) ? ` selected` : ``;
			sDisabled = (disabled) ? ` disabled` : ``;
			sHtml += `\t<option value="${Object.values(elem)[0]}"${sSelected}${sDisabled}>${Object.values(elem)[1]}</option>\n`;
		})
		sHtml += `</select>\n`;
		return sHtml;
		//clog('aaa', super.)
		//return this.getPropFromValue(val, targetProp);

		// <label for="title">Title</label>
		// <select id="title" name="title">
		// 	<option value="" selected>Please choose</option>

		// 	<option value="Mr">Mr</option>
		// 	<option value="Miss">Miss</option>
		// 	<option value="Mrs">Mrs</option>
		// 	<option value="Ms">Ms</option>

		// 	<option value="Dr">Dr</option>
		// 	<option value="Other">Other</option>
		// </select>


	}
}


