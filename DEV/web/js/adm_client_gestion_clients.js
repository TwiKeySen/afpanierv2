/*function toggle(id) {
    $(id).toggle(1000);
 
 }
 function create_formulaires() {
    var sResultat= "";
    var i;
    var iNumber= document.getElementById("iNumber").value;
    for (i=1; i<=iNumber; i++) {
        sResultat+= "<p>" + i + "&nbsp;: &nbsp; <input type=\"text\" name=\"note_" + i + "\" id=\"note_" + i + "\" value=\"" + Math.floor(Math.random()*20+1)  + "\"></p>";
    }
    document.getElementById("formulaire").innerHTML= sResultat;
 }*/
 
 var aOfClient= [];
             aOfClient[0]= [];
             aOfClient[0]["code_beneficiaire"]= "147";
             aOfClient[0]["civilite_form"]= "Feminin";
             aOfClient[0]["nom_prenom"]= "Dudu Lilou";
             aOfClient[0]["adresse"]= "123 rue du Bonheur";
             aOfClient[0]["code_postal_ville"]= "34000";
             aOfClient[0]["tel"]= "0600000002";
             aOfClient[0]["mail"]= "d_lilou@free.fr";
             aOfClient[0]["section"]= "Stagiaire";
                 
             function constructTable()	{
                 var i;
 
                 var sHTML= "";
                 sHTML+= "<thead>";
                 sHTML+= "<tr>";
                 sHTML+= "<td>Code bénéficiaire</td>";
                 sHTML+= "<td>Nom/prénom</td>";
                 sHTML+= "<td>Email</td>";
                 sHTML+= "<td>Téléphone</td>";
                 sHTML+= "<td>Editer</td>";
                 sHTML+= "<td>Supprimer</td>";
                 sHTML+= "</tr>";
                 sHTML+= "</thead>";
                 sHTML+= "<tbody>";
 
                 for (i=0; i<aOfClient.length; i++)	{
                     sHTML+= "<tr>";
                     sHTML+= "<td>" + aOfClient[i]["code_beneficiaire"] + "</td>";
                     sHTML+= "<td>" + aOfClient[i]["nom_prenom"] + "</td>";
                     sHTML+= "<td>" + aOfClient[i]["mail"] + "</td>";
                     sHTML+= "<td>" + aOfClient[i]["tel"] + "</td>";
                     sHTML+= "<td><button id='Editer"+i+"' onClick=\"editClient(" + i + ")\">Editer</button></td>";
					 sHTML+= "<td><button id='Supprimer"+i+"' onClick=\"supClient(" + i + ")\">Supprimer</button></td>";
                     sHTML+= "</tr>";
                 }

                 sHTML += "</tbody>";
                 $('#table_client').html(sHTML);
             }

             function rebuildDatable()	{
				$('#table_client').html("");
				table.clear(); 
				table.destroy(); 
				constructTable();
				table = $('#table_client').DataTable(configuration);
            }
            
            function resetFormulaire() {
                $('#code_beneficiaire').val("");
                $('#civilite_form').val("");
				$('#nom_prenom').val("");
				$('#adresse').val("");
				$('#code_postal_ville').val("");
				$('#telephone').val(null);
				$('#mail').val("");
				$('#section').val("");
			}

             function ajoutClient()	{
                 //function btn_ajouter()
                var iLongueur= aOfClient.length;
                aOfClient[iLongueur]= [];
                aOfClient[iLongueur]["code_beneficiaire"]= $('#code_beneficiaire').val();
                aOfClient[iLongueur]["civilite_form"]= $('#civilite_form').val();
                aOfClient[iLongueur]["nom_prenom"]= $('#nom_prenom').val();
                aOfClient[iLongueur]["adresse"]= $('#adresse').val();
                aOfClient[iLongueur]["code_postal_ville"]= $('#code_postal').val();
                aOfClient[iLongueur]["tel"]= $('#telephone').val();
                aOfClient[iLongueur]["mail"]= $('#mail').val();
                aOfClient[iLongueur]["section"]= $('#section').val();
                constructTable();
                //appel ici a la fonction mystère de reconstruire le datatable
                // alors qu'il a déjà été appelé
                resetFormulaire ();
            } 

            function majClient() {
                //function btn_modifier ()
                    aOfClient[iIndiceEditionEncours]["code_beneficiaire"] = $('#code_beneficiaire').val();
                    aOfClient[iIndiceEditionEncours]["civilite_form"] = $('#civilite_form').val();
                    aOfClient[iIndiceEditionEncours]["nom_prenom"] = $('#nom_prenom').val();
                    aOfClient[iIndiceEditionEncours]["adresse"] = $('#adresse').val();
                    aOfClient[iIndiceEditionEncours]["code_postal_ville"] = $('#code_postal_ville').val();
                    aOfClient[iIndiceEditionEncours]["tel"] = $('#telephone').val();
                    aOfClient[iIndiceEditionEncours]["mail"] = $('#mail').val();
                    aOfClient[iIndiceEditionEncours]["section"] = $('#section').val();
                    rebuildDatable();
                    resetFormulaire();
                    cacherBouton('#btn_modifier');
                    cacherBouton('#btn_ajouter');
                }

            //function supClient(iIndiceEdit) {
                //aOfClient.splice(iIndiceEdit, 1);
                //construct();
            //}           
			var iIndiceEditionEncours;
			function editClient(iIndiceEdit)	{
				//alert("iIndiceEdit = " + iIndiceEdit);
				iIndiceEditionEncours= iIndiceEdit;
				$('#code_beneficiaire').val(aOfClient[iIndiceEdit]["code_beneficiaire"]);
				$('#civilite_form').val(aOfClient[iIndiceEdit]["civilite_form"]);
				$('#nom_prenom').val(aOfClient[iIndiceEdit]["nom_prenom"]);
				$('#adresse').val(aOfClient[iIndiceEdit]["adresse"]);
				$('#code_postal_ville').val(aOfClient[iIndiceEdit]["code_postal_ville"]);
				$('#tel').val(aOfClient[iIndiceEdit]["tel"]);
				$('#mail').val(aOfClient[iIndiceEdit]["mail"]);
				$('#section').val(aOfClient[iIndiceEdit]["section"]);
			}

            var iIndiceSupEncours;

			function supClient(iIndiceSup)	{
				iIndiceSupEnCours= iIndiceSup;
				aOfClient.splice(iIndiceSup,1);
				rebuildDatable();
				resetFormulaire();
            }
            
            /*function cacherBouton(the_button) {
                let divHTML = document.getElementById(the_button);
                    let value = divHTML.style.display;
                    console.log('LOG => ', value)
            
                    if (value === 'none') {
                        divHTML.style.display = 'flex';
                    } else {
                        divHTML.style.display = 'none';
                    }
                }*/
                          // CONFIGURATION DATATABLE
                         const configuration = {
                             "stateSave": false,
                             "order": [[2, "asc"]],
                             "pagingType": "simple_numbers",
                             "searching": true,
                             "lengthMenu": [[10, 25, 50, 100, -1], ["Dix", "Vingt cinq", "Cinquante", "Cent", "Tous"]], 
                             "language": {
                                 "info": "Client _START_ à _END_ sur _TOTAL_ sélectionnés",
                                 "emptyTable": "Aucun client",
                                 "lengthMenu": "_MENU_ Clients par page",
                                 "search": "Rechercher : ",
                                 "zeroRecords": "Aucun résultat de recherche",
                                 "paginate": {
                                     "previous": "Précédent",
                                     "next": "Suivant"
                                 },
                                 "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                                 "sInfoEmpty":      "Utilisateurs 0 à 0 sur 0 sélectionné",
                             },
                             "columns": [
                                 {
                                     "orderable": true,
                                     "autowidth": true
                                 },
                                 {
                                     "orderable": true,
                                     "autowidth": true
                                 },
                                 {
                                     "orderable": true,
                                     "autowidth": true
                                 },
                                 {
                                     "orderable": true,
                                     "autowidth": true
                                 },
                                 {
                                     "orderable": false,
                                     "autowidth": true
                                 },
                                 {
                                     "orderable": false,
                                     "autowidth": true
                                 }
                             ],
                             'retrieve': true
                         };
             var table; // cette fonction permet de mettre en application le tp datatable une fois que tout  été effectué
             $(document).ready(function() {
                 constructTable();
                 // INIT DATATABLE (permet d'avoir le look complet du tableau datatable)
                 table = $('#table_client').DataTable(configuration);
             });

             //function construct() {
                //tables.destroy();
                //constructTable();
                //tables = $("#table_client").DataTable(configuration);
            //}
 

        
   
 

 




     