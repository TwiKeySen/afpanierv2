




//alert(Input.doesTypeExist('text'))

// à voir : oConfigTable.oDataTable.select("Nom = NOM")
// 	dt.iIndiceLigneCliquée	= $(this).closest('tr').get(0)._DT_RowIndex; // index de ligne cliquée

var dt = {
	iIndiceLigneEnEdition	: null,
	iIndiceLigneCliquée		: null,
	configuration : {
		//'bRetrieve' : true,
		"stateSave": false,
		"order": [[0, "asc"]],
		"pagingType": "simple_numbers",
		"searching": true,
		// keys: {
		// 	className: 'highlight'
		// },
		"lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Tous"]],
		"language": {
			"info": "Promotions _START_ à _END_ sur _TOTAL_ sélectionnées",
			"emptyTable": "Aucune promotion",
			"lengthMenu": "_MENU_ Promotions par page",
			"search": "Rechercher : ",
			"zeroRecords": "Aucun résultat de recherche",
			"paginate": {
				"previous": "Précédent",
				"next": "Suivant"
			},
			"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
			"sInfoEmpty":      "Promotions 0 à 0 sur 0 sélectionnée",
			'rowId': function(a) {   // DT_RowId
				return 'id_' + a.nom;
			}
		},
		"columns": [],	// array rempli par ajouterConfigColonnesDataTable()
		//"columnDefs" : [{"targets": [2], "searchable": "select"}, {"targets": [3, 4, 5, 6], "searchable": false}], // dbg
		//"pageLength": 15, // nb de lignes par page
		'retrieve': true,
		// "createdRow": function (row, data, index) {
		// 	$.each($('td', row), function (colIndex) {
		// 		console.log('createdRow', colIndex, index, data, row)
		// 		row.id = data.id;
		// 		//$(this).parent().attr('data-initial', data);
		// 	}
		// )}
	}
}


// CONFIG GENERALE DATATABLE liée à une seule table
// idColonne : index de la colonne affichée	 OU  id de la colonne  OU  'auto_increment'
var oConfig = {};
oConfig.sId =					'table_promos';
oConfig.idColonne =				'nom',				// quelle colonne constitue la clé primaire de chaque ligne ?
	oConfig.oConfigDataTable =		dt,					// config du datatable
	oConfig.oDataTable = 			{},					// dataTable (setté au chargement de la page)
	oConfig.oTable = 				{},					// table html (setté au chargement de la page)
	oConfig.aColonnes =				[],					// config des différentes colonnes (visibles ou non) (setté après)
	oConfig.aLignes =				[],					// config des différentes lignes (visibles ou non) : l'ensemble des données (setté après)
	oConfig.oTemplatesColonnes =	{},					// config des types de données prédéfinis dont on se sert pour configurer plus rapidement une nouvelle colonne
	oConfig.col =					[],					// colonnes dans l'ordre d'affichage (setté après)
	oConfig.fnCol =					{},					// fonctions concernant les colonnes
	oConfig.fnLig =					{},
	oConfig.const =					{},
	oConfig.const.sIdConteneurFormulaires = `form_promos`;
oConfig.var =					{}
oConfig.enumConfig =			{}
oConfig.enumDef =				{}
oConfig.var.sIdEtition = 		null;
oConfig.var.bNouvellePromo = 	false;
oConfig.var.bModifie = 			false;
oConfig.var.iNbLignesInserees = 0;
oConfig.enumConfig.type_promo =	[
	{ value: 'euro', 		display:  '€' },
	{ value: 'pourcent', 	display:  '%' },
	{ value: 'aliment', 	display:  'Aliment' },
]

oConfig.enumDef.type_promo = new EnumDef('type_promo', oConfig.enumConfig.type_promo, 'euro');


var table, sIdPromo;
var aIndexColonnesSelonPositionDansTableau = [];
var sAffichageSiIndéfini = '';
var aPosition_dans_tableauColonnes = [];
var oInfosCell;	// infos sur la cellule en cours d'édition

var re = [];
re.sDélimiteurs = `#{(.*?)}`;
re.sAccolades = `(?:\\s*)({.*})?(?:\\s*)(?:$|;)`;
re.sEntier = `\\b(?:int(?:eger)?|ent(?:ier)?)\\b`;
re.sArrondi = `\\b(?:arr(ondi[etr]?)?|(ar)?round)\\b`;


$(document).ready(function() {									// quand page chargée
	let oConsole = {
		'window': 	window,
		'document': document,
		'Object': 	Object,
		'formatages possibles......': f
	}
	clog(`explorons tout ça....`, oConsole);
	insérerDonnéesJSDansArray(oConfig);
	initConfig(oConfig);
	construireTable(oConfig);
	genererDataSet(oConfig);
	insérerDataSetDansTable(oConfig);
	activerBoutonAjouterPromo();
	oConfig.oDataTable.filtersOn();

	$('#table_promos tbody tr td .btn.btn-éditer').on('click', function() {
		let iIndexLigne = $(this).closest('tr').index();
		let sIdLigne = $(this).closest('tr').attr('id');
		//éditerLigne(iIndexLigne, 0, 'table_promos');
		editerPromo(sIdLigne);
	})

	$('#table_promos tbody tr td .btn.btn-supprimer').on('click', function() {
		let iIndexLigne = $(this).closest('tr').index();
		supprimerLigne(iIndexLigne, 'table_promos');
	})

	$('.btn.btn-ajouter_promo').on('click', function() {
		ajouterLigne();
	})

	//afficherFormulaires();
	oConfig.oTable.on('dblclick', 'tbody tr td', function () {
		//alert(   oConfigTable.oDataTable.row( $(this) ).id   )
		//clog (  oConfigTable.oDataTable.row( $(this) )  )
		//return;
		if ($(this).hasClass('edition'))  { return; }
		iIdPromo			= $(this).parent().attr('id'); // parseInt(oConfigTable.oDataTable.cell(this).index().row)
		bEstCelluleBouton	= $(this).hasClass('btn_cellule');
		iLigne				= $(this).parent().index(); // id_promo : oConfigTable.oDataTable.cell(this).index().row;
		iColonne 			= $(this).index(); // oConfigTable.oDataTable.cell(this).index().column;
		//iObjColonne			= IndexObjDepuisIndexColonneAffichee(iColonne);
		//sIdChamp			= $(this).parent('table').children(`thead:first-child th:nth-child(${iColonne})`).attr('id');
		//clog(iLigne + ', ' + iColonne, iObjColonne)
		//sIdChamp			= $(this).parent('table').children(`thead:first-child th:nth-child(${iColonne})`).attr('id');
		sIdChamp 			= oConfig.col[iColonne].id;
		sTexteAffiche		= $(this).text();
		//sTexteStocke		= oConfig.aLignes[iIdPromo][sIdChamp];
		sTypeChamp			= oConfig.col[iColonne].type;
		//éditerCellule(iLigne, iColonne);
		oConfig.oCell = obtInfosCelluleDepuisIndex(iLigne, iColonne, 'table_promos');
		éditerLigne(iLigne, iColonne);
		//console.log(oConfig.oDataTable.cells( iIdPromo, '' ).render( 'display' ))	//info dblck affichée
		//console.log(oConfig.oDataTable.cells( '.édit_cell' ).data()[0])				//texte de la cellule sélectionnée (qui possède la classe 'édit_cell')
		//console.log(oConfig.oDataTable.cells( iIdPromo, '' ).render( 'sort' ))		//cellule dblck si triée
		//oConfig.oDataTable.cell( this ).invalidate().draw(); // met à jour la donnée de la cellule dans le datatable
	});
});

function initConfig(oConfigTable = oConfig) {
	clog('oConfigTable', oConfigTable);
	ajouterConfigColonnesDataTable(oConfigTable);
}
function genererDataSet(oConfigTable = oConfig) {
	// génère les jeux de données et les place dans col
	//aDataSet = oConfigTable.fnCol.obtDataSetArray(oConfig);			// désactivé
	//oConfigTable.aColonnes.oConfigTable.fnCol.obtDataSetArrayObjets(oConfig);
	oConfigTable.col.insérerJeuDeDonneesDansDataTable( oConfig.aLignes.obtJeuDeDonnees() )
}
function insérerDataSetDansTable(oConfigTable = oConfig) {
	// let oDataSet =
	// {
	// 	est_actif: false,
	// 	age: 100,
	// 	date_debut: "2019-04-20",
	// 	editer_supprimer: 'iii',//"<div>   <span class=\"btn btn-éditer fas fa-pencil-alt\"></span>   <span class='btn btn-supprimer fas fa-trash-alt'></span>   </div>",
	// 	email: "toto@afpa.fr",
	// 	nom: "bbbbbbbbbb",
	// 	prenom: "Andouille",
	// 	sexe: "H",
	// }
	//let aDataSet = ['val1', 'val2', 'val3', 'val4', 'val5', 'val6', 'val7', 'val8' ];
	let aDataSet1 = ['val1', 'val2', 'val3', 'val4', 'val5', 'val6', 'val7', 'val8' ];
	let aDataSet2 = ['toto.1', 'toto2', 'toto3', 'toto4', 'toto5', 'toto6', 'toto7', 'toto8' ];
	let aDataSetM = [aDataSet1, aDataSet2];
	//oConfigTable.oDataTable.row.add(oDataSet).draw();
	insererDonneesDansTable(oDataSet);
	//let oDataSetNew = dataSetArrayVersDataSetObj(aDataSet);
	//clog('........aDataSet',aDataSet)
	//insererDonneesDansTable(oDataSet)
	//insererDonneesDansTable(aDataSetM);
	function dataSetArrayVersDataSetObj(aDataSet) {
		let aResult = [];
		if (aDataSet.containsOnlyType('array')) {
			// aDataSet contient plusieurs lignes de données --> récursivité sur chaque ligne
			// on ajoute à aResult le nouvel objet retourné par l'appel récursif
			aDataSet.forEach(function(aLigneDeDonnee) {
				aResult.push(dataSetArrayVersDataSetObj(aLigneDeDonnee));
			})
		} else {
			// aDataSet contient une seule ligne de données --> on parcourt chaque valeur de donnée
			// et on l'ajoute à l'objet créé
			let oLigneDeDonnee = {};
			//let iIndexColo
			//let sIdColonne = ;
			aDataSet.forEach(function(aValeurDonnee) {

				//oLigneDeDonnee.
			})

		}
		//oConfig.col.
		//aDataSet.every()
	}
}
function activerBoutonAjouterPromo() {
	$('#form_btn_ajouter').on('click', function() {
		creerPromo();
	})
}
function creerPromo(oConfigTable = oConfig) {
	annulerEdition();
	oConfigTable.var.bNouvellePromo = true;
	editerPromo(null);
}
function annulerEdition(oConfigTable = oConfig) {
	oConfigTable.var.sIdEdition = null;
	$(`tr`).children('td').removeClass('edition');
	$('#' + oConfigTable.const.sIdConteneurFormulaires).addClass(`d-none`);
}
function supprimerLignesDuDataTable(oConfigTable = oConfig) {
	//let iNbLignes = oConfigTable.oTable.DataTable().rows().count();
	oConfigTable.oTable.DataTable().rows().remove().draw();
}
/**
 * Récupère les données saisies par l'utilisateur et les envoie vers l'array
 * @param {*} oConfigTable
 */
function enregistrerDonneesFormulaires(oConfigTable = oConfig) {
	let aIdFormulaires = [];
	let oJeuDeDonnees = {};
	let oFormulaire;
	let sIdPromo = oConfigTable.var.sIdEdition;
	aIdFormulaires = oConfigTable.col.getPropFromAllObjectsInArrayWhere('id', 'type', '!==', 'bouton'); // récupère les id des colonnes visibles qui ne sont pas de type 'bouton'
	aIdFormulaires.forEach(function(sNomProp){
		oFormulaire = $('#_' + sNomProp).children().not('label');
		oJeuDeDonnees[sNomProp] = getDomElemtValue(oFormulaire);
	})
	// todo... 	contrôle de saisie
	Object.assign(oConfigTable.aLignes[+sIdPromo], oJeuDeDonnees);
	// todo... 	afficher enregistrement réussi
	annulerEdition();
	supprimerLignesDuDataTable();
	genererDataSet();
}



function obtIndexColDataTable (sTypeDonnée = null, iIndex = null) {
	if (iIndex !== null) {
		return aIndexColonnesSelonPositionDansTableau[iIndex];
	}
}


function obtenirIndexDansTableauSiCorrespondance(aTableau, sParamètre, sValeur) {
	var bTrouvé = false;
	var iIndexTrouvé;
	aTableau.forEach(function(colonne, index, tableau) {
		if (!bTrouvé) {
			if (aTableau[index][sParamètre] == sValeur) {
				bTrouvé = true;
				iIndexTrouvé = index;
			}
		}
	})
	return iIndexTrouvé;
}


oConfig.oTemplatesColonnes.formaterTexte = function(sTexte, sFormatVoulu, sIdColonne = 'email.....todo') {
	let sFormatReconnu, sTexteFormaté;
	if (sFormatVoulu.toLowerCase() === 'camelCase'.toLowerCase()) {
		sFormatReconnu = 'camelCase';										// camelCase
		sTexteFormaté = sTexte.toCamelCase();
	} else if (sFormatVoulu.toLowerCase() === 'PascalCase'.toLowerCase()) {
		sFormatReconnu = 'PascalCase';										// PascalCase
		sTexteFormaté = sTexte.toPascalCase();
	} else if (sFormatVoulu.toLowerCase() === 'min'.toLowerCase()) {
		sFormatReconnu = 'minuscules';										// minuscules
		sTexteFormaté = sTexte.toLowerCase();
	} else if (sFormatVoulu.toLowerCase() === 'MAJ'.toLowerCase()) {
		sFormatReconnu = 'MAJUSCULES';										// MAJUSCULES
		sTexteFormaté = sTexte.toUpperCase();
	} else if (sFormatVoulu.toLowerCase() === 'PremièreLettreMAJ'.toLowerCase()) {
		sFormatReconnu = 'PremièreLettreMAJ';								// PremièreLettreMAJ
		sTexteFormaté = sTexte.capitalize();
	} else if (sFormatVoulu.toLowerCase() === 'email'.toLowerCase()) {
		sFormatReconnu = 'email';											// email
		sTexteFormaté = sTexte.toLowerCase();
	}
	return sTexteFormaté;
}
// j 		Jour du mois en chiffres; sans le zéro si inférieur à 10.
// jj 		Jour du mois en chiffres; avec le zéro si inférieur à 10.
// jjj 		Jour de la semaine sous la forme de son nom abbrégé.
// jjjj 	Jour de la semaine représenté par son nom complet.
// m 		Mois en chiffres; sans le zéro si inférieur à 10.
// mm 		Mois en chiffres; avec le zéro si inférieur à 10.
// mmm 		Mois sous la forme de son nom abbrégé.
// mmmm 	Mois représenté par son nom complet.
// aa 		Année sous la forme des 2 derniers chiffres; avec le zéro si inférieur à 10.
// aaaa 	Année représentée par 4 chiffres.
// exemple :
// 'AAAA-MM-JJ' (c'est sous ce format-ci qu'une DATE est stockée dans MySQL)
// 'AAMMJJ'	'AAAA/MM/JJ'	'AA+MM+JJ'	'AAAA%MM%JJ'	AAAAMMJJ (nombre)	AAMMJJ (nombre)


/**
 * Renvoie le type de donnée s'il existe
 * @param {String} sId Id du type de donnée concerné (ex: 'prenom')
 * @return {ref:oConfig.oTemplatesColonnes[sId]} Référence vers l'objet contenant les propriétés du type de donnnée
 */
function typeDeDonnée(sId = 'prenom', oConfigTable = oConfig) {
	let type = oConfigTable.oTemplatesColonnes[sId];
	if (type !== undefined) {
		return type;
	} else {
		throw new Error(`le type de données "${sId}" n'existe pas dans l'array "oConfigTable.oTemplatesColonnes"`);
	}
}

/**
 * Renvoie une propriété du type de donnée si elle existe
 * @param {String} sId Id du type de donnée (ex: 'prenom')
 * @param {String} sNomProp Nom de la propriété voulue (ex: 'libellé')
 * @param {?String} valueIfUndefined Valeur qui sera renvoyée si la propriété demandée n'existe pas ('' par défaut)
 * @param {?String} bErrorIfPropUndefined Si true, une erreur sera déclenchée si si la propriété demandée n'existe pas (false par défaut)
 * @return {*} Valeur de la propriété
 */
function propTypeDeDonnée(sId = 'prenom', sNomProp = 'libellé', valueIfUndefined = '', bErrorIfPropUndefined = false) {
	let type = typeDeDonnée(sId);
	let prop = type[sNomProp];
	if (prop === undefined) {
		if (bErrorIfPropUndefined)  {
			throw new Error(`la propriété "${sNomProp}" du type de données "${sId}" n'existe pas dans l'array "oConfigTable.oTemplatesColonnes"`);
		}
		return valueIfUndefined;
	}
	return prop;
}

/**
 * Génère une erreur si la config fournie n'est pas de type 'object'
 * @param {Object} config Config qui sera transmise à une fonction obtCodeHtml*()
 */
function vérifTypeConfig(config) {
	if (typeof(config) !== 'object')  { throw new Error(`config doit être un objet contenant les paramètres de l'objet à créer`); }
}


/**
 * Crée la configuration de l'élément input[type='text'] à générer
 * cette config sera transmise à obtCodeHtmlInputText()
 * @param {String} sId Id du type de donnée concerné (ex: 'prenom')
 * @return {Object} Objet contenant les différents attributs de l'input à créer ('placeholder', 'maxlength'...)
 */
function obtConfigInputText(sId = 'prenom') {
	let config = {
		class: propTypeDeDonnée(sId, 'class'),
		maxlength: propTypeDeDonnée(sId, 'taille_max'),
		placeholder: propTypeDeDonnée(sId, 'placeholder'),
		pattern: propTypeDeDonnée(sId, 'pattern')
	}
	return config;
}
function obtCodeHtmlInputText(config, sValeur = '') {
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	sDébutHtml 		= `<input type='text'`;
	sMilieuHtml		= '';
	sFinHtml 		= `>`;
	vérifTypeConfig(config);
	// si valeur non passée en config, on applique une valeur par défaut
	if (config.maxlength === '')  	{ config.maxlength 		= 254 }
	Object._deletePropIf(config, '', '===', '');
	// génère le code à partir de la config
	Object.keys(config).forEach(function(nomProp) {
		sMilieuHtml += ` ${nomProp}="${config[nomProp]}"`;
	})
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + ` value="${sValeur}"` + sFinHtml;
	return sCodeHtmlInput;
}


/**
 * Crée la configuration de l'élément input[type='email'] à générer
 * cette config sera transmise à obtCodeHtmlInputEmail()
 * @param {?String} sId Id du type de donnée concerné (ici: 'email')
 * @return {Object} Objet contenant les différents attributs de l'input à créer ('placeholder', 'maxlength'...)
 */
function obtConfigInputEmail(sId = 'email') {
	let config = {
		class: propTypeDeDonnée(sId, 'class'),
		maxlength: propTypeDeDonnée(sId, 'taille_max'),
		placeholder: propTypeDeDonnée(sId, 'placeholder'),
		pattern: propTypeDeDonnée(sId, 'pattern')
	}
	return config;
}
function obtCodeHtmlInputEmail(config, sValeur = '') {
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	sDébutHtml 		= `<input type='email'`;
	sMilieuHtml		= '';
	sFinHtml 		= `>`;
	vérifTypeConfig(config);
	// si valeur non passée en config, on applique une valeur par défaut
	if (config.maxlength === '')  	{ config.maxlength 		= 254 }
	Object._deletePropIf(config, '', '===', '');
	// génère le code à partir de la config
	Object.keys(config).forEach(function(nomProp) {
		sMilieuHtml += ` ${nomProp}="${config[nomProp]}"`;
	})
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + ` value="${sValeur}"` + sFinHtml;
	return sCodeHtmlInput;
}

/**
 * Crée la configuration de l'élément input[type='number'] à générer
 * cette config sera transmise à obtCodeHtmlInputEmail()
 * @param {?String} sId Id du type de donnée concerné (ex: 'age')
 * @return {Object} Objet contenant les différents attributs de l'input à créer ('min', 'max', 'step'...)
 */
function obtConfigInputNumber(sId = 'age') {
	let config = {
		min: propTypeDeDonnée(sId, 'valeur_min'),
		max: propTypeDeDonnée(sId, 'valeur_max'),
		step: propTypeDeDonnée(sId, 'pas')
	}
	return config;
}
function obtCodeHtmlInputNumber(config, sValeur = '') {
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	sDébutHtml 		= `<input type='number'`;
	sMilieuHtml		= '';
	sFinHtml 		= `>`;
	vérifTypeConfig(config);
	// si valeur non passée en config, on applique une valeur par défaut
	//if (config.step === '')  	{ config.step 		= 1 }
	Object._deletePropIf(config, '', '===', '');
	// génère le code à partir de la config
	Object.keys(config).forEach(function(nomProp) {
		sMilieuHtml += ` ${nomProp}="${config[nomProp]}"`;
	})
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + ` value="${sValeur}"` + sFinHtml;
	return sCodeHtmlInput;
}

/**
 * Crée la configuration de l'élément input[type='date'] à générer
 * cette config sera transmise à obtCodeHtmlInputEmail()
 * @param {?String} sId Id du type de donnée concerné (ici: 'date')
 * @return {Object} Objet contenant les différents attributs de l'input à créer ('min', 'max'...)
 */
function obtConfigInputDate(sId = 'date') {
	let config = {
		min: propTypeDeDonnée(sId, 'valeur_min'),
		max: propTypeDeDonnée(sId, 'valeur_max'),
	}
	return config;
}
function obtCodeHtmlInputDate(config, sValeur = '') {
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	sDébutHtml 		= `<input type='date'`;
	sMilieuHtml		= '';
	sFinHtml 		= `>`;
	vérifTypeConfig(config);
	// si valeur non passée en config, on applique une valeur par défaut
	//if (config.step === '')  	{ config.step 		= 1 }
	Object._deletePropIf(config, '', '===', '');
	// génère le code à partir de la config
	Object.keys(config).forEach(function(nomProp) {
		sMilieuHtml += ` ${nomProp}="${config[nomProp]}"`;
	})
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + ` value="${sValeur}"` + sFinHtml;
	return sCodeHtmlInput;
}

/**
 * Crée la configuration de l'élément select à générer
 * cette config sera transmise à obtCodeHtmlSelect()
 * @param {?String} sId Id du type de donnée concerné (ex: 'sexe')
 * @return {Object} Objet contenant des infos pour aider à la génération du code Html de l'objet
 */
function obtConfigSelect(sId = 'sexe') {
	let config = {}, valeursPossibles = propTypeDeDonnée(sId, 'valeurs_possibles');
	if (Array.isArray(valeursPossibles)) {
		config.sTypeValeursPossibles = 'array';
	} else if (typeof(valeursPossibles) === 'object') {
		config.sTypeValeursPossibles = 'object';
	}
	config.sFormatAffichage = propTypeDeDonnée(sId, 'format_affichage');
	return config;
}
function obtCodeHtmlSelect(config, sValeur = '', sId = 'sexe', testKeyWithUpperAndLowerCase = true) {
	let sSelected, sValeurAffichée, valeursPossibles = propTypeDeDonnée(sId, 'valeurs_possibles');
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	vérifTypeConfig(config);
	sDébutHtml 		= `<select>`;
	sMilieuHtml		= '';
	// génère le code à partir de la config
	if (config.sTypeValeursPossibles === 'array') {
		valeursPossibles.forEach(function(valeur) {
			sValeurAffichée = obtDonnéeFormatée(valeur, config.sFormatAffichage, undefined, false);
			sSelected = valeur.toUpperCase() === sValeur.toUpperCase() ? ' selected' : '';
			sMilieuHtml += `<option value="${valeur}"${sSelected}>${sValeurAffichée}</option>`;
		})
	} else if (config.sTypeValeursPossibles === 'object') {
		Object.keys(valeursPossibles).forEach(function(nomProp) {
			sValeurAffichée = valeursPossibles[nomProp];
			sValeurAffichée = obtDonnéeFormatée(sValeurAffichée, config.sFormatAffichage, undefined, false);
			sSelected = nomProp.toUpperCase() === sValeur.toUpperCase() ? ' selected' : '';
			sMilieuHtml += `<option value="${nomProp}"${sSelected}>${sValeurAffichée}</option>`;
		})
	}
	sFinHtml 		= `</select>`;
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + sFinHtml;
	return sCodeHtmlInput;
}

/**
 * Crée la configuration de l'élément input[type='date'] à générer
 * cette config sera transmise à obtCodeHtmlInputEmail()
 * @param {?String} sId Id du type de donnée concerné (ici: 'date')
 * @return {Object} Objet contenant les différents attributs de l'input à créer ('min', 'max'...)
 */
function obtConfigBoolean(sId = 'est_actif') {
	let config = {
		format_affichage: propTypeDeDonnée(sId, 'format_affichage'),
	}
	return config;
}
function obtCodeHtmlBoolean(config, sValeur = false) {
	let sDébutHtml, sMilieuHtml, sFinHtml, sCodeHtmlInput;
	sDébutHtml 		= `<input type='checkbox' class='${config.format_affichage}'`;
	sMilieuHtml		= '';
	sFinHtml 		= `>`;
	vérifTypeConfig(config);
	// si valeur non passée en config, on applique une valeur par défaut
	//if (config.step === '')  	{ config.step 		= 1 }
	if (sValeur)  { sMilieuHtml += ` checked`}
	sCodeHtmlInput 	= sDébutHtml + sMilieuHtml + sFinHtml;
	return sCodeHtmlInput;
}


// pattern : possible sur input de type...text, search, password, url, tel, email
oConfig.oTemplatesColonnes.prenom = {
	'libellé' :					"Prénom client",
	'id' : 						"prenom",
	'type' : 					"texte",
	'nul_possible' : 			false,
	'format_stockage' :			"min",
	'format_affichage' :		"PremièreLettreMAJ",
	'taille_min' :				2,
	'taille_max' :				61,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'regex' :					/^([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+([-]([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+)*$/i,
	'pattern' :					`^([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+([-]([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+)*$`, // '(?i)' avant ne marche pas
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		let m;
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!this.estValideTaille(sDonnée)) {
			return false;
		};
		return ((m = this.regex.exec(sDonnée)) !== null)
	},
	formaterPourAffichage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_affichage);
	},
	formaterPourStockage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_stockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputText('prenom');
		return obtCodeHtmlInputText(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtDonneeBdd: function(id = 0, oConfigTable = oConfig) {

	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return donneeStockee.capitalize();
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return Input.html( 'text', { value: this.obtContenuCell(donneeStockee) } );
	}
}
oConfig.oTemplatesColonnes.nom = {
	'libellé' :					"Nom client",
	'id' : 						"nom",
	'type' : 					"texte",
	'nul_possible' : 			false,
	'format_stockage' :			"min",
	'format_affichage' : 		"MAJ",
	'taille_min' :				2,
	'taille_max' :				122,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'regex' :					/^([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+([-]([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+)*$/i,
	'pattern' :					`^([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+([-]([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+)*$`,
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		let m;
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!this.estValideTaille(sDonnée)) {
			return false;
		};
		return ((m = this.regex.exec(sDonnée)) !== null)
	},
	formaterPourAffichage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_affichage);
	},
	formaterPourStockage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_stockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputText('nom');
		return obtCodeHtmlInputText(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return donneeStockee.toUpperCase();
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return Input.html( 'text', { value: this.obtContenuCell(donneeStockee) } );
	}
}
oConfig.oTemplatesColonnes.email = {
	'libellé' :					"E-mail",
	'id' : 						"email",
	'type' : 					"texte",
	'nul_possible' : 			false,
	'format_stockage' :			"email",
	'format_affichage' : 		"email",
	'taille_min' :				6,
	'taille_max' :				254,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'regex' :					/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i,
	'pattern' :					"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		let m;
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!this.estValideTaille(sDonnée)) {
			return false;
		};
		return ((m = this.regex.exec(sDonnée)) !== null)
	},
	formaterPourAffichage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_affichage);
	},
	formaterPourStockage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_stockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputText('email');
		return obtCodeHtmlInputEmail(config, oConfigTable.aLignes[idLigne][this.id])
	}
}	// todo functions + enum
oConfig.oTemplatesColonnes.age = {
	'libellé' :					"Age",
	'id' : 						"age",
	'type' : 					"nombre",
	'nul_possible' : 			true,
	'format_stockage' :			"entier",
	'format_affichage' : 		"#{ENT} ans",
	'taille_min' :				1,
	'taille_max' :				3,
	'valeur_min' :				0,
	'valeur_max' :				140,
	'pas' :						1,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	estValideTaille: function(donnée){
		donnée = '' + donnée;
		return (donnée.length >= this.taille_min && donnée.length <= this.taille_max);
	},
	estValide: function(donnée){
		if (sDonnée === undefined)  { return false; }
		let bNombreEntier, bTailleValide, bValeurDansIntervalle;
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		bNombreEntier = ('' + donnée).isStrInteger();
		if (!bNombreEntier)  { return false; }
		bTailleValide = (this.estValideTaille(donnée));
		if (!bTailleValide)  { return false; }
		bValeurDansIntervalle = (donnée >= this.valeur_min && donnée <= this.valeur_max);
		if (!bValeurDansIntervalle)  { return false; }
		return true;
	},
	formaterPourAffichage: function(sDonnée){
		return sDonnée + (sDonnée <= 1 ? " an" : " ans");
	},
	formaterPourStockage: function(sDonnée){
		return sDonnée;
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputNumber('age');
		return obtCodeHtmlInputNumber(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return donneeStockee + ' ans';
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id };
		if (donneeStockee != undefined && donneeStockee > 0)  { oAttr.value = donneeStockee };
		return Input.html( 'number', oAttr );
	}
}
oConfig.oTemplatesColonnes.sexe = {
	'libellé' :					"Sexe",
	'id' : 						"sexe",
	'type' : 					"enum",
	'nul_possible' : 			false,
	'valeurs_possibles' :		{ 'H': "homme", 'F': 'femme' }, // ['homme', 'femme']   //   { 'H': "homme", 'F': 'femme' }
	'sensible_casse' :			false,
	'format_stockage' :			"texte",
	'format_affichage' : 		"PremièreLettreMAJ",
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	estValideTaille: function(sDonnée){
		let bTailleRespectée;
		if (Array.isArray(this.valeurs_possibles)) {
			bTailleRespectée = true;
		} else if (typeof(this.valeurs_possibles) === 'object') {
			bTailleRespectée = (sDonnée.length === 1);
		}
		return bTailleRespectée;
	},
	estValide: function(sDonnée){
		if (sDonnée === undefined)  { return false; }
		let bValeurExiste = false;
		let bTailleRespectée = this.estValideTaille(sDonnée);
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!bTailleRespectée)  { return false; }
		if (this.sensible_casse === true) {
			bValeurExiste = (this.valeurs_possibles[sDonnée] !== undefined);
		} else {
			Object.keys(this.valeurs_possibles).forEach(function(clé) {
				if (clé.toUpperCase() === sDonnée.toUpperCase()) {
					bValeurExiste = true;
				}
			})
		}
		return bValeurExiste;
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigSelect('sexe');
		return obtCodeHtmlSelect(config, oConfigTable.aLignes[idLigne][this.id], this.id)
	}
}	// todo functions + enum
oConfig.oTemplatesColonnes.date = {
	'libellé' :					"Date",
	'id' : 						"date",
	'type' : 					"date",
	'nul_possible' : 			true,
	'format_stockage' : 		"#{aaaa}-#{mm}-#{jj}", 				// "aaaa-mm-jj"
	'format_affichage' : 		"#{Jjjj} #{j} #{Mmm} #{aaaa}", 		// "Jjj jj Mmm aaaa"
	'taille_min' :				8,
	'taille_max' :				10,
	'valeur_min' :				"1980-06-01",
	'valeur_max' :				"2040-05-31",
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'input' : 					{
		'type' :				"date",
		'class' : 				""
	},
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		if (sDonnée === undefined)  { return false; }
		let m, bTailleRespectée, bDateDansIntervalle, bDateExistante;
		if (sDonnée === '' && this.nul_possible === true)  { return true; }
		bTailleRespectée = (this.estValideTaille(sDonnée));
		if (!bTailleRespectée)  { return false; }
		sDonnée = obtDateAvecZéro(sDonnée);
		bDateExistante = (estDateExistante(sDonnée));
		if (!bDateExistante)  { return false; }
		bDateDansIntervalle = (sDonnée >= this.valeur_min && sDonnée <= this.valeur_max);
		if (!bDateDansIntervalle)  { return false; }
		return true;
	},
	formaterPourAffichage: function(dDate, sFormatPourAffichage = 'auto'){
		if (sFormatPourAffichage === 'auto')  { sFormatPourAffichage = this.format_affichage }
		return obtDonnéeFormatée(dDate, sFormatPourAffichage);
	},
	formaterPourStockage: function(dDate, sFormatPourStockage = 'auto'){
		if (sFormatPourStockage === 'auto')  { sFormatPourStockage = this.format_stockage }
		return obtDonnéeFormatée(dDate, sFormatPourStockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputDate('date');
		return obtCodeHtmlInputDate(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let bDateActuelleOuFuture, dToday, dStockee;
		if (donneeStockee === '' || donneeStockee === undefined) {
			donneeStockee = '-';
		} else {
			dToday = new Date();
			dStockee = new Date(donneeStockee);
			bDateActuelleOuFuture = (dStockee >= dToday);
			donneeStockee = new Date(donneeStockee).JJMMAAAA();
			if (bDateActuelleOuFuture)  { donneeStockee = `<b>` + donneeStockee + `</b>`; }
		}
		return donneeStockee;
	}
}
oConfig.oTemplatesColonnes.bouton = {
	'type' : 					"bouton",
	'triable' : 				false,
	'éditable' : 				false,
	'filtrable' : 				false,
	obtContenuCell: function() {
		return this.contenu;
	}
}
oConfig.oTemplatesColonnes.est_actif = {
	'libellé' :					"Actif",
	'id' : 						"est_actif",
	'type' : 					"boolean",
	'nul_possible' : 			true,
	'format_stockage' :			"boolean",
	'format_affichage' : 		"switch",
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	'valdefaut_pour_creation':	true,
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigBoolean('est_actif');
		return obtCodeHtmlBoolean(config, oConfigTable.aLignes[idLigne][this.id])
	},
	estValide: function(sDonnée){
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		return (typeof(sDonnée) === 'boolean');
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id,  class:'switch',  checked: donneeStockee,  disabled : true };
		return Checkbox.html( oAttr );
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id,  class:'switch',  checked: donneeStockee };
		return Checkbox.html( oAttr, `Etat de la promo :` );
	}
}
oConfig.oTemplatesColonnes.type_promo = {
	'libellé' :					"Type",
	'id' : 						"type_promo",
	'type' : 					"enum",
	'nul_possible' : 			false,
	'valeurs_possibles' :		{ 'euro': "€", 'pourcent': '%', 'aliment': 'Aliment'},
	'sensible_casse' :			false,
	'format_stockage' :			"texte",
	'format_affichage' : 		"PremièreLettreMAJ",
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	'valdefaut_pour_creation':	'euro',
	estValideTaille: function(sDonnée){
		let bTailleRespectée;
		if (Array.isArray(this.valeurs_possibles)) {
			bTailleRespectée = true;
		} else if (typeof(this.valeurs_possibles) === 'object') {
			bTailleRespectée = (sDonnée.length === 1);
		}
		return bTailleRespectée;
	},
	estValide: function(sDonnée){
		if (sDonnée === undefined)  { return false; }
		let bValeurExiste = false;
		let bTailleRespectée = this.estValideTaille(sDonnée);
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!bTailleRespectée)  { return false; }
		if (this.sensible_casse === true) {
			bValeurExiste = (this.valeurs_possibles[sDonnée] !== undefined);
		} else {
			Object.keys(this.valeurs_possibles).forEach(function(clé) {
				if (clé.toUpperCase() === sDonnée.toUpperCase()) {
					bValeurExiste = true;
				}
			})
		}
		return bValeurExiste;
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigSelect('sexe');
		return obtCodeHtmlSelect(config, oConfigTable.aLignes[idLigne][this.id], this.id)
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return new Enum(this.id, donneeStockee).html('', true);
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return new Enum(this.id, donneeStockee, { id: this.id }).html(`Quel avantage souhaitez vous apporter :`);
	}
}
oConfig.oTemplatesColonnes.code_promo = {
	'libellé' :					"Code",
	'id' : 						"code_promo",
	'type' : 					"texte",
	'nul_possible' : 			false,
	'format_stockage' :			"min",
	'format_affichage' : 		"MAJ",
	'taille_min' :				5,
	'taille_max' :				15,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'regex' :					/^([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+([-]([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+)*$/i,
	'pattern' :					`^([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+([-]([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+)*$`,
	'valdefaut_pour_creation':	'',
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		let m;
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!this.estValideTaille(sDonnée)) {
			return false;
		};
		return ((m = this.regex.exec(sDonnée)) !== null)
	},
	formaterPourAffichage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_affichage);
	},
	formaterPourStockage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_stockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputText('nom');
		return obtCodeHtmlInputText(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return (donneeStockee === '' || donneeStockee === undefined) ?
			`<i>appli. auto</b>`:
			donneeStockee.toUpperCase();
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id : this.id,  value: donneeStockee };
		return Input.html( 'text', oAttr, 'Code promo <i>(application automatique si vide)</i> :' );
	}
}
oConfig.oTemplatesColonnes.description = {
	'libellé' :					"Description",
	'id' : 						"description",
	'type' : 					"texte",
	'nul_possible' : 			false,
	'format_stockage' :			"min",
	'format_affichage' :		"PremièreLettreMAJ",
	'taille_min' :				0,
	'taille_max' :				150,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'regex' :					/^([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+([-]([a-zÀ-ÿ]+(( |')[a-zÀ-ÿ]+)*)+)*$/i,
	'pattern' :					`^([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+([-]([a-zA-ZÀ-ÿ]+(( |')[a-zA-ZÀ-ÿ]+)*)+)*$`, // '(?i)' avant ne marche pas
	'valdefaut_pour_creation':	'',
	estValideTaille: function(sDonnée){
		return (sDonnée.length >= this.taille_min && sDonnée.length <= this.taille_max);
	},
	estValide: function(sDonnée){
		let m;
		if (sDonnée === undefined)  { return false; }
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		if (!this.estValideTaille(sDonnée)) {
			return false;
		};
		return ((m = this.regex.exec(sDonnée)) !== null)
	},
	formaterPourAffichage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_affichage);
	},
	formaterPourStockage: function(sDonnée){
		return oConfig.oTemplatesColonnes.formaterTexte(sDonnée, this.format_stockage);
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputText('prenom');
		return obtCodeHtmlInputText(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		return donneeStockee;
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let sResult = `<label for="${this.id}">Ici vous pouvez saisir un commentaire <i>visible par vous uniquement</i> :</label>`;
		sResult += `\n<textarea id="${this.id}" name="${this.id}" cols="40" rows="5">${donneeStockee}</textarea>`;
		return sResult;
	}
}
oConfig.oTemplatesColonnes.quantite = {
	'libellé' :					"Quantité",
	'id' : 						"quantite",
	'type' : 					"nombre",
	'nul_possible' : 			true,
	'format_stockage' :			"entier",
	'format_affichage' : 		`#{ENT} / 15`,
	'taille_min' :				1,
	'taille_max' :				2,
	'valeur_min' :				0,
	'valeur_max' :				10,
	'pas' :						1,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	'valdefaut_pour_creation':	0,
	estValideTaille: function(donnée){
		donnée = '' + donnée;
		return (donnée.length >= this.taille_min && donnée.length <= this.taille_max);
	},
	estValide: function(donnée){
		if (sDonnée === undefined)  { return false; }
		let bNombreEntier, bTailleValide, bValeurDansIntervalle;
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		bNombreEntier = ('' + donnée).isStrInteger();
		if (!bNombreEntier)  { return false; }
		bTailleValide = (this.estValideTaille(donnée));
		if (!bTailleValide)  { return false; }
		bValeurDansIntervalle = (donnée >= this.valeur_min && donnée <= this.valeur_max);
		if (!bValeurDansIntervalle)  { return false; }
		return true;
	},
	formaterPourAffichage: function(donnée){
		return donnée + ` / ${this.valeur_max}`;
	},
	formaterPourStockage: function(sDonnée){
		return sDonnée;
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputNumber('age');
		return obtCodeHtmlInputNumber(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		if (donneeStockee === 0 || donneeStockee === undefined)  { donneeStockee = 0; }
		if (donneeStockee > 0)  { donneeStockee = `<b>` + donneeStockee + `</b>` }
		return donneeStockee + ' / ' + this.valeur_max;
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id : this.id,  value: donneeStockee };
		return Input.html( 'number', oAttr, 'Limite de quantité :' );
	}
}
oConfig.oTemplatesColonnes.valeur = {
	'libellé' :					"Valeur",
	'id' : 						"valeur",
	'type' : 					"nombre",
	'nul_possible' : 			true,
	'format_stockage' :			"entier",
	'format_affichage' : 		`#{ENT}`,
	'taille_min' :				1,
	'taille_max' :				2,
	'valeur_min' :				0,
	'valeur_max' :				25,
	'pas' :						1,
	'triable' : 				true,
	'éditable' : 				true,
	'filtrable' : 				true,
	'mode_de_filtrage' : 		'sélection',
	'valdefaut_pour_creation':	10,
	estValideTaille: function(donnée){
		donnée = '' + donnée;
		return (donnée.length >= this.taille_min && donnée.length <= this.taille_max);
	},
	estValide: function(donnée){
		if (sDonnée === undefined)  { return false; }
		let bNombreEntier, bTailleValide, bValeurDansIntervalle;
		if (sDonnée === '' && this.nul_possible == true)  { return true; }
		bNombreEntier = ('' + donnée).isStrInteger();
		if (!bNombreEntier)  { return false; }
		bTailleValide = (this.estValideTaille(donnée));
		if (!bTailleValide)  { return false; }
		bValeurDansIntervalle = (donnée >= this.valeur_min && donnée <= this.valeur_max);
		if (!bValeurDansIntervalle)  { return false; }
		return true;
	},
	formaterPourAffichage: function(donnée){
		return donnée + ` / ${this.valeur_max}`;
	},
	formaterPourStockage: function(sDonnée){
		return sDonnée;
	},
	obtCodeHtmlObjet: function(idLigne = 0, oConfigTable = oConfig) {
		let config = obtConfigInputNumber('age');
		return obtCodeHtmlInputNumber(config, oConfigTable.aLignes[idLigne][this.id])
	},
	obtContenuCell: function(jeuDeDonnees, donneeStockee = jeuDeDonnees[this.id]) {
		return donneeStockee;
	},
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id,  value: donneeStockee };
		return Input.html( 'number', oAttr, 'Valeur de la promotion :');
	}
}


// COLONNES (TYPES DE DONNEES + BOUTONS)

/**
 * Renvoie une colonne à partir de son id
 * @param {String} sId Id de la colonne (ex:"prenom")
 * @return {Ref:oConfigTable.aColonnes[index]} Référence vers le bon index de oConfigTable.aColonnes (déterminé automatiquement grâce à l'id fourni)
 */
function aColDepuisId(sId = 'prenom', oConfigTable = oConfig) {
	let sClé;
	oConfigTable.aColonnes.forEach(function(valeur, clé) {
		if (oConfigTable.aColonnes[clé].id === sId) {
			sClé = clé;
		}
	})
	return oConfigTable.aColonnes[sClé];
}

// COLONNES :  FONCTIONS
/**
 * Renvoie le nombre de colonnes contenues dans oConfig.aColonnes
 * possibilité de filtrer les résultats en ne ciblant que les colonnes
 * dont la propriété 'sNomProprieteCherchee' vaut la valeur 'valeurCherchee'
 * @param {String} sNomProprieteCherchee Nom de la propriété dont on cherchera la valeur
 * @param {*} valeurCherchee Valeur de la propriété à tester
 * @param {Object} oConfigTable
 * @return {Integer} Nombre de colonnes
 */
oConfig.fnCol.obtNbColonnes = function(sNomProprieteCherchee = '', valeurCherchee = '', oConfigTable = oConfig) {
	let iNbColonnes = 0;
	if (sNomProprieteCherchee === '') {
		return oConfigTable.aColonnes.count();
	} else {
		oConfigTable.aColonnes.forEach(function(valeur) {
			if (valeur[sNomProprieteCherchee] === valeurCherchee) {
				iNbColonnes++;
			}
		})
	}
	return iNbColonnes;
}
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne = function(oTemplateColonne, oColonneAvecProprietesPrioritaires = {}, oConfigTable = oConfig) {
	if (typeof(oTemplateColonne) === 'object') {		// un template est fourni, on y ajoute les autres propriétés
		ajouterPropriétésObj(oColonneAvecProprietesPrioritaires, oTemplateColonne);
		oConfigTable.aColonnes.push(oColonneAvecProprietesPrioritaires);
	}
}
oConfig.fnCol.insererColonneDansArray = function(oColonne, aColonnes = 'todo....') {
	if (Array.isArray(oColonne)) {					// oColonne est un array... erreur
		errTypeColonne();
	} else if (typeof(oColonne) === 'object') {		// oColonne est un objet... ok
		insererColonneDansArraySansVerifier(oColonne, this);
	} else {										// autre type... erreur
		errTypeColonne();
	}
	function insererColonneDansArraySansVerifier(oColonne, aCible) {
		if (Array.isArray(aCible)) {				// oColonne est un array... ok
			aCible.push(oColonne);
		} else {									// sinon... erreur
			alert(`insererColonneDansArray() n'accepte qu'une cible de type array`);
		}
	}
	function errTypeColonne() {
		alert(`oConfig.aColonnes.insererColonneDansArray() n'accepte qu'un oColonne de type objet`);
	}
}
oConfig.fnCol.colDepuisId = function(sIdColonne, oConfigTable = oConfig) {
	return obtenirIndexDansTableauSiCorrespondance(oConfigTable.aColonnes, 'sId', sIdColonne);
}
oConfig.aColonnes.obtOrdreAffichageColonnes = function(oConfigTable = oConfig) {
	var iNbColonnes = this.length;
	var iPosition_dans_tableau;
	var i;
	var iCompteurTours = 0;
	var iCompteurposition_dans_tableauCorrespond = 0;
	// aPosition_dans_tableauColonnes	-> position_dans_tableau des colonnes par ordre croissant
	for (i = 0; i < iNbColonnes; i++) {
		iPosition_dans_tableau = this[i]['position_dans_tableau'];
		insérer_valeur_et_trier_tableau_croissant(aPosition_dans_tableauColonnes, iPosition_dans_tableau);
	}
	// aIndexColonnesSelonPositionDansTableau	-> index des colonnes par ordre de position_dans_tableau croissant
	for (iCompteurTours = 0; iCompteurTours < iNbColonnes; iCompteurTours++) {
		iPosition_dans_tableauCherchée = aPosition_dans_tableauColonnes[iCompteurTours];
		for (let i = 0; i < iNbColonnes; i++) {
			iPosition_dans_tableauColonne = this[i]['position_dans_tableau'];
			if (iPosition_dans_tableauColonne == iPosition_dans_tableauCherchée) {
				if (this[i]['visible_dans_tableau']) {
					aIndexColonnesSelonPositionDansTableau.push(i);
				}
				iCompteurposition_dans_tableauCorrespond++;
			}
		}
	}
	//clog('aPosition_dans_tableauColonnes', aPosition_dans_tableauColonnes);
	//clog('aIndexColonnesSelonPositionDansTableau', aIndexColonnesSelonPositionDansTableau);
	// ajoute propriété 'index_colonne' à chaque colonne dans this[]
	for (i = 0 ; i < iNbColonnes; i++) {
		iIndexColonne = aIndexColonnesSelonPositionDansTableau[i];
		if (iIndexColonne !== undefined) {
			this[iIndexColonne]['index_colonne'] = i;
		}
	}
}
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.nom, {				// COLONNE  NOM
	'visible_dans_tableau' : 	false,
	'position_dans_tableau' : 	0
})


// COLONNES :  DONNEES
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.date, {				// COLONNE  DATE D'INSCRIPTION
	'visible_dans_tableau' : 	true,
	'libellé' :					"Date de début",
	'id' : 						"date_debut",
	// 'format_affichage' : 		"#{Jjj} #{j} #{Mmm} #{aaaa}",
	'valeur_min' :				"2019-12-31",
	'valeur_max' :				"2021-01-31",
	'position_dans_tableau' : 	10,
	'formulaire':				{
		emplacement:					'.date_debut'
	},
	'valdefaut_pour_creation':	new Date().AAAAMMJJ(),
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id };
		if (donneeStockee != undefined && donneeStockee !== '')  { oAttr.value = donneeStockee };
		return Input.html( 'date', oAttr, `Date de début :` );
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.date, {				// COLONNE  DATE D'INSCRIPTION
	'visible_dans_tableau' : 	true,
	'libellé' :					"Date de fin",
	'id' : 						"date_fin",
	'formulaire':				{
		emplacement:					'.date_fin'
	},
	// 'format_affichage' : 		"#{Jjj} #{j} #{Mmm} #{aaaa}",
	'valeur_min' :				"2019-12-31",
	'valeur_max' :				"2021-01-31",
	'position_dans_tableau' : 	20,
	'valdefaut_pour_creation':	new Date().AAAAMMJJ(),
	obtContenuFormulaire: function(oJeuDeDonnees, donneeStockee = oJeuDeDonnees[this.id]) {
		let oAttr = { id: this.id };
		if (donneeStockee != undefined && donneeStockee !== '')  { oAttr.value = donneeStockee };
		return Input.html( 'date', oAttr, `Date de fin :` );
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.code_promo, {			// COLONNE  NOM
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	30,
	'formulaire':				{
		emplacement:					'.code_promo'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.sexe, {				// COLONNE  SEXE
	'visible_dans_tableau' : 	false,
	'position_dans_tableau' : 	40
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.valeur, {				// COLONNE  VALEUR
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	50,
	'formulaire':				{
		emplacement:					'.valeur'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.type_promo, {			// COLONNE  TYPE DE PROMOTION
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	60,
	'formulaire':				{
		emplacement:					'.type_promo'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.description, {		// COLONNE  DESCRIPTION
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	70,
	'formulaire':				{
		emplacement:					'.description'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.quantite, {			// COLONNE  QUANTITE
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	75,
	'formulaire':				{
		emplacement:					'.quantite'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.est_actif, {			// COLONNE  ABONNÉ À LA NEWSLETTER
	'visible_dans_tableau' : 	true,
	'position_dans_tableau' : 	80,
	'formulaire':				{
		emplacement:					'.est_actif'
	}
})
oConfig.fnCol.insererColonneDansArrayDepuisTemplateColonne(oConfig.oTemplatesColonnes.bouton, {				// COLONNE  BOUTON EDITER / SUPPRIMER
	'visible_dans_tableau' : 	true,
	'libellé' :					"",
	'id' : 						"editer_supprimer", //
	'contenu' : 				`<div>   <span class="btn btn-éditer fas fa-pencil-alt"></span>   <span class='btn btn-supprimer fas fa-trash-alt'></span>   </div>`,
	'position_dans_tableau' : 	90
})

oConfig.aColonnes.obtOrdreAffichageColonnes(oConfig);
oConfig.col = cfgCol(oConfig);
/**
 * Renvoie un jeu de données contenant les valeurs par défaut pour la création d'une promo
 * @param {*} oConfigTable
 * @param {*} bInclureBoutonsEventuels
 */
oConfig.col.obtJeuDeDonneesParDefautPourCreationPromo = function(oConfigTable = oConfig, bInclureBoutonsEventuels = false) {
	// todo.... définir valeur par défaut pour colonnes dans aCol, et remplacer col par aCol dans cette fonction
	let oJeuDeDonnees = {};
	let sNomProp, sTypeProp;
	for (let i = 0; i < oConfigTable.col.length; i++) {
		oColonne = oConfigTable.col[i];
		sNomProp = oColonne.id;
		sTypeProp = oColonne.type;
		if (sTypeProp === 'bouton') {
			if (bInclureBoutonsEventuels)  { oJeuDeDonnees[sNomProp] = oColonne.contenu; }
			continue;
		}
		oJeuDeDonnees[sNomProp] = oColonne.valdefaut_pour_creation;
	}
	return oJeuDeDonnees;
}
/**
 * Insère un jeu de données dans le datatable
 * @param { [{}..]  |  {{}..}  |  {}  |  [] } jeuDeDonnees Jeu de données à insérer
 * @param {*} oConfigTable
 */
oConfig.col.insérerJeuDeDonneesDansDataTable = function(jeuDeDonnees, oConfigTable = oConfig) {
	let sTypeJeuDeDonnees = typeOf(jeuDeDonnees);
	let oColonnes = this;
	let iColonnes = oColonnes.length;
	let iLignes = Object.keys(jeuDeDonnees).length;
	let sIdColonne, sNomProp;
	let aLignePourDataTable;
	let valStockee, sContenuCellule;
	let oLigneDataTable;
	if (sTypeJeuDeDonnees === 'array' && jeuDeDonnees.containsOnlyType('object', false)) {	// si le tableau ne contient que des objets (array inclus)
		jeuDeDonnees.forEach(function(oLigne) {
			oColonnes.insérerJeuDeDonneesDansDataTable(oLigne);		// ...récursivité
		})
	} else if (sTypeJeuDeDonnees === 'object' && Object._containsOnlyType(jeuDeDonnees, 'object', false)) {	// si l'objet ne contient que des objets (array inclus)
		jeuDeDonnees.forEach(function(oLigne) {
			oColonnes.insérerJeuDeDonneesDansDataTable(oLigne);		// ...récursivité
		})
	} else if (sTypeJeuDeDonnees === 'array') {
		if (jeuDeDonnees.count() !== oConfigTable.col.count())  {
			throw new Error(`insérerJeuDeDonneesDansDataTable() : le nombre de propriétés du jeu de données n'est pas égal au nombre de colonnes du datatable.`);
		}
		// on affiche la ligne dans le dataTable
		oLigneDataTable = oConfigTable.oDataTable.row.add(jeuDeDonnees).node();
		oConfigTable.oDataTable.draw();
		// on lui attribue un id
		oLigneDataTable.id = '' + oConfigTable.var.iNbLignesInserees;
		oConfigTable.var.iNbLignesInserees++;
	} else if (sTypeJeuDeDonnees === 'object') {
		aLignePourDataTable = [];
		for (let c = 0; c < iColonnes; c++) {
			sIdColonne = oColonnes[c].id;
			// ici on a correspondance entre id colonne et propriété de ligne
			valStockee = jeuDeDonnees[sIdColonne];
			sContenuCellule = oColonnes[c].obtContenuCell( jeuDeDonnees ) ?? '';
			aLignePourDataTable.push( sContenuCellule );
		}
		oConfig.col.insérerJeuDeDonneesDansDataTable(aLignePourDataTable, oConfigTable);	// on envoie l'array en récursivité
	}
}

function editerPromo(sId = undefined, oConfigTable = oConfig) {
	let sIdConteneurForm = oConfigTable.const.sIdConteneurFormulaires;
	let oJeuDeDonnees, iColInArray, oCol, oCles;
	let sNomProp, valeur, sHtmlFormulaire, oFormulaire;
	$('#' + sIdConteneurForm).addClass(`d-none`);
	if (sId == undefined) {
		oJeuDeDonnees = oConfig.col.obtJeuDeDonneesParDefautPourCreationPromo();
	} else {
		oJeuDeDonnees = oConfigTable.aLignes[+sId];
		oConfigTable.var.sIdEdition = sId;
		$(`tr[id=${sId}]`).children('td').addClass('edition');
	}
	oCles = Object.keys(oJeuDeDonnees);
	for (let i = 0; i < oCles.length; i++) {
		sNomProp = oCles[i];
		valeur = oJeuDeDonnees[sNomProp];
		iColInArray = oConfigTable.col.getKeyObjInArrayWhere('id', '===', sNomProp);
		if (iColInArray === undefined)  { continue; }
		oCol = oConfigTable.col[iColInArray];
		// ici on est positionné sur la ligne à éditer et on a ciblé la colonne dans l'array
		// en dessous on a filtré que les propriétés éditables et visibles et dont la valeur a été définie
		sHtmlFormulaire = oCol.obtContenuFormulaire(oJeuDeDonnees);
		oFormulaire = $(oCol.formulaire.emplacement);
		oFormulaire.html(sHtmlFormulaire);
		oFormulaire.attr('id', '_' + sNomProp);
	}
	$('#' + sIdConteneurForm).removeClass(`d-none`);
	$('#' + sIdConteneurForm).children().on('change', function(){
		$('#form_btn_enregistrer').removeClass(`d-none`);
	})

	$('#form_btn_annuler').on('click', function(){
		annulerEdition();
	})
	$('#form_btn_enregistrer').on('click', function(){
		enregistrerDonneesFormulaires();
	})
}

//oConfig.col.insérerJeuDeDonneesDansDataTable();

oConfig.aLignes.config = {
	// id de la ligne  -->		ici valeur correspondant au nom		('_auto' pour auto-incrémentation)
	id:	'nom'
}
// LIGNES :  FONCTIONS
oConfig.aLignes.obtNbLignes = function() {
	return this.length;
}
/**
 * Ajoute le jeu de données fourni à l'array
 * @param {Array|Object} dataSet dataSet (objet OU array + objet contenant l'id de chaque colonne....todo)
 */
oConfig.aLignes.ajouterDataSet = function(dataSet, bVérifierDonnées = 'false...todo', bStockerDansBDD = 'false...todo', oConfigTable = oConfig) {
	let sTypeDataSet;
	if (typeOf(dataSet) === 'array') {						// dataSet est un array...erreur
		sTypeDataSet = 'array';
		// todo: vérifier si l'array dataSet ne contient que des objets non array
		// oui : appel récursif
		// non : erreur
		errTypeDataSet();
	} else if (typeOf(dataSet) === 'object') {
		sTypeDataSet = 'object';
		ajouterDataSetSansVérifier(dataSet, oConfigTable);	// dataSet est un objet... ok
	} else {
		errTypeDataSet();									// autre type... erreur
	}
	function ajouterDataSetSansVérifier(dataSet, oConfigTable) {
		if (Array.isArray(oConfigTable.aLignes)) {			// dataSet est un array... ok
			let iPremierIndex = oConfigTable.aLignes.length;
			let sIdColonne;
			let sIdLigne;
			//oConfigTable.aLignes[iPremierIndex] = dataSet;
			sIdLigne = obtIdLigne(dataSet, oConfigTable);
			//sIdLigne = dataSet[sIdLigne].toLowerCase();
			insererIdDansObjDataSet(dataSet, sIdLigne);	// todo finir pour auto_increment
			oConfigTable.aLignes.push(dataSet);
		} else {											// sinon... erreur
			alert(`ajouterDataSet() n'accepte qu'une cible de type array`);
		}
		function obtIdLigne(dataSet, oConfigTable, sIdLigneObtenuSelon = undefined) { // index_col_visible, id_col
			let sIdColonne, sIdLigne, iIndexColonne, oColonne;
			// va vérifier dans la config quelle colonne va servir pour attribuer l'id ligne
			if (sIdLigneObtenuSelon === undefined) {
				sIdLigneObtenuSelon = idLigneObtenuSelon(dataSet, oConfigTable);
				sIdLigne = obtIdLigne(dataSet, oConfigTable, sIdLigneObtenuSelon);
			} else if (sIdLigneObtenuSelon === 'id_colonne') {
				sIdColonne = oConfigTable.fnCol.estIdColonneExistant(oConfigTable.idColonne, false, '');
				// à ce stade sIdColonne contient le nom de l'id colonne qui servira à obtenir l'id ligne
				if (obtPropColDepuisId(sIdColonne, 'type') !== 'bouton') {
					sIdLigne = '' + dataSet[sIdColonne];
					return sIdLigne;
				}
				//clog(colDepuisId,sIdColonne)
				sIdLigne = obtIdLigneSiColNonBouton();
			} else if (sIdLigneObtenuSelon === 'index_colonne') {
				iIndexColonne = oConfigTable.col[iIndexColonne].id;
				sIdLigne = obtIdLigneSiColNonBouton();
			}
			return sIdLigne.toLowerCase();
			function obtIdLigneSiColNonBouton() {
				if (oConfigTable.col[oConfigTable.idColonne].type !== 'bouton') {
					return dataSet[iIndexColonne];
				} else {
					alert(`Erreur : oConfigTable.idColonne ne doit pas renvoyer vers une colonne de type 'bouton' car l'id des lignes ne peuvent pas être récupérés à partir de celle-ci`);
				}
			}
			function idLigneObtenuSelon(dataSet, oConfigTable) {
				if (typeof(oConfigTable.idColonne) === 'number') {
					// id nombre : l'index de la colonne existe ?	-> 'index_colonne'
					if (oConfigTable.fnCol.estIndexColonneExistant(oConfigTable.idColonne, oConfigTable)) {
						return 'index_colonne';
					}
				} else if (typeof(oConfigTable.idColonne) === 'string') {
					sIdColonne = oConfigTable.fnCol.estIdColonneExistant(oConfigTable.idColonne, false, '');
					// id texte : l'id de la colonne existe ? 		-> 'id_colonne'
					if (sIdColonne !== '') {
						return 'id_colonne';
					}
				}
				// sinon											-> 'auto_incrementation'
				return 'auto_incrementation';
			}
		}
	}
	function insererIdDansObjDataSet(dataSet, sId) {
		dataSet.id = sId;
	}
	function errTypeDataSet() {
		alert(`oConfig.aLignes.ajouterDataSet() n'accepte qu'un dataSet de type objet`);
	}
}
// oConfig.aLignes.idDepuisIndexArray = function(iIndexArray = 0) {
// 	if (typeof(oConfig.aLignes.config.id) === 'string') {

// 	}
// 	function obtIdCorrespondantAColonne() {

// 	}
// }

// LIGNES :  DONNEES
function insérerDonnéesJSDansArray(oConfigTable = oConfig) {
	oConfigTable.aLignes.ajouterDataSet({
		'date_debut' :				'2021-03-18',
		'date_fin' :				'2021-04-17',
		'code_promo':				'',
		'description':				'Un kg de carottes offertes',
		'quantite' : 				5,
		'valeur':					1,
		'type_promo':				'aliment',
		'code_promo':				'SIPOLA217',
		'est_actif':				true,
		'prenom' :					"Fred",
		'nom' : 					"Campan"
	})
	oConfigTable.aLignes.ajouterDataSet({
		'date_debut' :				'2019-10-04',
		'date_fin' :				'2019-10-20',
		'code_promo':				'',
		'description':				'5€ de réduction',
		'code_promo':				'MALIGO834',
		'valeur':					5,
		'type_promo':				'euro',
		'est_actif':				false,
		'prenom' :					"Sandra",
		'nom' : 					"Plafo"
	})
	oConfigTable.aLignes.ajouterDataSet({
		'date_debut' :				'2020-09-04',
		'date_fin' :				'2020-11-03',
		'code_promo':				'TABIRO514',
		'description':				'',
		'valeur':					15,
		'type_promo':				'pourcent',
		'tel' : 					"0600000003",
		'est_actif':				true,
		'prenom' :					"Bernard",
		'nom' : 					"Mila"
	})
	oConfigTable.aLignes.ajouterDataSet({
		'date_debut' :				'2020-09-30',
		'date_fin' :				'',
		'type_promo':				'euro',
		'code_promo':				'MEFALI597',
		'description':				'500g de tapenade offerts',
		'tel' : 					"0600000004",
		'valeur': 					.5,
		'type_promo':				'aliment',
		'quantite' : 				2,
		'est_actif':				true,
		'prenom' :					"Noé",
		'nom' : 					"Barnu",
		'email' : 					"essai@yahoo.fr"
	})
	oConfigTable.aLignes.ajouterDataSet({
		'date_debut' :				'2020-10-15',
		'date_fin' :				'',
		'tel' : 					"0600000005",
		'valeur': 					20,
		'type_promo':				'pourcent',
		'code_promo':				'',
		'description':				'',
		'quantite' : 				1,
		'est_actif':				false,
		'prenom' :					"Chloé",
		'nom' : 					"Fura"
	})
}
oConfig.aLignes.obtJeuDeDonnees = function() {
	return this;
}

var aDataSet;
var oDataSet;


const sHtmlBoutonSauvegarder = `<i class="btn save far fa-save"></i>`;
const sHtmlBoutonEffacer = `<i class="btn clear fas fa-eraser"></i>`

/** Remplit l'array de config d'une colonne du dataTable dans oConfigTable.oConfigDataTable.columns
 * à partir de la config des colonnes fournie dans oConfigTable.aColonnes (récupérée dans oConfigTable.col)
 * https://datatables.net/reference/option/columns.type
 */
ajouterConfigColonnesDataTable = function(oConfigTable = oConfig) {
	let cfgColonneTable;
	oConfigTable.col.forEach(function(colonne) {
		if (dt.configuration.columns[colonne.index_colonne] === undefined) {
			dt.configuration.columns[colonne.index_colonne] = {};
		};
		cfgColonneTable = dt.configuration.columns[colonne.index_colonne];
		//cfgColonneTable['data'] = null;
		cfgColonneTable['name'] = colonne.id;
		cfgColonneTable['title'] = colonne.libellé;
		cfgColonneTable['visible'] = colonne.visible_dans_tableau;
		//cfgColonneTable['type'] = 'string'; 		// date / num / num-fmt / ... / html / string
		cfgColonneTable['orderable'] = colonne.triable;
		cfgColonneTable['searchable'] = colonne.filtrable;
		cfgColonneTable['searchtype'] = obtModeDeFiltrageInEnglish(colonne.mode_de_filtrage);
		cfgColonneTable['defaultContent'] = '';

		// cfgColonneTable['searchtype'] = function ( data, type, row ) {
		//     return data +' ('+ row[3]+')';
		// },
		//oConfigTable.oDataTable.column( 'location:name' ).data()   			// donnée 'location'
	})
	function obtModeDeFiltrageInEnglish(obtModeDeFiltrageInFrench) {
		switch (obtModeDeFiltrageInFrench) {
			case 'sélection' : 			return 'select';
			case 'texte' : 				return 'text';
			default : 					return 'text';
		}
	}
}


/**
 * Renvoie un dataSet des données sous forme d'array de valeurs
 * @return {Array} Tableau contenant un jeu de toutes les données
 */
oConfig.fnCol.obtDataSetArray = function(oConfigTable = oConfig) {
	let aDataMultiple = [], aDataSimple, sIdCol, sType;
	oConfigTable.aLignes.forEach(function(ligne) {
		aDataSimple = [];
		oConfigTable.aColonnes.forEach(function(colonne) {
			//
			sIdCol = colonne.id;
			sType = colonne.type;
			if (sType === 'bouton') {
				aDataSimple.push(colonne.contenu);
			} else {
				aDataSimple.push(ligne[sIdCol]);
			}
		})
		aDataMultiple.push(aDataSimple)
	})
	return aDataMultiple;
}

/**
 * Renvoie un dataSet des données sous forme d'array d'objets
 * @return {Array} Objet contenant un jeu de toutes les données
 */
oConfig.fnCol.obtDataSetArrayObjets = function(oConfigTable = oConfig) {
	let aDataMultiple = [], oDataSimple, sIdCol, sType;
	oConfigTable.aLignes.forEach(function(ligne) {
		oDataSimple = {};
		oConfigTable.col.forEach(function(colonne) {
			sIdCol = colonne.id;
			sType = colonne.type;
			if (sType === 'bouton') {
				oDataSimple[sIdCol] = colonne.contenu;
			} else {
				oDataSimple[sIdCol] = ligne[sIdCol];
			}

		})
		aDataMultiple.push(oDataSimple)
	})
	return aDataMultiple;
}

/**
 * Renvoie le nombre de colonnes qui apparaissent dans la table
 * @return {Integer}
 */
oConfig.fnCol.obtNbColVisiblesDansTable = function(oConfigTable = oConfig) {
	let aObjColonnes = oConfigTable.aColonnes;
	let iNbColVisibles = 0;
	aObjColonnes.forEach(function(valeur, clé) {
		if (valeur.visible_dans_tableau === true) {
			iNbColVisibles++;
		}
	})
	return iNbColVisibles;
}

/**
 * Vérifie si un l'id fourni existe dans une des colonnes contenues dans aCol
 * @param {*} sIdColonne L'id de la colonne à vérifier
 * @param {*} bRespecterCasse Respecter la casse ?
 * @return {String} Retourne '' si non, ou l'id tel qu'il est écrit dans la colonne si trouvé
 */
oConfig.fnCol.estIdColonneExistant = function(sIdColonne = 'code', bRespecterCasse = false, sValeurSiNonTrouvé = '', oConfigTable = oConfig) {
	let sRetour = sValeurSiNonTrouvé;
	oConfigTable.aColonnes.forEach(function(colonne) {
		if (bRespecterCasse)  {
			if (colonne.id === sIdColonne)  { sRetour = colonne.id; }
		} else {
			if (colonne.id.toUpperCase() === sIdColonne.toUpperCase())  { sRetour = colonne.id; }
		}
	})
	return sRetour;
}

/**
 * Vérifie si l'index de la colonne fournie existe dans aCol
 * @param {Integer} iIndexColonne L'index de la colonne à vérifier
 * @param {Object} oConfigTable Config de la table
 * @return {Boolean} Retourne si l'index existe ou non
 */
oConfig.fnCol.estIndexColonneExistant = function(iIndexColonne = 0, oConfigTable = oConfig) {
	return (oConfigTable.col[iIndexColonne] !== undefined);
}

/**
 * Renvoie un array d'objets contenant la config de chaque colonne de la table dans l'ordre d'affichage
 * permet de générer l'array oConfigTable.col
 * @param {Array} aObjetsColonnes Tableau d'objets contenant la config de chaque colonne
 * @param {Array} aCléColonnesDansObjDansOrdreAffichageTable Tableau contenant l'id pour aObjetsColonnes de chaque colonne dans l'ordre d'affichage de la table
 * @param {String} sIdTable L'id de la table
 * @return {Array} Tableau contenant la config dans l'ordre d'affichage
 */
function cfgCol(oConfigTable = oConfig) {
	let aRetour = [];
	let aObjetsColonnes = oConfigTable.aColonnes;
	let aCléColonnesDansObjDansOrdreAffichageTable = aIndexColonnesSelonPositionDansTableau;
	let iIndexColonneDansObj, iIndexColDansTable;
	let iNbColonnesDansTable = aCléColonnesDansObjDansOrdreAffichageTable.length;
	let bVisibleDansTableau;
	for (iIndexColDansTable = 0; iIndexColDansTable < iNbColonnesDansTable; iIndexColDansTable++) {
		iIndexColonneDansObj = aCléColonnesDansObjDansOrdreAffichageTable[iIndexColDansTable];
		bVisibleDansTableau = (aObjetsColonnes[iIndexColonneDansObj].visible_dans_tableau);
		if (bVisibleDansTableau) {
			aRetour.push(aObjetsColonnes[iIndexColonneDansObj]);
		}
	}
	return aRetour;
}


/**
 * Renvoie le nombre de lignes qui sont dans l'array
 * @return {Integer}
 */
oConfig.fnLig.obtNbLigVisiblesDansTable = function(oConfigTable = oConfig) {
	return oConfigTable.aLignes.length;
}

function construireTable(oConfigTable = oConfig)	{
	var i;
	var sHTML= "";
	var iNbColonnes		= oConfigTable.fnCol.obtNbColVisiblesDansTable();
	var iNbLignes		= oConfigTable.aLignes.count();
	sHTML+= "<thead>";
	sHTML+= "<tr>";
	// ENTETE de chaque colonne
	for (i=0; i<iNbColonnes; i++) {
		//iIndexColonne = aIndexColonnesSelonPositionDansTableau[i];
		// sHTML+= "<th id='" + col[i]['id'] + "'>";		// ajoute l'id colonne
		// sHTML+= col[i]['libellé'] + "</th>";			// ajoute le libellé
		sHTML+= `<th id='${oConfigTable.col[i].id}' name='${oConfigTable.col[i].id}'>`;
		sHTML+= "</th>";
	}
	sHTML+= "</tr>";
	sHTML+= "</thead>";

	// sHTML+= "<tbody>";
	// for (let idLigne = 0; idLigne < iNbLignes; idLigne++) {
	// 	sHTML+= `<tr id='${idLigne}'>`;
	// 		for (iC=0; iC<iNbColonnes; iC++) {
	// 			// iIndexColonne		= aIndexColonnesSelonPositionDansTableau[iC];
	// 			sHTML	+= "<td>" + '' + "</td>";
	// 		}
	// 	sHTML+= "</tr>";
	// }
	// sHTML+= "</tbody>";

	oConfigTable.oTable = $(`#${oConfigTable.sId}`);
	oConfigTable.oTable.html(sHTML);

	// insère les données dans chaque cellule
	//insérerContenuDansCellules();

	//oConfig.oTable.DataTable().filtersClear();
	oConfigTable.oTable.DataTable().destroy();
	oConfigTable.oDataTable = oConfigTable.oTable.DataTable(dt.configuration);
}

/**
 * Insére un jeu de données dans le dataTable
 * @param {Array|Object} dataSet
 * @param {Object} oConfigTable
 */
function insererDonneesDansTable(dataSet, bInsérerDansDataTable = 'true...todo', oConfigTable = oConfig) {
	//oConfigTable.oTable.append()
	let lig, aDataSet;
	if (typeOf(dataSet) === 'array' && dataSet.containsOnlyType('object')) {
		// tableau d'objet
		dataSet.forEach(function(oDonneeDataSet) {
			// appel récursif sur chaque objet
			insererDonneesDansTable(oDonneeDataSet, bInsérerDansDataTable, oConfigTable);
		})
	} else if (typeOf(dataSet) === 'object') {
		// objet
		// on insère les données
		aDataSet = objDonneeDataSetVersArrDonneeDataSet(dataSet);
		lig = oConfigTable.oDataTable.row.add(aDataSet).node();
		oConfigTable.oDataTable.draw();
		lig.id = '' + oConfigTable.var.iNbLignesInserees;
		// Incrémente l'id ligne
		oConfigTable.var.iNbLignesInserees++;	// todo id_ligne en fonction de config (id_colonne)
	}
};

/**
 * Transforme un objet contenant une ligne de données en array
 * pour pouvoir l'insérer dans la table ensuite
 * @param {Object} oDonneeDataSet Données à insérer (ce n'est pas un problème
 * si cet objet comprend davantage de données que le nombre de colonnes à afficher)
 * @return {Array} Données sous forme de tableau dans l'ordre d'affichage des colonnes
 */
function objDonneeDataSetVersArrDonneeDataSet(oDonneeDataSet) {
	let aRetour = [];
	if (typeOf(oDonneeDataSet) !== 'object') { throw new Error(`objDonneeDataSetVersArrDonneeDataSet() --> oDonneeDataSet doit être un objet`); }
	oConfig.col.forEach(function(colonneVisible) {
		aRetour.push(oDonneeDataSet[colonneVisible.id]);
	})
	return aRetour;
}

// "aoColumnDefs": [
// 	{
// 		 "aTargets": [5],
// 		 "mData": null,
// 		 "mRender": function (data, type, full) {
// 			 return '<a href="#" onclick="alert(\''+ full[0] +'\');">Process</a>';
// 		 }
// 	 }
//   ]

// parcourt chaque cellule de #table_promos pour y ajouter un contenu
function insérerContenuDansCellules(sAffichageOuStockage = 'affichage', sIdTable = 'table_promos') {			// ICI!!!
	let iIndexColonne, iIndexLigne, sIdPromo;
	let oCell, sCodeHtmlBoutons;
	$(`#table_promos tbody tr`).each(function(){ // parcourt chaque ligne
		sIdPromo = $(this).attr('id');
		iIndexLigne = $(this).index();
		$(this).children('td').each(function(){ // parcourt chaque cellule de la ligne
			iIndexColonne = $(this).index();
			oCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
			// if (oCell.sType === 'boolean') {		// booléen: on insère l'objet checkbox.switch
			// 	insérerObjetDansCellule(iIndexLigne, iIndexColonne, sIdTable);
			// } else
			if (oCell.sType === 'bouton') {
				sCodeHtmlBoutons = obtPropColDepuisId(oCell.sIdColonne, 'contenu');
				$(this).html(sCodeHtmlBoutons);
			} else if (oCell.sType !== 'bouton') {
				insérerValeurDansCelluleDepuisIndex(iIndexColonne, iIndexLigne);
			}
			// $(this).html(oCell.sType)			// affiche le type de donnée de chaque cellule
			// $(this).html(iIndexLigne + ' ' + iIndexColonne)			// affiche ligne et colonne de chaque cellule
		})
	});
}

function insérerValeurDansCelluleDepuisIndex(iIndexColonne = 0, iIndexLigne = 0, sAffichageOuStockage = 'affichage', sIdTable = 'table_promos') {
	let sValeurAAfficher = obtValeurCelluleDepuisIndex(iIndexLigne, iIndexColonne, 'affichage', sIdTable);
	$(`#${sIdTable} tbody tr:eq(${iIndexLigne}) td:eq(${iIndexColonne})`).text(sValeurAAfficher);	// on affiche le texte dans le td
}

function insérer_valeur_et_trier_tableau_croissant(arr_tableau_valeurs, nb_valeur) {
	var bool_flag_a_permuté;
	var int_longueur_tableau               = arr_tableau_valeurs.length;
	if (isNaN(nb_valeur))  { return; }
	arr_tableau_valeurs.push(nb_valeur);
	do {
		bool_flag_a_permuté = false;
		for (let i = 0; i < int_longueur_tableau; i++) {
			if (arr_tableau_valeurs[i] > arr_tableau_valeurs[i + 1]) {
				bool_flag_a_permuté         = true;
				nb_val_temp                 = arr_tableau_valeurs[i];
				arr_tableau_valeurs[i]      = arr_tableau_valeurs[i + 1];
				arr_tableau_valeurs[i + 1]  = nb_val_temp;
			}
		}
	} while (bool_flag_a_permuté)
}

// Retourne les index où les valeurs dans un tableau des valeurs commençant par
function obtDepuisPremiersCaractères(tableau, sPremiersCaractèresVoulus, sElémentVoulu = 'index', sFormatSiValeur = 'initial') {
	var aTableauARetourner 				= [];
	const iLongueurPremiersCaractères 	= sPremiersCaractèresVoulus.length;
	var sPremiersCaractèresDansTableau;
	for (let i = 0; i < tableau.length; i++) {
		sPremiersCaractèresDansTableau 	= tableau[i].substr(0, iLongueurPremiersCaractères);
		if (sPremiersCaractèresDansTableau === sPremiersCaractèresVoulus) {
			switch (sElémentVoulu) {
				case 'index':
					aTableauARetourner.push(i);
					break;
				case 'valeur':
					sValeur = obtStrFormaté(tableau[i], sFormatSiValeur); //obtDonnéeFormatée()
					aTableauARetourner.push(sValeur);
					break;
			}
		}
	}
	if (aTableauARetourner.length > 1) {
		return aTableauARetourner;
	} else {
		return aTableauARetourner[0];
	}
}

// Renvoie un affichage propre d'une donnée au format souhaité
function obtDonnéeFormatée(donnéeBrute, sFormatVoulu, valeurSiIndéfini = undefined, bAfficherEnConsole = false) {
	// str entre guillemets
	// (?<guillemets>["'`])(.*?)(?P=guillemets)
	var regexDélimiteurs = new RegExp(re.sDélimiteurs, 'g');
	let oResultats, aParamètres;
	var sDonnéeFormatée;
	var sTypeDeDonnéeBrute = typeof(donnéeBrute);
	if (donnéeBrute === undefined)  { return valeurSiIndéfini; }
	if (typeof(sFormatVoulu) === 'object') { 			// si sFormatVoulu est un tableau, on applique les différents formats voulus les uns après les autres
		sFormatVoulu.forEach(function(sFormatAAppliquer) {
			sFormatAAppliquer = sFormatAAppliquer.trim().toLowerCase();
			donnéeBrute = obtDonnéeFormatée(donnéeBrute, sFormatAAppliquer, valeurSiIndéfini)
		})
		return donnéeBrute;
	} else if (typeof(sFormatVoulu) === 'string') { 	// si c'est un texte, on applique le format voulu
		sFormatVoulu 	= sFormatVoulu.trim();
		if (donnéeBrute === '') { return ''; }
		// Formats spécifiés entre accolades
		if ((oResultats = regexDélimiteurs.exec(sFormatVoulu)) != null) {
			regexDélimiteurs.lastIndex = 0;
			let iPosDbtSousChaine, iPosDbtFormat, iPosFinFormat, iPosFinSousChaine, sFormatEntreAccolades, sSousDonnéeFormatée;
			// let iTailleSousChaine, iTailleFormat, iTailleDonnéeInsérée,
			sDonnéeFormatée = sFormatVoulu;
			while ((oResultats = regexDélimiteurs.exec(sFormatVoulu)) != null) {
				iPosDbtSousChaine = oResultats.index;
				iPosDbtFormat = iPosDbtSousChaine + 2;
				iPosFinFormat = iPosDbtFormat + oResultats[1].length;
				iPosFinSousChaine = iPosFinFormat + 1;
				sFormatEntreAccolades = oResultats[1];
				sSousDonnéeFormatée = obtDonnéeFormatée(donnéeBrute, sFormatEntreAccolades, valeurSiIndéfini);
				// iTailleSousChaine = iPosFinSousChaine - iPosDbtSousChaine;
				// iTailleFormat = iPosFinFormat - iPosDbtFormat;
				// iTailleDonnéeInsérée = sSousDonnéeFormatée.length;
				//sDonnéeFormatée = insérerSousChaine(sFormatVoulu, sSousDonnéeFormatée, iPosDbtSousChaine, iPosFinSousChaine);
				sDonnéeFormatée = sDonnéeFormatée.replace(/#{(.*?)}/, sSousDonnéeFormatée);
			}
			return sDonnéeFormatée;
		}
	}
	// DONNÉE DE TYPE INDÉFINI
	if (sTypeDeDonnéeBrute === 'undefined') {
		return ''; 	// 'undefined';
		// DONNÉE NOMBRE
	} else if (sTypeDeDonnéeBrute === 'number') {
		// nombre entier					new Intl.NumberFormat().format(donnéeBrute) ??
		re.reDétectionFormatDemandé = new RegExp(re.sEntier + re.sAccolades, 'i');
		if ((re.reDétectionFormatDemandé.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = parseInt(donnéeBrute);
			return renvoyerRésultat('nombre entier');
		}
		// nombre arrondi
		re.reDétectionFormatDemandé = new RegExp(re.sArrondi + re.sAccolades, 'i');
		if ((re.reDétectionFormatDemandé.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = Math.round(donnéeBrute);
			return renvoyerRésultat('nombre arrondi');
		}
		// €, euros, monnaie
		// monnay(mille: ".", décimal: ", ")
		if ((/^(?:monnaie|\$|eur(?:o?s)?)$/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = parseInt(donnéeBrute) + ' €';
			return renvoyerRésultat('nombre monétaire €');	// todo
		}
		if ((/^(?:monnay|\$|dol(?:lars?)?)$/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = parseInt(donnéeBrute) + ' $';
			return renvoyerRésultat('nombre monétaire $');	// todo
		}
		// DONNÉE OBJET
		// Objet Date --> on convertit en str et on rappelle la fonction
	} else if (sTypeDeDonnéeBrute === 'object') {
		let sDate = donnéeBrute.AAAAMMJJ();
		sDonnéeFormatée = obtDonnéeFormatée(sDate, sFormatVoulu, valeurSiIndéfini);
		// TEXTE
	} else if (sTypeDeDonnéeBrute === 'string') {
		// DATE		// peut être fournie en 'string' (format anglais 'aaaa/mm/jj') ou objet 'Date'
		// 'aaaa':	// 2021
		// 'aa':	// 21
		// 'mmmm':	// septembre
		// 'mmm':	// sept.
		// 'mm':	// 09
		// 'm':		// 9
		// 'jjjj':	// samedi
		// 'jjj':	// sam.
		// 'jj':	// 04
		// 'j':		// 4

		if ((/^(19|20)\d{2}-(0?[1-9]|1[0-2])-(0?[1-9]|1\d|2\d|3[01])$/.exec(donnéeBrute)) !== null) {
			let dDate = new Date(donnéeBrute);
			let sFormatDeMajuscules;
			aDate = donnéeBrute.split('-');
			// aaaa
			if ((/^\baaaa\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.AAAA();
				return renvoyerRésultat('str date AAAA');
				// aa
			} else if ((/^\baa\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.AA();
				return renvoyerRésultat('str date AA');
				// mmmm
			} else if ((/^\bmmmm\b$/i.exec(sFormatVoulu)) !== null) {
				sFormatDeMajuscules = vérifierMajusculesSurFormatDate(sFormatVoulu);
				switch (sFormatDeMajuscules) {
					case 'MAJ' : sDonnéeFormatée = dDate.MMMM('fr', 'toUpperCase'); break;
					case 'PremièreLettreMAJ' : sDonnéeFormatée = dDate.MMMM('fr', 'toUpperCaseFirstLetter');
				}
				return renvoyerRésultat('str date MMMM');
				// mmm
			} else if ((/^\bmmm\b$/i.exec(sFormatVoulu)) !== null) {
				sFormatDeMajuscules = vérifierMajusculesSurFormatDate(sFormatVoulu);
				switch (sFormatDeMajuscules) {
					case 'MAJ' : sDonnéeFormatée = dDate.MMM('fr', 'toUpperCase'); break;
					case 'PremièreLettreMAJ' : sDonnéeFormatée = dDate.MMM('fr', 'toUpperCaseFirstLetter');
				}
				return renvoyerRésultat('str date MMM');
				// mm
			} else if ((/^\bmm\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.MM();
				return renvoyerRésultat('str date MM');
				// m
			} else if ((/^\bm\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.M();
				return renvoyerRésultat('str date M');
				// jjjj
			} else if ((/^jjjj(?:\((.*?)\))?$/i.exec(sFormatVoulu)) !== null) {
				sFormatDeMajuscules = vérifierMajusculesSurFormatDate(sFormatVoulu);
				switch (sFormatDeMajuscules) {
					case 'MAJ' : sDonnéeFormatée = dDate.JJJJ('fr', 'toUpperCase'); break;
					case 'PremièreLettreMAJ' : sDonnéeFormatée = dDate.JJJJ('fr', 'toUpperCaseFirstLetter');
				}
				return renvoyerRésultat('str date JJJJ');
				// jjj
			} else if ((/^\bjjj\b$/i.exec(sFormatVoulu)) !== null) {
				sFormatDeMajuscules = vérifierMajusculesSurFormatDate(sFormatVoulu);
				switch (sFormatDeMajuscules) {
					case 'MAJ' : sDonnéeFormatée = dDate.JJJ('fr', 'toUpperCase'); break;
					case 'PremièreLettreMAJ' : sDonnéeFormatée = dDate.JJJ('fr', 'toUpperCaseFirstLetter');
				}
				return renvoyerRésultat('str date JJJ');
				// jj
			} else if ((/^\bjj\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.JJ();
				return renvoyerRésultat('str date JJ');
				// j
			} else if ((/^\bj\b$/i.exec(sFormatVoulu)) !== null) {
				sDonnéeFormatée = dDate.J();
				return renvoyerRésultat('str date J');
			}
		}
		// camelCase
		if ((/\b(Camel( ?Case)?|Chameau)\b/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.toCamelCase();
			return renvoyerRésultat('str camelCase');
			// PascalCase
		} else if ((/\b(Pascal( ?Case)?|Upper ?Camel ?Case)\b/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.toPascalCase();
			return renvoyerRésultat('str PascalCase');
			// minuscules
		} else if ((/\b(min(uscules?)?|low(er)?( ?case)?)\b/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.toLowerCase();
			return renvoyerRésultat('str lowerCase');
			// MAJUSCULES
		} else if ((/\b(maj(uscules?)?|up(per)?( ?case)?)\b/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.toUpperCase();
			return renvoyerRésultat('str UpperCase');
			// PremièreLettreMAJ
		} else if ((/capital(e|ize)?|(1(r?st|[èe]re?)?|first|premi[èe]re?)( ?lett(re|er))?/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.capitalize();
			return renvoyerRésultat('str UpperCaseFirstLetter');
			// e-mail..... todo
		} else if ((/(adresse ?)?((e-?)?mail|courriel|[ée]lectronique)/i.exec(sFormatVoulu)) !== null) {
			sDonnéeFormatée = donnéeBrute.toLowerCase();
			return renvoyerRésultat('str e-mail');
		}
		// DONNÉE D'UN TYPE AUTRE --> INCONNU
	} else {					// autre..
		sDonnéeFormatée = donnéeBrute;
		return renvoyerRésultat('sTypeDeDonnéeBrute non reconnu !!!');
	}
	if (sDonnéeFormatée === undefined)  {
		sDonnéeFormatée = valeurSiIndéfini;
		return renvoyerRésultat('sDonnéeFormatée === undefined');
	}
	return renvoyerRésultat('non détecté : renvoyé en dernière ligne');
	function renvoyerRésultat(sFormatDétecté, bDébugguer = false) {
		let o = {};
		if (bAfficherEnConsole === true || bDébugguer === true) {
			o.sDonnéeEnEntrée = donnéeBrute;
			o.sTypeDeDonnéeBruteEnEntrée = sTypeDeDonnéeBrute;
			o.sFormatVoulu = sFormatVoulu;
			o.sFormatVouluDétecté = sFormatDétecté;
			o.sDonnéeEnSortie = sDonnéeFormatée;
			console.log(`obtDonnéeFormatée()....`, o)
		}
		return sDonnéeFormatée;
	}
}
function obtParamsPassésEnArg(argumt) {
	if (typeof(argumt) === 'string') {
		re.reDétectionFormatDemandé = new RegExp(re.sAccolades, '');
		if ((re.reDétectionFormatDemandé.exec(sFormatVoulu)) !== null) {
			return JSON.parse(re.reDétectionFormatDemandé[1]);
		}
	}
}
// /(?:^|,|\s)\b([^"'`]*?)\b(?:$|:|\s)/
// symbole  :"€", mille:".",décimal:", "			renvoie tous les noms de paramètres qui ne sont pas entre guillemets

//alert(`essai ${jjj(3)} !`)
var f = [];
f._pour_nombre = [
	{'entier': "nombre en entier, tronqué si décimal fourni (ex: '37')"},
	{'monnaie': ""}];
f._pour_date = [
	{'aaaa': "année sur 4 chiffres (ex: '2018'))"},
	{'aa': "année sur 2 chiffres (ex: '18')"},
	{'mmmm': "mois en lettres (ex: 'février'). Variante : 'Mmmm' affiche la première lettre en MAJ / 'MMMM' affiche l'ensemble en MAJ"},
	{'mmm': "mois abbregé en lettres (ex: 'fév.'). Variante : 'Mmm' affiche la première lettre en MAJ / 'MMM' affiche l'ensemble en MAJ"},
	{'mm': "mois sur 2 chiffres (ex: '02')"},
	{'m': "mois sur 1 chiffre (ex: '2'), ou 2 chiffres si >= 10"},
	{'jjjj': "jour de la semaine en lettres (ex: 'lundi'). Variante : 'Jjjj' affiche la première lettre en MAJ / 'JJJJ' affiche l'ensemble en MAJ"},
	{'jjj': "jour de la semaine abbregé en lettres (ex: 'lun.'). Variante : 'Jjj' affiche la première lettre en MAJ / 'JJJ' affiche l'ensemble en MAJ"},
	{'jj': "jour du mois sur 2 chiffres (ex: '08')"},
	{'j': "jour du mois sur 1 chiffre (ex: '8'), ou 2 chiffres si >= 10"}];
_aide = function () {
	console.log('formatages possibles......', f);
}
// todo:
function _monnaie(sép1000 = '.', sép0 = ',', symbole = '€') {
	return 'monnaie' + JSON.stringify({symbole, sép1000, sép0});
}
function _entier(nNombre, sép1000 = '.') {
	let o = obtParamsPassésEnArg(arguments[1]);
	return 'entier' + JSON.stringify({sép1000});
}
function _entier(donnéeBrute = 'auto', sép1000 = '.') {
	let o = obtParamsPassésEnArg(arguments[0]);
	if (typeof(o) !== object) {
		o.sép1000 = sép1000;
	}
	return parseInt(donnéeBrute);
}

function vérifierMajusculesSurFormatDate(sFormatDeDate) {
	let sLettre1, sLettre2;
	sLettre1 = sFormatDeDate.substr(0, 1);
	sLettre2 = sFormatDeDate.substr(1, 1);
	if (sLettre2 === sLettre2.toUpperCase()) {
		return 'MAJ';
	} else if (sLettre1 === sLettre1.toUpperCase()) {
		return 'PremièreLettreMAJ';
	} else {
		return 'min';
	}
}

// retourne l'index de l'objet dans 'aCol' depuis la position de la colonne dans le tableau affiché
function IndexObjDepuisIndexColonneAffichee(iIndexColonneAffichee, oConfigTable = oConfig) {
	for (i = 0; i < oConfigTable.aColonnes.length; i++) {
		if (oConfigTable.aColonnes[i]['index_colonne'] == iIndexColonneAffichee) {
			return i;
		}
	}
}

/**
 * Retourne la cellule ciblée d'une table
 * @param {Integer} iIndexLigne
 * @param {Integer} iIndexColonne
 * @param {String} sIdTable
 * @return {ref:Object} Référence vers la cellule
 */
function cellule(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	return cell = $(`#${sIdTable} tbody tr:eq(${iIndexLigne}) td:eq(${iIndexColonne})`);
}

function ligne(iIndexLigne = 0, sIdTable = 'table_promos') {
	return lig = $(`#${sIdTable} tbody tr:eq(${iIndexLigne})`);
}

function ligneId(iIdLigne = 0, sIdTable = 'table_promos') {
	return lig = $(`#${sIdTable} tbody tr:#${iIndexLigne}`);
}

/**
 * Renvoie si une cellule existe ou non
 * @param {Integer} iIndexLigne Numéro de la ligne
 * @param {Integer} iIndexColonne Numéro de la colonne
 * @param {String} sIdTable Id de la table
 * @return {Boolean}
 */
function estCelluleExistante(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos'){
	let iNbLignes = $(`#${sIdTable} tbody tr`).length;
	let iNbColonnes = $(`#${sIdTable} thead tr:eq(0) th`).length;
	let cell = $(`#${sIdTable} tbody tr:eq(${iIndexLigne}) td:eq(${iIndexColonne})`);
	if (iIndexLigne < 0 || iIndexLigne >= iNbLignes)  { return false; }
	if (iIndexColonne < 0 || iIndexColonne >= iNbColonnes)  { return false; }
	return true;
}

/**
 * Renvoie si une cellule est éditable ou non
 * @param {Integer} iIndexLigne le numéro de la ligne
 * @param {Integer} iIndexColonne le numéro de la colonne
 * @param {String} sIdTable l'id de la table
 * @return {Boolean}
 */
function estCelluleEditable(iIndexLigne = 0, iIndexColonne = 0, arg3 = 'à venir...', oConfigTable = oConfig){
	let bCelluleExiste = estCelluleExistante(iIndexLigne, iIndexColonne, oConfigTable.sId);
	if (!bCelluleExiste)  { return false; }
	return oConfigTable.col[iIndexColonne].éditable;
}

/**
 * Renvoie une propriété d'une ligne, à partir de son index
 * @param {Integer} iIndexLig Index de la ligne dans aLig
 * @param {String} [sNomProp='prenom'] Nom de la propriété dont on veut récupérer la valeur
 * @param {?String} [sIdTable='table_promos'] Id de la table
 * @return {*}
 */
function obtPropLigDepuisIndex(iIndexLig, sNomProp = 'prenom', arg3 = 'à venir...', oConfigTable = oConfig) {
	let sIdLig = $(`#${oConfigTable.sId} tbody tr:eq(${iIndexLig})`).prop('id');
	return oConfigTable.aLignes[+sIdLig][sNomProp];
}

/**
 * Renvoie une propriété d'une colonne, à partir de son index
 * @param {Integer} iIndexCol Index de la colonne dans la table
 * @param {String} [sNomProp='id'] Nom de la propriété dont on veut récupérer la valeur
 * @return {*}
 */
function obtPropColDepuisIndex(iIndexCol, sNomProp = 'id', oConfigTable = oConfig) {
	return oConfigTable.col[iIndexCol][sNomProp]; //sRetour;
}

/**
 * Renvoie une propriété d'une colonne, à partir de son id
 * @param {Integer} idCol Id de la colonne dans l'array'
 * @param {String} [sNomProp='libellé'] Nom de la propriété dont on veut récupérer la valeur
 * @return {*}
 */
function obtPropColDepuisId(idCol = 'prenom', sNomProp = 'libellé', oConfigTable = oConfig) {
	let sRetour;
	oConfigTable.aColonnes.forEach(function(valeur, clé){
		if (oConfigTable.aColonnes[clé].id === idCol) {
			sRetour = oConfigTable.aColonnes[clé][sNomProp];
		}
	})
	return sRetour;
}

function obtInfosCelluleDepuisIndex(iIndexLigne = 0, iIndexColonne = 0, arg3 = 'à venir...', oConfigTable = oConfig) {
	let o = {};
	o.bEstCelluleExistante = estCelluleExistante(iIndexLigne, iIndexColonne, oConfigTable.sId);
	if (!o.bEstCelluleExistante)  { return o; }
	o.bEstCelluleEditable = estCelluleEditable(iIndexLigne, iIndexColonne, oConfigTable.sId);
	o.sType = oConfigTable.col[iIndexColonne].type;
	o.sIdColonne = oConfigTable.col[iIndexColonne].id;
	o.sIdLigne = $(`#${oConfigTable.sId} tbody tr:eq(${iIndexLigne})`).prop('id');
	return o;
}

function obtInfosCelluleDepuisId(idLigne = 0, idColonne = 'code', arg3 = 'à venir...', oConfigTable = oConfig) {
	let o = {};
	o.iIndexLigne = $(`#${oConfigTable.sId} tbody tr[id=${idLigne}]`).index();
	o.iIndexColonne = $(`#${oConfigTable.sId} thead:eq(0) tr:eq(0) th[id=${idColonne}]`).index();
	o.bEstCelluleExistante = estCelluleExistante(o.iIndexLigne, o.iIndexColonne, oConfigTable.sId);
	o.sFormatStockage = obtPropColDepuisId(idColonne, 'format_stockage');
	o.sFormatAffichage = obtPropColDepuisId(idColonne, 'format_affichage');
	//o.sValeurStockée = obtValeurCelluleDepuisIndex(o.iIndexLigne, o.iIndexColonne, 'stockage');
	//o.sValeurStockéeFormatée = obtDonnéeFormatée(o.sValeurStockée, o.sFormatStockage);
	if (!o.bEstCelluleExistante)  { return o; }
	o.bEstCelluleEditable = estCelluleEditable(o.iIndexLigne, o.iIndexColonne, oConfigTable.sId);
	return o;
}

function obtValeurCelluleDepuisIndex(iIndexLigne = 0, iIndexColonne = 0, pourAffichageOuStockage = 'affichage', arg4 = 'à venir...', oConfigTable = oConfig) {
	let oInfosCell = obtInfosCelluleDepuisIndex(+ iIndexLigne, iIndexColonne, oConfigTable.sId);
	//clog('oInfosCell', oInfosCell);
	return obtValeurCelluleDepuisId(oInfosCell.sIdLigne, oInfosCell.sIdColonne, pourAffichageOuStockage);
}

function obtValeurCelluleDepuisId(idLigne = 0, idColonne = 'code', pourAffichageOuStockage = 'affichage', oConfigTable = oConfig) {
	let sValeurStockée, sRetour;
	oInfosCell = obtInfosCelluleDepuisId('' + idLigne, idColonne, oConfigTable.sId);
	if (!oInfosCell.bEstCelluleEditable)  { alert('non_edit'); return; }
	oInfosCell.valeurStockée = oConfigTable.aLignes[idLigne][idColonne];
	oInfosCell.sType = obtPropColDepuisId(idColonne, 'type');
	oInfosCell.valeur = oConfigTable.aLignes[idLigne][idColonne];
	oInfosCell.sFormatStockage = obtPropColDepuisId(idColonne, 'format_stockage');
	oInfosCell.sFormatAffichage = obtPropColDepuisId(idColonne, 'format_affichage');
	//clog('oInfosCell', oInfosCell);
	if (pourAffichageOuStockage === 'stockage') {
		if (oInfosCell.sType === 'enum') {
			sRetour = oInfosCell.valeurStockée;
		} else {
			sRetour = obtDonnéeFormatée(oInfosCell.valeurStockée, oInfosCell.sFormatStockage, '');
		}
	} else if (pourAffichageOuStockage === 'affichage') {
		if (oInfosCell.sType === 'enum') {
			oInfosCell.sValeurAffichée = obtValeurAffichageEnum(oInfosCell.valeurStockée, idColonne, true, false);
		} else {
			oInfosCell.sValeurAffichée = oInfosCell.valeurStockée;
		}
		oInfosCell.sValeurAffichée = obtDonnéeFormatée(oInfosCell.sValeurAffichée, oInfosCell.sFormatAffichage);
		sRetour = oInfosCell.sValeurAffichée;
	}
	return sRetour;
}

/**
 * Renvoie la valeur affichée d'un enum, à partir de sa valeur stockée
 * Pour cela, la propriété 'valeurs_possibles' de la colonne doit contenir
 * un objet contenant pour chaque clé (valeur stockée) une valeur à afficher
 * ex : 	{ 	'M' : 	'Mâle',
 * 				'F' : 	'Femelle'	}
 * @param {String} sValeurStockée Valeur 'raccourcie' (celle qui est stockée dans la BDD)
 * @param {String} sIdColonne Id de la colonne concernée
 * @param {Boolean} testKeyWithUpperAndLowerCase Si true, la valeur raccourcie est testée en minuscules/majuscules
 */
function obtValeurAffichageEnum(sValeurStockée = 'H', sIdColonne = 'type', testKeyWithUpperAndLowerCase = true, bFormaterPourAffichage = true) {
	let oValeursPossibles = obtPropColDepuisId(sIdColonne, 'valeurs_possibles');
	oInfosCell.valeursPossibles = obtPropColDepuisId(sIdColonne, 'valeurs_possibles');
	if (Array.isArray(oInfosCell.valeursPossibles)) {
		oInfosCell.sTypeValeursPossibles = 'array';
		oInfosCell.valeursPossibles.forEach(function(valeur) {
			if (valeur.toUpperCase() === sValeurStockée.toUpperCase()) {
				oInfosCell.sValeurAffichée = sValeurStockée;
			}
		})
	} else if (typeof(oInfosCell.valeursPossibles) === 'object') {
		oInfosCell.sTypeValeursPossibles = 'object';
		oInfosCell.sValeurAffichée = oInfosCell.valeursPossibles[sValeurStockée];
		if (testKeyWithUpperAndLowerCase) {
			if (oInfosCell.sValeurAffichée === undefined)  { oInfosCell.sValeurAffichée = oValeursPossibles[sValeurStockée.toLowerCase()]; }
			if (oInfosCell.sValeurAffichée === undefined)  { oInfosCell.sValeurAffichée = oValeursPossibles[sValeurStockée.toUpperCase()]; }
		}
	}
	if (bFormaterPourAffichage) {
		oInfosCell.sValeurAffichée = obtDonnéeFormatée(oInfosCell.sValeurAffichée, oInfosCell.sFormatAffichage);
	}
	return oInfosCell.sValeurAffichée;
}

/**
 * Passe en mode édition la ligne demandée
 * et sélectionne le champ correspondant à 'iIndexColonne'
 * @param {Integer} iIndexLigne le numéro de la ligne
 * @param {Integer} iIndexColonne le numéro de la colonne
 * @param {String} sIdTable l'id de la table
 * @return {Boolean} false si cellule non éditable
 */
function éditerLigne(iIndexLigneAEditer = 0, iIndexColonneFocus = 0, sIdTable = 'table_promos'){
	let lig, cell, iIndexLigne, iIndexColonne, sValeurPourAffichage, objDansCell;
	let bObjetsCellsLigneChargés = false, bPasDeValeurInvalideSaisie;
	lig = $(`#${sIdTable} tbody tr:eq(${iIndexLigneAEditer})`);
	lig.children('td').each(function() { // pour chaque cellule de la ligne
		cell = $(this);
		iIndexColonne = cell.index();
		éditerCelluleNew(iIndexLigneAEditer, iIndexColonne, sIdTable);
		détecterChangementValeurSaisieDansCellule(iIndexLigneAEditer, iIndexColonne, sIdTable);
	})
	bObjetsCellsLigneChargés = true;
	sélectionnerObjet(iIndexLigneAEditer, iIndexColonneFocus, sIdTable);
	objDansCell = lig.children('td').children();
	objDansCell.on('keydown', function(e) {
		cell = $(this).parent('td');
		iIndexColonne = cell.index();
		if (e.which === 13) { 													// touche 'entrée'
			if (!e.altKey) {
				bPasDeValeurInvalideSaisie = quitterSaisieCellule(iIndexLigneAEditer, iIndexColonne, sIdTable, true, false);
				if (bPasDeValeurInvalideSaisie) {
					sélectionnerCellule(iIndexLigneAEditer, iIndexColonne + 1, sIdTable);
				}
			}
		} else if (e.keyCode === 27) { 											// touche 'échap'
			//quitterSaisieLigne(iIndexLigneAEditer, iIndexColonne, sIdTable);
			quitterSaisieLignes(sIdTable, true, true);

		} else if (e.keyCode === 9) { 											// touche 'tab'
			e.stopPropagation();
			quitterSaisieCellule(iIndexLigneAEditer, iIndexColonne, sIdTable);
			éditerCelluleNew(iIndexLigneAEditer, iIndexColonne + 1, sIdTable);
		} else if (e.keyCode >= 37 && e.keyCode <= 40) {						// touche flèche gauche : 'e.key === ArrowLeft'
			e.stopPropagation();
			if (e.shiftKey && e.key === 'ArrowLeft') {							// touche directionnelle gauche + maj enfoncé (37)
				bPasDeValeurInvalideSaisie = quitterSaisieCellule(iIndexLigneAEditer, iIndexColonne, sIdTable, true, false);
				if (bPasDeValeurInvalideSaisie) {
					sélectionnerCellule(iIndexLigneAEditer, iIndexColonne - 1, sIdTable);
				}
			} else if (e.shiftKey && e.key === 'ArrowUp') {						// touche directionnelle haut + maj enfoncé (38)
				bPasDeValeurInvalideSaisie = quitterSaisieLigne(iIndexLigneAEditer, iIndexColonne, sIdTable, true, true);
				if (bPasDeValeurInvalideSaisie) {
					éditerLigne(iIndexLigneAEditer - 1, iIndexColonne, sIdTable);
				}
			} else if (e.shiftKey && e.key === 'ArrowRight') { 					// touche directionnelle droite + maj enfoncé (39)
				bPasDeValeurInvalideSaisie = quitterSaisieCellule(iIndexLigneAEditer, iIndexColonne, sIdTable, true, false);
				if (bPasDeValeurInvalideSaisie) {
					sélectionnerCellule(iIndexLigneAEditer, iIndexColonne + 1, sIdTable);
				}
			} else if (e.shiftKey && e.key === 'ArrowDown') { 					// touche directionnelle bas + maj enfoncé (40)
				bPasDeValeurInvalideSaisie = quitterSaisieLigne(iIndexLigneAEditer, iIndexColonne, sIdTable, true, true);
				if (bPasDeValeurInvalideSaisie) {
					éditerLigne(iIndexLigneAEditer + 1, iIndexColonne, sIdTable);
				}
			}
		}
	})
}


/**
 * Passe en mode édition la cellule demandée
 * si celle-ci a été paramétrée comme éditable
 * @param {Integer} iIndexLigne le numéro de la ligne
 * @param {Integer} iIndexColonne le numéro de la colonne
 * @param {String} sIdTable l'id de la table
 * @return {Boolean} false si cellule non éditable
 */
function éditerCelluleNew(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos'){
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	//clog('oInfosCell', oInfosCell);
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	insérerObjetDansCellule(iIndexLigne, iIndexColonne);
	cell.addClass('edition');
}

/**
 * Insère un objet (input/select..) dans la cellule spécifiée
 * @param {Integer} iIndexLigne Index de la ligne
 * @param {Integer} iIndexColonne Index de la colonne
 * @param {Integer} sIdTable Id de la table
 */
function insérerObjetDansCellule(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	let sCodeHtml = aColDepuisId(oInfosCell.sIdColonne).obtCodeHtmlObjet(+oInfosCell.sIdLigne);
	//sCodeHtml += sHtmlBoutonSauvegarder;
	cellule(iIndexLigne, iIndexColonne, sIdTable).html(sCodeHtml);
}

function sélectionnerObjet(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	cellule(iIndexLigne, iIndexColonne).children().select();
}

/**
 * Écoute les changements de valeur sur l'objet situé dans la cellule spécifiée
 * et ajoute la classe 'modified' à la cellule si valeur saisie (formatée pr stockage) !== de valeur stockée initiale
 * @param {Integer} iIndexLigne Index de la ligne
 * @param {Integer} iIndexColonne Index de la colonne
 * @param {Integer} sIdTable Id de la table
 */
function détecterChangementValeurSaisieDansCellule(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	let cell, sFormatStockage, sValeurInitialeFormatStockage, sValeurSaisieFormatAffichage, sValeurSaisieFormatStockage;
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	sFormatStockage = obtPropColDepuisIndex(iIndexColonne, 'format_stockage');
	sValeurInitialeFormatStockage = obtValeurCelluleDepuisIndex(iIndexLigne, iIndexColonne, 'stockage');
	cell = cellule(iIndexLigne, iIndexColonne);
	let bCelluleModifiée = false;
	if (oInfosCell.sType === 'texte')  {											// texte		input_texte
		cell.children('input').on('keyup', function() {
			définirCelluleCommeModifiéeSiValeursDifférentes(sValeurInitialeFormatStockage, iIndexLigne, iIndexColonne, sIdTable);
		})
	} else if (oInfosCell.sType === 'nombre' || oInfosCell.sType === 'date')  {		// date			input_date
		cell.children('input').on('change', function() {
			définirCelluleCommeModifiéeSiValeursDifférentes(sValeurInitialeFormatStockage, iIndexLigne, iIndexColonne, sIdTable);
		})
	} else if (oInfosCell.sType === 'boolean') {									// booléen		checkbox.switch
		cell.children('input').on('change', function() {
			définirCelluleCommeModifiéeSiValeursDifférentes(sValeurInitialeFormatStockage, iIndexLigne, iIndexColonne, sIdTable);
		})
		//cell.html(sValeurSaisieFormatStockage);
	} else if (oInfosCell.sType === 'enum') {										// enum			select
		cell.children('select').on('change', function() {
			définirCelluleCommeModifiéeSiValeursDifférentes(sValeurInitialeFormatStockage, iIndexLigne, iIndexColonne, sIdTable);
		})
		//cell.html(sValeurSaisieFormatStockage);
	} else {
		//cell.html('???');
		return;
	}
}

function définirCelluleCommeModifiéeSiValeursDifférentes(sValeurInitiale, iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	let sValeurSaisie = obtValeurSaisie(iIndexLigne, iIndexColonne, sIdTable, 'stockage');
	// clog('sValeurInitiale', sValeurInitiale);
	// clog('sValeurSaisie', sValeurSaisie);
	if (oInfosCell.sType === 'enum') {
		sValeurSaisie = sValeurSaisie.toUpperCase();
		sValeurInitiale = sValeurInitiale.toUpperCase();
	}
	if (sValeurSaisie == sValeurInitiale)  {
		cellule(iIndexLigne, iIndexColonne).removeClass('modified');	return false;
	} else {
		cellule(iIndexLigne, iIndexColonne).addClass('modified');		return true;
	}
}

function obtValeurSaisie(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos', sPourAffichageOuStockage = 'stockage') {
	let cell, sFormatStockage, sFormatAffichage, sValeurInitialeFormatStockage, sValeurSaisieFormatAffichage, sValeurSaisieFormatStockage;
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	let sRetour;
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	sFormatStockage = obtPropColDepuisIndex(iIndexColonne, 'format_stockage');
	sFormatAffichage = obtPropColDepuisIndex(iIndexColonne, 'format_affichage');
	sValeurInitialeFormatStockage = obtValeurCelluleDepuisIndex(iIndexLigne, iIndexColonne, 'stockage');
	if (oInfosCell.sType !== 'enum') {
		sValeurInitialeFormatStockage = obtDonnéeFormatée(sValeurInitialeFormatStockage, sFormatStockage);
	}
	cell = cellule(iIndexLigne, iIndexColonne);
	if (oInfosCell.sType === 'texte')  {								// texte	(input_texte)
		sValeurSaisieFormatAffichage = cell.children('input').val().trim();
		sValeurSaisieFormatStockage = obtDonnéeFormatée(sValeurSaisieFormatAffichage, sFormatStockage);
	} else if (oInfosCell.sType === 'nombre')  {						// nombre	(input_nombre)
		sValeurSaisieFormatStockage = +cell.children('input').val();
	} else if (oInfosCell.sType === 'date')  {							// date		(input_date)
		sValeurSaisieFormatStockage = cell.children('input').val();
	} else if (oInfosCell.sType === 'boolean') {						// booléen	(checkbox)
		sValeurSaisieFormatStockage = cell.children('input').is(':checked');
	} else if (oInfosCell.sType === 'enum') {							// enum		(select)
		sValeurSaisieFormatStockage = cell.children('select').val();
	}
	sRetour = sValeurSaisieFormatStockage;
	if (sPourAffichageOuStockage === 'stockage' && oInfosCell.sType !== 'enum') {
		sRetour = obtDonnéeFormatée(sRetour, sFormatStockage);
	} else if (sPourAffichageOuStockage === 'affichage') {
		if (oInfosCell.sType === 'enum')  {
			sRetour = obtValeurAffichageEnum(sValeurSaisieFormatStockage, oInfosCell.sIdColonne, true, false);
		}
		//if (sValeurSaisieFormatAffichage === undefined)  { sValeurSaisieFormatAffichage = sValeurSaisieFormatStockage; }
		if (oInfosCell.sType === 'boolean') {
			sRetour = '' + sRetour;
		} else {
			sRetour = obtDonnéeFormatée(sRetour, sFormatAffichage);
		}
	}
	return sRetour;
}

function enregistrerValeurSaisieSiValide(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos', oConfigTable = oConfig) {
	sIdTable = oConfigTable.sId;
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	let sValeurAStocker = obtValeurSaisie(iIndexLigne, iIndexColonne, sIdTable, 'stockage');
	let sValeurAAfficher = obtValeurSaisie(iIndexLigne, iIndexColonne, sIdTable, 'affichage');
	let bEstValeurValide = estDonnéeSaisieValide(iIndexLigne, iIndexColonne, sIdTable);
	let cell = cellule(iIndexLigne, iIndexColonne);
	if (bEstValeurValide) {
		cell.html(sValeurAAfficher);
		oConfigTable.aLignes[+oInfosCell.sIdLigne][oInfosCell.sIdColonne] = sValeurAStocker;
		cell.removeClass('edition');
		cell.removeClass('modified');
	} else {
		alert(`Enregistrement impossible : la donnée saisie n'est pas valide !`)
		$(`#${sIdTable} tbody tr:eq(${iIndexLigne}) td:eq(${iIndexColonne})`).children().select();
	}
	return bEstValeurValide;
}

function estDonnéeSaisieValide(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	let sValeurSaisie = obtValeurSaisie(iIndexLigne, iIndexColonne, sIdTable);
	let bEstValeurValide = aColDepuisId(oInfosCell.sIdColonne).estValide(sValeurSaisie);
	return bEstValeurValide;
}

/**
 * Quitte la cellule en cours d'édition (avec ou sans sauvegarde des données)
 * @param {*} iIndexLigne Index de la ligne
 * @param {*} iIndexColonne Index de la colonne
 * @param {String} sIdTable Id de la table
 * @param {Boolean} bEnregistrer Sauvegarde les modifications effectuées
 * @param {Boolean} bConfirmAvantEnregistrement Demande confirmation avant enregistrement
 * @return {Boolean} Renvoie false si la donnée à sauvegarder n'est pas valide
 */
function quitterSaisieCellule(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos', bEnregistrer = true, bConfirmAvantEnregistrement = true) {
	let cell, sFormatStockage, sValeurInitialeFormatStockage, sValeurInitialeFormatAffichage;
	let oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
	let bValeurSauvegardée;
	let bPasDeValeurInvalideSaisie = true;
	if (!oInfosCell.bEstCelluleEditable)  { return; }
	sFormatStockage = obtPropColDepuisIndex(iIndexColonne, 'format_stockage');
	sValeurInitialeFormatStockage = obtValeurCelluleDepuisIndex(iIndexLigne, iIndexColonne, 'stockage');
	sValeurInitialeFormatAffichage = obtValeurCelluleDepuisIndex(iIndexLigne, iIndexColonne, 'affichage');
	if (oInfosCell.sType !== 'enum') {
		sValeurInitialeFormatStockage = obtDonnéeFormatée(sValeurInitialeFormatStockage, sFormatStockage);
	}
	cell = cellule(iIndexLigne, iIndexColonne);
	if (cell.hasClass('modified')) {
		cell.addClass('question');
		if (bConfirmAvantEnregistrement) {
			bEnregistrer = confirm('Souhaitez-vous enregistrer les modifications apportées à la cellule ?');
		}
		cell.removeClass('question');
		if (bEnregistrer) {
			bValeurSauvegardée = enregistrerValeurSaisieSiValide(iIndexLigne, iIndexColonne, sIdTable);
			if (!bValeurSauvegardée) {
				bPasDeValeurInvalideSaisie = false;
			}
		} else {
			cell.html('' + sValeurInitialeFormatAffichage);
			cell.removeClass('edition');
			cell.removeClass('modified');
		}
	} else {
		cell.html(sValeurInitialeFormatAffichage);
		cell.removeClass('edition');
		cell.removeClass('modified');
	}
	return bPasDeValeurInvalideSaisie;
}

function quitterSaisieLigne(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos', bEnregistrer = true, bConfirmAvantEnregistrement = true) {
	let bPasDeValeurInvalideSaisieDansLigne = true, bPasDeValeurInvalideSaisieDansCell;
	let oInfosCell;
	ligne = $(`#${sIdTable} tbody tr:eq(${iIndexLigne})`);
	ligne.children(`td`).each(function() { // parcourt chaque cellule de la ligne
		iIndexColonne = $(this).index();
		oInfosCell = obtInfosCelluleDepuisIndex(iIndexLigne, iIndexColonne, sIdTable);
		if (oInfosCell.bEstCelluleEditable)  {
			bPasDeValeurInvalideSaisieDansCell = quitterSaisieCellule(iIndexLigne, iIndexColonne, sIdTable, bEnregistrer, bConfirmAvantEnregistrement);
			if (bPasDeValeurInvalideSaisieDansCell) {
				$(this).removeClass('edition');
				$(this).removeClass('modified');
			} else {
				bPasDeValeurInvalideSaisieDansLigne = false;
			}
		}
	})
	return bPasDeValeurInvalideSaisieDansLigne;
}

function quitterSaisieLignes(sIdTable = 'table_promos', bEnregistrer = true, bConfirmAvantEnregistrement = true) {
	let ligne, iIndexLigne, bPasDeValeurInvalideSaisieDansLignes = true, bPasDeValeurInvalideSaisieDansLigne;
	$(`#${sIdTable} tbody tr`).each(function() {
		ligne = $(this);
		iIndexLigne = ligne.index();
		bPasDeValeurInvalideSaisieDansLigne = quitterSaisieLigne(iIndexLigne, 0, sIdTable, bEnregistrer, bConfirmAvantEnregistrement);
		if (!bPasDeValeurInvalideSaisieDansLigne) {
			bPasDeValeurInvalideSaisieDansLignes = false;
		}
	});
	return bPasDeValeurInvalideSaisieDansLignes;
}

function supprimerLigne(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	ligne(iIndexLigne).children('td').each(function(){
		$(this).addClass('question');
	})
	let bSupprimerLigne = confirm(`Souhaitez-vous vraiment supprimer la ligne ?`);
	if (bSupprimerLigne) {
		ligne(iIndexLigne).remove();
	}
	ligne(iIndexLigne).children('td').each(function(){
		$(this).removeClass('question');
	})
}

function sélectionnerCellule(iIndexLigne = 0, iIndexColonne = 0, sIdTable = 'table_promos') {
	cellule(iIndexLigne, iIndexColonne, sIdTable).children().select();
}

function ajouterLigne(sIdTable = 'table_promos', oConfigTable = oConfig) {
	//var aValeursPrDatatable = obtArrayValeursDepuisLigneDonnéesFormulaires(oLignePromoDonnéesDesFormulaires) // récupère les valeurs dans un objet array
	//var aValeursPrDatatable = ['zzz',2,3,4,5,6,7,8];
	var oValeurs = {
		prenom:	'toto',
		nom:	'zorro',
		sexe:	'3',
		email:	'4',
		age:	'5',
		est_actif:	'6',
		date_debut: 'date',
		editer_supprimer:	'7'
	}
	oConfigTable.oDataTable = oConfigTable.oTable.DataTable().row.add(oValeurs).draw(true); // envoie les valeurs pour les intégrer au datatable
}

// ANCIEN


// function remplacerIndexPourFonctionBouton(objCellulesVisiblesNonEditables, sIndex) {
// 	for (let i = 0; i < Object.keys(objCellulesVisiblesNonEditables).length; i++) {
// 		sClé 	= Object.keys(objCellulesVisiblesNonEditables)[i];
// 		sValeur = objCellulesVisiblesNonEditables[sClé];
// 		sValeur = sValeur.replace(/\^index\^/i, sIndex);
// 		objCellulesVisiblesNonEditables[sClé] = sValeur;
// 	}
// }

// function obtArrayValeursDepuisLigneDonnéesFormulaires(objLigneDonnéesAAjouter) {
// 	var sTypeDonnée;
// 	var aValeursPourAjoutDansDatatable = [];
// 	for (let i = 0; i < aIndexColonnesSelonPositionDansTableau.length; i++) {
// 		sIndex		= aIndexColonnesSelonPositionDansTableau[i];
// 		sTypeDonnée = oConfigTable.aColonnes[sIndex].id
// 		sValeur 	= objLigneDonnéesAAjouter[sTypeDonnée];
// 		aValeursPourAjoutDansDatatable.push(sValeur)
// 	}
// 	return aValeursPourAjoutDansDatatable;
// }

// function ajouterPromo()	{
// 	return;
// 	var iIndexPromo = oConfig.aLignes.length;
// 	var oLignePromoDonnéesDesFormulaires 	= obtDonnéesDepuisFormulaires();	 // valeurs des formulaires
// 	oConfig.aLignes[iIndexPromo] 			= oLignePromoDonnéesDesFormulaires;
// 	var oCellulesVisiblesNonEditables 			= obtCellulesVisiblesNonEditables(); // boutons
// 	remplacerIndexPourFonctionBouton(oCellulesVisiblesNonEditables, iIndexPromo); // index des promos pour clics sur boutons
// 	ajouterPropriétésObj(oLignePromoDonnéesDesFormulaires, oCellulesVisiblesNonEditables) // ajoute les boutons
// 	var aValeursPrDatatable = obtArrayValeursDepuisLigneDonnéesFormulaires(oLignePromoDonnéesDesFormulaires) // récupère les valeurs dans un objet array
// 	oConfigTable.oTable.DataTable().row.add(aValeursPrDatatable).draw(true); // envoie les valeurs pour les intégrer au datatable
// 	afficherFormulaires();
// }

// function obtDonnéesDepuisFormulaires() {
// 	var obj 			= {};
// 	var iIndexColonne;
// 	var sIdColonne;
// 	var sTypeColonne;
// 	for (let iC = 0; iC < aIndexColonnesSelonPositionDansTableau.length; iC++) {
// 		iIndexColonne	= aIndexColonnesSelonPositionDansTableau[iC];
// 		sIdColonne		= oConfigTable.aColonnes[iIndexColonne]['id'];
// 		sTypeColonne	= oConfigTable.aColonnes[iIndexColonne]['type'];
// 		bEditable		= oConfigTable.aColonnes[iIndexColonne]['éditable'];
// 		if (sTypeColonne !== 'bouton' && bEditable) { // on va récupérer toutes les données différentes d'un bouton
// 			// pour chaque champ éditable, récupère la valeur du formulaire
// 			oFormulaire = $('#' + sIdColonne);
// 			sTypeFormulaire		= oFormulaire.attr('type');
// 			if (sTypeFormulaire === 'date') {
// 				// si input_date --> applique formatage
// 				sFormatAffichageVoulu 	= oConfigTable.aColonnes[iIndexColonne]['format_affichage']
// 				sValeur					= oFormulaire.val();
// 				sValeur 				= obtDonnéeFormatée(sValeur, 'aaaa-mm-jj')
// 			} else {
// 				sValeur					= oFormulaire.val();
// 			}
// 			obj[sIdColonne] 	= sValeur;
// 		}
// 	}
// 	return obj;
// }

// function obtCellulesVisiblesNonEditables(sIndexARemplacerDansFonction = '') {
// 	var cellulesVisiblesNonEditables = {};
// 	for (let i = 0; i < oConfigTable.aColonnes.length; i++) {
// 		bVisibleDansTableau = oConfigTable.aColonnes[i].visible_dans_tableau;
// 		bEditable 			= oConfigTable.aColonnes[i].éditable;
// 		if (bVisibleDansTableau && !bEditable) {
// 			sTypeDonnée 	= oConfigTable.aColonnes[i].id;
// 			sContenuCellule = oConfigTable.aColonnes[i].contenu;
// 			cellulesVisiblesNonEditables[sTypeDonnée] = sContenuCellule;
// 		}
// 	}
// 	return cellulesVisiblesNonEditables;
// }

// function majPropriétéPromo(iIndexPromo, sIdPropriété, sValeur) {
// 	sValeur = sValeur.trim();
// 	iIndexColonne 		= $('#' + sIdPropriété).index() // récupère l'index de la colonne de la cellule dont l'id correspond à la propriété voulue
// 	sValeurAvantModif 	= oConfigTable.oDataTable.cell(iIndexPromo, iIndexColonne).data();
// 	oConfigTable.aLignes[iIndexPromo][sIdPropriété]	= sValeur;
// 	bValeurDifférente = (sValeurAvantModif.toLowerCase() != sValeur.toLowerCase())
// 	if (bValeurDifférente) {
// 		oConfigTable.oDataTable.cell(iIndexPromo, iIndexColonne).data(sValeur); // met à jour la cellule
// 		$('tbody tr.édit_ligne td').eq(iIndexColonne).addClass('val_modifiée');

// 	}
// }

// function majPromo(iIndexPromo = null) { // bouton 'modifier'
// 	if (iIndexPromo === null)	{ iIndexPromo = dt.iIndiceLigneEnEdition }
// 	var iC;
// 	var iIndexColonne;
// 	var sDonnéeColonne;
// 	var sIdColonne;
// 	var sTypeColonne;
// 	var iNbColonnes		= oConfigTable.aColonnes.length;
// 	for (iC=0; iC<iNbColonnes; iC++) {
// 		iIndexColonne	= aIndexColonnesSelonPositionDansTableau[iC];
// 		sIdColonne		= oConfigTable.aColonnes[iIndexColonne]['id'];
// 		sTypeColonne	= oConfigTable.aColonnes[iIndexColonne]['type'];
// 		sTypeChamp		= $('#' + sIdColonne).attr('type');
// 		bEditable		= oConfigTable.aColonnes[iIndexColonne]['éditable'];
// 		sFormatAffichageVoulu = oConfigTable.aColonnes[iIndexColonne]['format_affichage']
// 		function remplirCellule() {

// 		}
// 		if (sTypeColonne !== 'bouton' && bEditable) { // on va récupérer toutes les données différentes d'un bouton
// 			// pour chaque champ éditable
// 			switch (sTypeChamp) {
// 				case 'text' :		sValeur	= $('#' + sIdColonne).val(); break;
// 				case 'number' :		sValeur	= $('#' + sIdColonne).val(); break;
// 				case 'date' :
// 					sValeur	= $('#' + sIdColonne).val();
// 					// alert(sValeur)
// 					// alert(sFormatAffichageVoulu)
// 					//sValeur = sValeur.split("-").reverse().join("/");
// 					sValeur = obtDonnéeFormatée(sValeur, 'aaaa-mm-jj')
// 					break;
// 			}
// 				majPropriétéPromo(iIndexPromo, sIdColonne, sValeur)
// 		}
// 	}
// 	afficherFormulaires();
// }

// function éditerPromo(iIndiceEdit, sIdChampASélectionner = null)	{
// 	dt.iIndiceLigneEnEdition = iIndiceEdit;
// 	// dbg ligne ajoutée ne contient pas d'id --> pas de css
// 	$('tr').removeClass('édit_ligne'); 					 	// suppr classe 'édit_ligne' sur chaque ligne
// 	$('#' + dt.iIndiceLigneEnEdition).addClass('édit_ligne'); // ajoute classe 'édit_ligne' à la ligne à éditer
// 	afficherFormulaires(iIndiceEdit);					 	// montre les formulaires
// 	$('.formulaire').change(function(e){
// 		// dbg
// 		if (edt.iIndiceLigneEnEdition.isStrInteger()) {
// 			$('#btn_modifier').removeClass('hide');			// affiche le bouton 'modifier'
// 		}
// 	});

// 	if (sIdChampASélectionner !== null) {
// 		sTypeInput = $('#' + sIdChampASélectionner).attr('type');
// 		if (sTypeInput === 'date') {
// 			$('#' + sIdChampASélectionner).focus();			// donne le focus si input date
// 		} else {
// 			$('#' + sIdChampASélectionner).select();		// sélectionne si input texte
// 		}
// 	} else {
// 		$('tbody tr.édit_ligne td').eq(0).addClass('édit_cell');
// 	}
// }

// function afficherFormulaires(iIndiceEdit = null) {
// 	$('tbody td').removeClass('édit_cell'); // mise en forme de la cellule à éditer
// 	if (iIndiceEdit === null)	{
// 		// supprime les classes d'édition
// 		$('#' + dt.iIndiceLigneEnEdition).removeClass('édit_ligne'); // mise en forme de la ligne à éditer
// 		$('#btn_annuler').addClass('hide');		// affiche le bouton 'annuler'
// 	} else {
// 		$('#btn_annuler').removeClass('hide');	// affiche le bouton 'annuler'
// 	}
// 	var iC;
// 	var iIndexColonne;
// 	var sDonnéeColonne;
// 	var sIdColonne;
// 	var sTypeColonne;
// 	var iNbColonnes				= oConfigTable.aColonnes.length;
// 	var sIdPremierFormulaire	= oConfigTable.aColonnes[0]['id'];
// 	$('.ctn_formulaire').remove();
// 	for (iC = 0; iC < iNbColonnes; iC++) {
// 		iIndexColonne	= aIndexColonnesSelonPositionDansTableau[iC];
// 		sDonnéeColonne	= oConfigTable.aColonnes[iIndexColonne]['libellé'];
// 		sIdColonne		= oConfigTable.aColonnes[iIndexColonne]['id'];
// 		sTypeColonne	= oConfigTable.aColonnes[iIndexColonne]['type'];
// 		bEditable		= oConfigTable.aColonnes[iIndexColonne]['éditable'];
// 		if (sTypeColonne !== 'bouton' && bEditable) { // on va récupérer toutes les données différentes d'un bouton
// 			// pour chaque colonne à afficher
// 			oColonne = oConfigTable.aColonnes[iIndexColonne] ;
// 			sCodeChamp = "<p class='ctn_formulaire'>" + sDonnéeColonne + " : " + obtenirCodeHtmlFormulaire(iIndexColonne) + "</p>";
// 			$("#champs").append(sCodeChamp);
// 			// Récupère la valeur du cache et l'affiche 'propre' dans le formulaire input
// 			if (iIndiceEdit === null) {
// 				sDonnéeFormatéePrStockage = '';
// 			} else {
// 				sFormatStockage				= oConfigTable.aColonnes[iIndexColonne].format_stockage;
// 				sFormatAffichage			= oConfigTable.aColonnes[iIndexColonne].format_affichage;
// 				sDonnéeEnCache				= oConfig.aLignes[iIndiceEdit][sIdColonne];
// 				//sDonnéeFormatéePrStockage	= obtDonnéeFormatée(sDonnéeEnCache, sFormatStockage)
// 				// dbg : on ne cherche pas le format d'affichage mais de stockage
// 				sDonnéeFormatéePrStockage	= sDonnéeEnCache;
// 				//if (sTypeColonne === 'date') {alert('avt:' + sDonnéeEnCache)}
// 				//sDonnéeFormatéePrAffichage	= obtDonnéeFormatée(sDonnéeEnCache, sFormatAffichage, )
// 			}
// 			$('#' + sIdColonne).val(sDonnéeFormatéePrStockage);
// 			if (sTypeColonne === 'date') {
// 				// pour le css du placeholder pour input[type=date]
// 				if (sDonnéeFormatéePrStockage.trim() === '') {
// 					$('#' + sIdColonne).addClass('empty');
// 				}
// 				$('#' + sIdColonne).on('change', function(){
// 					if (this.value === '') {
// 						this.classList.add('empty');
// 					} else {
// 						this.classList.remove('empty');
// 					}
// 				})
// 			}
// 			$('#' + sIdPremierFormulaire).select();
// 		}
// 	}
// 	$('.formulaire').keypress(function(e){
// 		if (dt.iIndiceLigneEnEdition.isStrInteger()) {
// 			$('#btn_modifier').removeClass('hide');			 	// affiche le bouton 'modifier'
// 		}
// 		$('#btn_ajouter').removeClass('hide');	// affiche le bouton 'ajouter'
// 		$('#btn_annuler').removeClass('hide');	// affiche le bouton 'annuler'
// 	});
// 	$('.formulaire').blur(function(e){
// 		sNomChamp			= $(this).attr('id');
// 		//alert(sNomChamp + iIndexColonneDansArray + iIndexColonneDansTableauAffiché + sType + bVisibleDsTab)
// 		sTexteSaisi			= $(this).val();
// 		iIndexColonneDansArray			= obtenirIndexDansTableauSiCorrespondance(oConfigTable.aColonnes, 'id', sNomChamp);
// 		iIndexColonneDansTableauAffiché = $('#_' + sNomChamp).index();
// 		sType 				= oConfigTable.aColonnes[iIndexColonneDansArray].type;
// 		bVisibleDsTab		= oConfigTable.aColonnes[iIndexColonneDansArray].visible_dans_tableau;
// 		if (bVisibleDsTab) {
// 			//alert(sFormatStockage + ' -- ' + sFormatAffichage)
// 			sIdColonne					= oConfigTable.aColonnes[iIndexColonneDansArray]['id'];
// 			sTypeInput					= $('#' + sIdColonne).attr('type');
// 			if (sTypeInput === 'date') {
// 				//$(this).val(sDonnéeFormatéePrAffichage); // pas de formatage si date (car la date provient du datepicker)
// 			} else {
// 				sFormatStockage		= oConfigTable.aColonnes[iIndexColonneDansArray].format_stockage;
// 				sFormatAffichage	= oConfigTable.aColonnes[iIndexColonneDansArray].format_affichage;
// 				sDonnéeFormatéePrStockage	= obtDonnéeFormatée(sTexteSaisi, sFormatStockage, sFormatAffichage)
// 				sDonnéeFormatéePrAffichage	= obtDonnéeFormatée(sTexteSaisi, sFormatAffichage, sFormatStockage)
// 				$(this).val(sDonnéeFormatéePrAffichage);
// 			}
// 			//$('tbody tr.édit_ligne td').eq(iIndexColonneDansTableauAffiché).text(sDonnéeFormatéePrAffichage);
// 		}
// 	});
// 	$('.formulaire').focusin(function(e){ // dbg: évt non déclenché qd dblclic ds 1ère colonne
// 		//alert()
// 		// quand on va éditer un formulaire... pour mise en valeur de la donnée à éditer dans le tableau html
// 		sNomChamp		= $(this).attr('id');
// 		sTexteSaisi		= $(this).val();
// 		iIndexColonneDansArray			= obtenirIndexDansTableauSiCorrespondance(oConfigTable.aColonnes, 'id', sNomChamp); //dbg
// 		iIndexColonneDansTableauAffiché = $('#_' + sNomChamp).index();
// 		sType 			= oConfigTable.aColonnes[iIndexColonneDansArray].type;
// 		sFormat			= oConfigTable.aColonnes[iIndexColonneDansArray].format;
// 		bVisibleDsTab	= oConfigTable.aColonnes[iIndexColonneDansArray].visible_dans_tableau;
// 		if (bVisibleDsTab) {
// 			$('tbody tr.édit_ligne td').removeClass('édit_cell');
// 			$('tbody tr.édit_ligne td').eq(iIndexColonneDansTableauAffiché).addClass('édit_cell');
// 		}
// 	});
// }

// // Renvoie le code html d'un input selon les attributs définis dans oConfigTable.aColonnes
// function obtenirCodeHtmlFormulaire(iIndexColonne) {
// 	var sCodeFormulaire = '<input';
// 	sIdColonne = oConfigTable.aColonnes[iIndexColonne].id;
// 	attributs = oConfigTable.aColonnes[iIndexColonne].input;
// 	if (typeof(attributs) !== 'object')		{ attributs = {}; }
// 	// ajoute une classe 'formulaire'
// 	// if (attributs.hasOwnProperty('class')) {
// 	// 	attributs.class += ' formulaire'; // dbg: classe ajoutée plusieurs fois
// 	// } else {
// 	// 	attributs.class  = 'formulaire';
// 	// }
// 	// ajoute l'id si non défini
// 	if (!attributs.hasOwnProperty('id')) {
// 		attributs.id = sIdColonne;
// 	}
// 	// ajoute le nom si non défini
// 	if (!attributs.hasOwnProperty('name')) {
// 		attributs.name = sIdColonne;
// 	}
// 	// ajoute le type 'texte' si type non défini
// 	if (!attributs.hasOwnProperty('type')) {
// 		attributs.type = 'text';
// 	}
// 	iNbAttributs = Object.keys(attributs).length;
// 	for (i = 0; i < iNbAttributs; i++) {
// 		sNomAttribut		= Object.keys(attributs)[i];
// 		sValeurAttribut		= attributs[sNomAttribut];
// 		sCodeFormulaire	   += ' ' + sNomAttribut + "=\"" + sValeurAttribut + "\"";
// 	}
// 	sCodeFormulaire 	   += '>';
// 	return sCodeFormulaire;
// }
