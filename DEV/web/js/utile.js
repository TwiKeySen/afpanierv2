

// INT - DATES
/**
 * Renvoie si une date existe ou pas
 * @param {TinyInt} iJour Numéro du jour dans le mois
 * @param {TinyInt} iMois Numéro du mois
 * @param {Integer} iAnnée Année
 * @return {Boolean} True si la date existe
 */
function estDateExistante(iJour, iMois, iAnnée) {
	if (arguments.length === 1) {
		let aDate = arguments[0].split('-');
		iJour = +aDate[2];
		iMois = +aDate[1];
		iAnnée = +aDate[0];
	}
	if (iJour < 1 || iMois < 1 || iMois > 12) {
		return false
	}
	return iJour > obtenirNbJoursDansMois(iMois, iAnnée) ? false : true
}
/**
 * Renvoie le nombre de jours présents dans le mois d'une année donnée
 * @param {TinyInt} iMois Numéro du mois
 * @param {Integer} iAnnée Année
 * @return {TinyInt} Nombre de jours
 */
function obtenirNbJoursDansMois(iMois, iAnnée) {
	if (iMois == 2) {
		if (iAnnée % 4 == 0) {
			if (iAnnée % 100 == 0) {
				return (iAnnée % 400 == 0) ? 29 : 28
			}
			return 29
		}
		return 28
	}
	if (iMois < 8) {
		return (iMois % 2 == 0) ? 30 : 31
	}
	return (iMois % 2 == 1) ? 30 : 31
}

function obtDateAvecZéro(sDate = '2020-4-1') {
	let aDate = sDate.split('-');
	if (aDate[1].length === 1)  { aDate[1] = '0' + aDate[1]; }
	if (aDate[2].length === 1)  { aDate[2] = '0' + aDate[2]; }
	return aDate.join('-');
}



// A TRIER

function clogd(sNomFonctionAppelante, ...args) {
    var sNomFonctionADéboguer;
    switch (typeof(fctDebug)) {
        case 'object': 
            fctDebug.forEach(function(sNomFonctionADéboguer) {
                if (bModeDebug && sNomFonctionAppelante === sNomFonctionADéboguer) { console.log(...args); }
            } )
            break;
        case 'string': 
            sNomFonctionADéboguer = fctDebug;
            if (bModeDebug && sNomFonctionAppelante === sNomFonctionADéboguer) { console.log(...args); }
    }
    
}


// Ajoute les propriétés d'un objet à un autre objet						// sNomObjModifieurDansObjALire = 'donnéeDefs', cibles = ['params', 'données'], sNomPropContenantDéfinitionDesCibles = 'cibles'
function ajouterPropriétésObj(objAModifier, objALire, bEcraserSiExiste = false, ciblage = undefined) {
	// avec Object.assign(obj1, obj2) : les props de 'obj2' sont passées à obj1 avec écrasement (non utilisé ici)
	// !! POUR DEBOGAGE EN CONSOLE : 	définir  'bModeDebug = true'  plus haut
	// configuration pour une modification ciblée 	(cet objet 'cfg' peut être fourni en argument dans 'ciblage')
	var cfg = {						   
		//sNomObjModifieurDansObjALire: 			  		'donnéeDefs',	// nom de l'objet qui contient les propriétés à modifier à l'endroit voulu
		//sNomPropContenantObjetsCiblésDansObjAModifier: 	'data',			// nom de l'array qui contient les objets dont on souhaite créer/modifier les propriétés, grâce aux index renseignés dans 'cibles'
		sNomPropContenantDéfinitionDesCibles: 				'cibles',		// nom de la propriété qui contient '_tout' / string / integer / array[str..ent]
		sValPropCiblesPourCiblageSurTousLesEléments:		'_tout'			// valeur contenue dans 'cibles', qui permettra de traiter Tous les éléments de l'array 'sNomPropContenantObjetsCiblésDansObjAModifier'
	};
	var f = 'ajouterPropriétésObj';
	var iLongueursNomPropContenantObjetsCiblésDansObjAModifier;
	var props 		= Object.getOwnPropertyNames(objAModifier);
	if (typeof(objAModifier) !== 'object' || typeof(objALire) !== 'object') { return false; }
if (typeof(ciblage) === 'object') {
//clogd( f, 'ciblage',  ciblage  );
ajouterPropriétésObj(cfg, ciblage, true)
// provisoire :
// 	ajouterPropriétésObj(cfg, {						   
// 	sNomObjModifieurDansObjALire: 			  		'donnéeDefs',	// nom de l'objet qui contient les propriétés à modifier à l'endroit voulu
// 	sNomPropContenantObjetsCiblésDansObjAModifier: 	'data',			// nom de l'array qui contient les objets dont on souhaite créer/modifier les propriétés, grâce aux index renseignés dans 'cibles'
// 	sNomPropContenantDéfinitionDesCibles: 			'cibles',		// nom de la propriété qui contient '_tout' / string / integer / array[str..ent]
// 	sValPropCiblesPourCiblageSurTousLesEléments:	'_tout'			// valeur contenue dans 'cibles', qui demande de traiter Tous les éléments de l'array 'sNomPropContenantObjetsCiblésDansObjAModifier'
// }, true)
		clogd( f, 'cfg (config du ciblage)',  cfg  );
	}
	var objAModifier_AvantModifs = jQuery.extend(true, {}, objAModifier);	// crée un clone de objAModifier
	clogd( f,	'objAModifier (avant modifs)',  	objAModifier_AvantModifs  )					

	for (prop in objALire) {	// var prop = Object.getOwnPropertyNames(objALire);
		if (hasOwnProperty.call(objALire, prop)) {
			bPropExiste = (objAModifier[prop] !== undefined)
			if (bPropExiste) {
				//sType = typeof(objALire[prop]);
				if (bEcraserSiExiste) {
					//if (prop != cfg.sNomObjModifieurDansObjALire) { dbg
						objAModifier[prop] = objALire[prop];
					//}
				}
			} else {
				objAModifier[prop] = objALire[prop];
			}
		}
	}

	var objAModifier_AprèsModifs = jQuery.extend(true, {}, objAModifier);	// crée un clone de objAModifier
	clogd( f,	'objAModifier (après modifs)',  	objAModifier_AprèsModifs  )					

	if (typeof(ciblage) !== undefined) { // si objet ciblage passé en argument
		if (hasOwnProperty.call(objALire, cfg.sNomObjModifieurDansObjALire)) {
			if (objALire[cfg.sNomObjModifieurDansObjALire][0] !== undefined) {
				objModifieur = objALire[cfg.sNomObjModifieurDansObjALire][0];
			} else {
				objModifieur = objALire[cfg.sNomObjModifieurDansObjALire];
			}
			clogd( f,	'objModifieur',			objModifieur)
			//clogd( f, 'objALire[cfg.sNomObjModifieurDansObjALire]	 --------',		objALire[cfg.sNomObjModifieurDansObjALire])
			
			//clogd( f, 'length	--------',		Object.keys(objALire[cfg.sNomObjModifieurDansObjALire]).length)
		} else {
			return;
		}
		if (typeof(objModifieur) === 'object') {
			clogd( f,  'objALire', 						objALire  )
			var ciblesTemp 	= objModifieur[cfg.sNomPropContenantDéfinitionDesCibles];
			//clogd( f,	'cibles (brutes)',		'ciblesTemp',	'objModifieur[cfg.sNomPropContenantDéfinitionDesCibles]',	ciblesTemp)
			// met dans un array les cibles
			switch (typeof(ciblesTemp)) {
				case undefined: 
					cfg.bCibleDéfinie 	= false;
				case 'string' :
					cfg.bCibleDéfinie 	= true;
					if (ciblesTemp === cfg.sValPropCiblesPourCiblageSurTousLesEléments) {
						cfg.cibles		= cfg.sValPropCiblesPourCiblageSurTousLesEléments;
					} else {
						cfg.cibles		= [];
						cfg.cibles[0] 	= ciblesTemp;
					}
					break;
				case 'number' :
					cfg.bCibleDéfinie 	= true;
					cfg.cibles			= [];
					cfg.cibles[0] 		= ciblesTemp;
					break;
				case 'object' :
					cfg.bCibleDéfinie 	= true;
					cfg.cibles 			= ciblesTemp;
					break;
				default: 
					cfg.bCibleDéfinie 	= false;
			}
			clogd( f,  'bCibleDéfinie', 	cfg.bCibleDéfinie  )
			clogd( f,  'cibles', 	'cfg.cibles',		cfg.cibles  )
			clogd( f,  'tableauContenantObjetsCiblés', 		'objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier]', 		objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier]  )
			var objAModifier_AvantModifsCiblage = jQuery.extend(true, {}, objAModifier);	// crée un clone de objAModifier
			clogd( f,	'objAModifier (avant modifs ciblage)',  	objAModifier_AvantModifsCiblage  )					
			if (hasOwnProperty.call(objAModifier, cfg.sNomPropContenantObjetsCiblésDansObjAModifier)) {
					//objAModifier.data = [{clé1obj1: "valclé1obj1", clé2obj1: "valclé2obj1"}, {clé1obj2: "valclé1obj2", clé2obj2: "valclé2obj2"}, {clé1obj3: "valclé1obj3", clé2obj3: "valclé2obj3"}, {clé1obj4: "valclé1obj4", clé2obj4: "valclé2obj4"}]
					Object.keys(objModifieur).forEach(function(cléACopierVersCible) {
						iLongueursNomPropContenantObjetsCiblésDansObjAModifier = Object.keys(objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier]).length;
						clogd( f,	'iLongueursNomPropContenantObjetsCiblésDansObjAModifier',		iLongueursNomPropContenantObjetsCiblésDansObjAModifier )
						if (cléACopierVersCible !== cfg.sNomPropContenantDéfinitionDesCibles) {
							clogd( f, 'cléACopierVersCible', 		cléACopierVersCible )
							clogd( f, 'cléACopierVersCible.val', 	'objModifieur[cléACopierVersCible]',	objModifieur[cléACopierVersCible] )
							if (cfg.bCibleDéfinie) {
								if (cfg.cibles === cfg.sValPropCiblesPourCiblageSurTousLesEléments) {
									// on copie vers toutes les clés
									objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier].forEach(function(cible) {
										if (bEcraserSiExiste || typeof(cible[cléACopierVersCible]) === undefined) {
											// (si la propriété n'existe pas), on la crée
											// (si elle existe), on l'écrase si (bEcraserSiExiste === true)
											cible[cléACopierVersCible] = objModifieur[cléACopierVersCible];
										}
									})
								} else {
									// on copie uniquement les clés spécifiées
									cfg.cibles.forEach(function(cible) {
										switch (typeof(cible)) {
											case 'number': 
												if (cible < 0) {
													cible = iLongueursNomPropContenantObjetsCiblésDansObjAModifier + cible;
												}
										}
										if (bEcraserSiExiste || typeof(objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier][cible][cléACopierVersCible]) === undefined) {
											// (si la propriété n'existe pas), on la crée
											// (si elle existe), on l'écrase si (bEcraserSiExiste === true)
											objAModifier[cfg.sNomPropContenantObjetsCiblésDansObjAModifier][cible][cléACopierVersCible] = objModifieur[cléACopierVersCible];
										}
									})
								}
							}
						}
					})
					var objAModifier_AprèsModifsCiblage = jQuery.extend(true, {}, objAModifier);	// crée un clone de objAModifier
					clogd( f,  'objAModifier (après modifs ciblage)',  	objAModifier_AprèsModifsCiblage  )
			}
		};
	}
	return objAModifier;
}
