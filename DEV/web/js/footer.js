/**
 * Tableau temporaire de test avant mise en place BDD
 * @type {*[]}
 */

var aSocial = [];
aSocial[0] = [];
aSocial[0]["ordre"] = 1;
aSocial[0]["nom"] = "Facebook";
aSocial[0]["picto"] = "fa-facebook";
aSocial[0]["lien"] = "https://fr-fr.facebook.com/";

aSocial[1] = [];
aSocial[1]["ordre"] = 2;
aSocial[1]["nom"] = "Twitter";
aSocial[1]["picto"] = "fa-twitter";
aSocial[1]["lien"] = "https://twitter.com/?lang=fr";

aSocial[2] = [];
aSocial[2]["ordre"] = 3;
aSocial[2]["nom"] = "Youtube";
aSocial[2]["picto"] = "fa-youtube";
aSocial[2]["lien"] = "https://www.youtube.com/";

let i;

/**
 * Chargement à l'ouverture de la page
 */
$( document ).ready(function() {

    /**
     * Affichage du picto selon ordre définit dans le tableau et plus tard dans adm_social
     * @type {string}
     */
    document.getElementById("order1").className += "fab " + aSocial[0]["picto"] + "";
    document.getElementById("order2").className += "fab " + aSocial[1]["picto"] + "";
    document.getElementById("order3").className += "fab " + aSocial[2]["picto"] + "";

    /**
     * Attribution du lien correspondant à chaque picto et redirection sur un autre onglet
     * @type {HTMLElement}
     */
    let a = document.getElementById("link1");
    let b = document.getElementById("link2");
    let c = document.getElementById("link3");

    a.setAttribute("href", aSocial[0]["lien"]);
    b.setAttribute("href", aSocial[1]["lien"]);
    c.setAttribute("href", aSocial[2]["lien"]);
});

