aOfProducteurs = 
[
    {
        "idadministrateur": 0,
        "note" : 0,
        "nomcontact": "George",
        "prenomcontact": "Zukerberg",
        "telephonecontact": "0702030405",
        "emailcontact": "goliath@outlook.com",
        "code_agent": "A514u",
        "password": "",
        "password_confirmation": "",
    },
    {
        "idadministrateur": 1,
        "note" : 0,
        "nomcontact": "Marc",
        "prenomcontact": "Zapataz",
        "telephonecontact": "0425185935",
        "emailcontact": "pico83@gmail.com",
        "code_agent": "A842r",
        "password": "",
        "password_confirmation": "",
    }

]


function constructTable()	
{
    var i;

    var sHTML= "";
    sHTML+= "<thead class=\"bg-secondary text-white\">";
    sHTML+= "<tr>";
    sHTML+= "<td>nom</td>";
    sHTML+= "<td>prenom</td>";
    sHTML+= "<td>Tél</td>";
    sHTML+= "<td>E-mail</td>";
    sHTML+= "<td>code_agent</td>";
    sHTML+= "<td>password</td>";
    sHTML+= "<td>password_confirmation</td>";
    sHTML+= "</tr>";
    sHTML+= "</thead>";
    sHTML+= "<tbody>";

    for (i=0; i<aOfAdmins.length; i++)	
    {
        sHTML+= "<tr>";
        sHTML+= "<td>" + aOfAdmins[i]["nom"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["prenom"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["telephone"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["email"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["code_agent"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["password"] + "</td>";
        sHTML+= "<td>" + aOfAdmins[i]["password_confirmation"] + "</td>";
        sHTML+= "<td><button onClick=\"editAdmin" + i + ")\" class=\"btn_action rounded\" data-toggle=\"collapse\" data-target=\"#form_afficher\">Editer</button></td>";
        sHTML+= "<td><button onClick=\"supprimAdmin(" + i + ")\" class=\"btn_action rounded\">Supprimer</button></td>";
        sHTML+= "</tr>";
    }
    
    sHTML+= "</tbody>";
    $('#table_admins').html(sHTML);
}

function afficheAdmin()	
{
    document.getElementById("btn_ajouter").className = "btn_action rounded";
    document.getElementById("btn_afficher").className = "btn_action rounded collapse";
    document.getElementById("btn_modifier").className = "btn_action rounded collapse";
    document.getElementById("btn_annuler").className = "btn_action rounded";
}

function ajoutAdmin()	
{
    document.getElementById("btn_ajouter").className = "btn_action rounded collapse";
    document.getElementById("btn_modifier").className = "btn_action rounded collapse";
    document.getElementById("btn_annuler").className = "btn_action rounded collapse";
    document.getElementById("btn_afficher").className = "btn_action rounded";
    document.getElementById("form_afficher").className = "collapse";

    var iLongueur= aOfAdmins.length;
    aOfAdmins[iLongueur]= [];
    aOfAdmins[iLongueur]["nom"]= $('#nom').val();
    aOfAdmins[iLongueur]["prenom"]= $('#prenom').val();
    aOfAdmins[iLongueur]["telephone"]= $('#telephone').val();
    aOfAdmins[iLongueur]["email"]= $('#email').val();
    aOfAdmins[iLongueur]["code_agent"]= $('#code_agent').val();
    aOfAdmins[iLongueur]["password"]= $('#password').val();
    aOfAdmins[iLongueur]["password_confirmation"]= $('#password_confirmation').val();
    rebuildTable() 

    $('#nom').val("");
    $('#prenom').val("");
    $('#telephone').val("");
    $('#email').val("");
    $('#code_agent').val("");
    $('#password').val("");
    $('#password_confirmation').val("");

}

var iIndiceEditionEncours;
function editAdmin(iIndiceEdit)	
{
    document.getElementById("btn_ajouter").className = "btn_action rounded hide";
    document.getElementById("btn_modifier").className = "btn_action rounded";
    document.getElementById("btn_annuler").className = "btn_action rounded";
    document.getElementById("form_afficher").className = "collapse";
    document.getElementById("btn_afficher").className = "btn_action rounded hide";
    
    iIndiceEditionEncours= iIndiceEdit;
    $('#nom').val( aOfAdmins[iIndiceEdit]["nom"] );
    $('#prenom').val( aOfAdmins[iIndiceEdit]["prenom"] );
    $('#telephone').val( aOfAdmins[iIndiceEdit]["telephone"] );
    $('#email').val( aOfAdmins[iIndiceEdit]["email"] );
    $('#code_agent').val( aOfAdmins[iIndiceEdit]["code_agent"] );
    $('#password').val( aOfAdmins[iIndiceEdit]["password"] );
    $('#password_confirmation').val( aOfAdmins[iIndiceEdit]["password_confirmation"] );
}

function majAdmin()	
{
    document.getElementById("btn_afficher").className = "btn_action rounded";
    document.getElementById("btn_ajouter").className = "btn_action rounded hide";
    document.getElementById("btn_modifier").className = "btn_action rounded hide";
    document.getElementById("btn_annuler").className = "btn_action rounded hide";
    document.getElementById("form_afficher").className = "collapse"; 

    aOfAdmins[iIndiceEditionEncours]["nom"]= $('#nom').val();
    aOfAdmins[iIndiceEditionEncours]["prenom"]= $('#prenom').val();
    aOfAdmins[iIndiceEditionEncours]["telephone"]= $('#telephone').val();
    aOfAdmins[iIndiceEditionEncours]["email"]= $('#email').val();
    aOfAdmins[iIndiceEditionEncours]["code_agent"]= $('#code_agent').val();
    aOfAdmins[iIndiceEditionEncours]["password"]= $('#password').val();
    aOfAdmins[iIndiceEditionEncours]["password_confirmation"]= $('#password_confirmation').val();
    rebuildTable() 

    $('#nom').val("");
    $('#prenom').val("");
    $('#telephone').val("");
    $('#email').val("");
    $('#code_agent').val("");
    $('#password').val("");
    $('#password_confirmation').val("");
}

function supprimAdmin(iIndiceSupprim)	
{
    aOfAdmin.splice (iIndiceSupprim, 1);
    rebuildTable() 
}

function annulAdmin()	
{
    document.getElementById("btn_ajouter").className = "btn_action rounded collapse";
    document.getElementById("btn_modifier").className = "btn_action rounded collapse";
    document.getElementById("btn_annuler").className = "btn_action rounded collapse";
    document.getElementById("btn_afficher").className = "btn_action rounded";
    document.getElementById("form_afficher").className = "collapse";

    $('#nom').val("");
    $('#prenom').val("");
    $('#telephone').val("");
    $('#email').val("");
    $('#code_agent').val("");
    $('#password').val("");
    $('#password_confirmation').val("");
}

// CONFIGURATION DATATABLE
const configuration = 
{
    "stateSave": false,
    "order": [[1, "asc"]],
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, 100, -1], ["Dix", "Vingt cinq", "Cinquante", "Cent", "Voir tout"]], 
    "language": 
    {
        "info": "Administrateurs _START_ à _END_ sur _TOTAL_ sélectionnées",
        "emptyTable": "Aucun administrateur",
        "lengthMenu": "_MENU_ Administrateur par page",
        "search": "Rechercher : ",
        "zeroRecords": "Aucun résultat de recherche",
        "paginate": 
        {
            "previous": "Précédent",
            "next": "Suivant"
        },
        "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoEmpty":      "Administrateur 0 à 0 sur 0 sélectionnée",
    },
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": false
        },
        {
            "orderable": false
        }
    ],
    'retrieve': true
};

var tables;
$(document).ready(function() 
{
    constructTable();
    // INIT DATATABLE
    tables = $('#table_Administrateurs').DataTable(configuration);
});

function rebuildTable() 
{
    $('#table_Administrateurs').html("");
    tables.clear(); 
    tables.destroy(); 
    constructTable();
    tables = $('#table_Administrateurs').DataTable(configuration);
}
