/**
 * Construction du tableau sans base de données pour test
 */

var aSocial = [];
aSocial[0] = [];
aSocial[0]["ordre"] = 1;
aSocial[0]["nom"] = "Facebook";
aSocial[0]["picto"] = "fa-facebook";
aSocial[0]["lien"] = "https://fr-fr.facebook.com/";

aSocial[1] = [];
aSocial[1]["ordre"] = 2;
aSocial[1]["nom"] = "Twitter";
aSocial[1]["picto"] = "fa-twitter";
aSocial[1]["lien"] = "https://twitter.com/?lang=fr";

aSocial[2] = [];
aSocial[2]["ordre"] = 3;
aSocial[2]["nom"] = "Youtube";
aSocial[2]["picto"] = "fa-youtube";
aSocial[2]["lien"] = "https://www.youtube.com/";

/**
 * Construction du DataTable
 */
function constructTable() {
    var i;

    var sHTML = "";
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th>Ordre</th>";
    sHTML += "<th>Nom</th>";
    sHTML += "<th>Pictogramme</th>";
    sHTML += "<th>Lien</th>";
    sHTML += "<th>Editer</th>";
    sHTML += "<th>Supprimer</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";
    /**
     * Boucle pour remplir chaque ligne du tableau
     */
    for (i = 0; i < aSocial.length; i++) {
        sHTML += "<tr>";
        /**
         * Si l'indice de la ligne est égal à 1, seule la flèche Down est affichée
         */
        if (i === 0) {
            sHTML += `<td class="ordre">
                        <span id="ordre">` + aSocial[i]["ordre"] + `</span>
                        <span onclick="down(`+ i +`)">
                            <i class= "fas fa-arrow-circle-down" id='down'></i>
                        </span>
                      </td>`;
        }
        /**
         * Si l'indice de la ligne est le dernier du tableau, seule la flèche Up est affichée
         */
        else if (i === aSocial.length - 1) {
            sHTML += `<td class="ordre">
                        <span id="ordre">` + aSocial[i]["ordre"] + `</span>
                        <span onclick="up(`+ i+ `)">
                            <i class= "fas fa-arrow-circle-up" id='up'></i>
                        </span>
                      </td>`;
        }
        /**
         * Pour toutes les lignes de milieu de tableau, les deux flèches s'affichent
         */
        else {
            sHTML += `<td class="ordre">
                        <span id="ordre">` + aSocial[i]["ordre"] + `</span>
                        <span onclick="down(`+ i + `)">
                            <i class="fas fa-arrow-circle-down" id='down'></i>
                        </span>
                        <span onclick="up(`+ i + `)">
                            <i class="fas fa-arrow-circle-up" id='up'></i>
                        </span>
                      </td>`;
        }
        sHTML += "<td class=\"nom\">" + aSocial[i]["nom"] + "</td>";
        sHTML += "<td class=\"picto\"><i class='fab " + aSocial[i]["picto"] + "'></i></td>";
        sHTML += "<td class=\"lien\">" + aSocial[i]["lien"] + "</td>";
        sHTML += "<td class=\"edit\" onClick=\"editSocial(" + i + ")\">Editer</td>";
        sHTML += "<td class=\"sup\" onClick=\"supprimSocial(" + i + ")\">Supprimer</td>";
        sHTML += "</tr>";
    }

    sHTML += "</tbody>";
    $('#tableSocial').html(sHTML);
    $('#ordre').val("");
    $('#nom').val("");
    $('#picto').val("");
    $('#lien').val("");
    $('#btn_ajouter').show();
    $('#btn_modifier').hide();
    $('#btn_annuler').hide();
}

// CONFIGURATION DATATABLE
const configuration = {
    "stateSave": false,
    "order": [
        [0, "asc"]
    ],
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [
        [10, 25, 50, 100, -1],
        [10, 20, 50, 100, "Tout"]
    ],
    "language": {
        "info": "Réseaux sociaux _START_ à _END_ sur _TOTAL_ sélectionnées",
        "emptyTable": "Aucun réseau social",
        "lengthMenu": "_MENU_ Réseaux sociaux par page",
        "search": "Rechercher : ",
        "zeroRecords": "Aucun résultat de recherche",
        "paginate": {
            "previous": "Précédent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        "sInfoEmpty": "Réseau social 0 à 0 sur 0 sélectionnée",
    },
    "columns": [{
        "orderable": false,
        },
        {
            "orderable": false,
        },
        {
            "orderable": false,
        },
        {
            "orderable": false,
        },
        {
            "orderable": false,
        },
        {
            "orderable": false,
        }
    ],
    'retrieve': true
};

var tables;
$(document).ready(function () {
    constructTable();
    // INIT DATATABLE
    tables = $('#tableSocial').DataTable(configuration);
});

function construct() {
    tables.destroy();
    constructTable();
    tables = $('#tableSocial').DataTable(configuration);
}

function btn() {
    if (sBase == "ajout"){
        $('#btn_ajouter').show();
        $('#btn_modifier').hide();
        $('#btn_annuler').hide();
    } else {
        $('#btn_ajouter').hide();
        $('#btn_modifier').show();
        $('#btn_annuler').show();
    }
}

var sBase = "ajout";
btn();

function ajoutSocial() {
    var iLongueur = aSocial.length;
    aSocial[iLongueur] = [];
    aSocial[iLongueur]["ordre"] = iLongueur + 1;
    aSocial[iLongueur]["nom"] = $('#nom').val();
    aSocial[iLongueur]["picto"] = $('#picto').val();
    aSocial[iLongueur]["lien"] = $('#lien').val();
    construct();
}

var iIndiceEditionEncours;

function editSocial(iIndiceEdit) {
    sBase= "edit";
    btn();
    iIndiceEditionEncours = iIndiceEdit;
    $('#nom').val(aSocial[iIndiceEdit]["nom"]);
    $('#picto').val(aSocial[iIndiceEdit]["picto"]);
    $('#lien').val(aSocial[iIndiceEdit]["lien"]);
}

function majSocial() {
    aSocial[iIndiceEditionEncours]["nom"] = $('#nom').val();
    aSocial[iIndiceEditionEncours]["picto"] = $('#picto').val();
    aSocial[iIndiceEditionEncours]["lien"] = $('#lien').val();
    construct();
}

function annulSocial() {
    construct();
}

function supprimSocial(iIndiceEdit) {
    aSocial.splice(iIndiceEdit, 1);
    construct();
}

/**
 * Permet de descendre la ligne pour l'ordre de priorité
 * On inverse la ligne sélectionnée avec celle du dessous
 * On stocke une copie du tableau dans une variable temporaire afin d'appliquer le nouvel ordre
 * @param i = indice sélectionné
 */
function down(i) {
    aSocial[i]["ordre"] = aSocial[i]["ordre"] + 1;
    aSocial[i+1]["ordre"] = aSocial[i+1]["ordre"] -1;
    var temp = aSocial.slice();
    aSocial[i] = temp[i+1];
    aSocial[i+1] = temp[i];
    construct()
}

/**
 * Permet de remonter la ligne pour l'ordre de priorité
 * On inverse la ligne sélectionnée avec celle du dessus
 * On stocke une copie du tableau dans une variable temporaire afin d'appliquer le nouvel ordre
 * @param i = indice sélectionné
 */
 function up(i) {
     aSocial[i]["ordre"] = i;
     aSocial[i-1]["ordre"] = i+1;
     var temp = aSocial.slice();
     console.log(temp);
     aSocial[i-1] = temp[i];
     aSocial[i] = temp[i-1];
     construct()
 }
